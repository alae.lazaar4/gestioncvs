package com.optimgov.cvs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class ProjetDemandeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProjetDemande.class);
        ProjetDemande projetDemande1 = new ProjetDemande();
        projetDemande1.setId(1L);
        ProjetDemande projetDemande2 = new ProjetDemande();
        projetDemande2.setId(projetDemande1.getId());
        assertThat(projetDemande1).isEqualTo(projetDemande2);
        projetDemande2.setId(2L);
        assertThat(projetDemande1).isNotEqualTo(projetDemande2);
        projetDemande1.setId(null);
        assertThat(projetDemande1).isNotEqualTo(projetDemande2);
    }
}
