package com.optimgov.cvs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class EnvironTechTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EnvironTech.class);
        EnvironTech environTech1 = new EnvironTech();
        environTech1.setId(1L);
        EnvironTech environTech2 = new EnvironTech();
        environTech2.setId(environTech1.getId());
        assertThat(environTech1).isEqualTo(environTech2);
        environTech2.setId(2L);
        assertThat(environTech1).isNotEqualTo(environTech2);
        environTech1.setId(null);
        assertThat(environTech1).isNotEqualTo(environTech2);
    }
}
