package com.optimgov.cvs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class RessourceProposeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RessourcePropose.class);
        RessourcePropose ressourcePropose1 = new RessourcePropose();
        ressourcePropose1.setId(1L);
        RessourcePropose ressourcePropose2 = new RessourcePropose();
        ressourcePropose2.setId(ressourcePropose1.getId());
        assertThat(ressourcePropose1).isEqualTo(ressourcePropose2);
        ressourcePropose2.setId(2L);
        assertThat(ressourcePropose1).isNotEqualTo(ressourcePropose2);
        ressourcePropose1.setId(null);
        assertThat(ressourcePropose1).isNotEqualTo(ressourcePropose2);
    }
}
