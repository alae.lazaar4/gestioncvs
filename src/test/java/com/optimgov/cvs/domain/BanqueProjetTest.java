package com.optimgov.cvs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class BanqueProjetTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BanqueProjet.class);
        BanqueProjet banqueProjet1 = new BanqueProjet();
        banqueProjet1.setId(1L);
        BanqueProjet banqueProjet2 = new BanqueProjet();
        banqueProjet2.setId(banqueProjet1.getId());
        assertThat(banqueProjet1).isEqualTo(banqueProjet2);
        banqueProjet2.setId(2L);
        assertThat(banqueProjet1).isNotEqualTo(banqueProjet2);
        banqueProjet1.setId(null);
        assertThat(banqueProjet1).isNotEqualTo(banqueProjet2);
    }
}
