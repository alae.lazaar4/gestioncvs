package com.optimgov.cvs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class RessourceDemandeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RessourceDemande.class);
        RessourceDemande ressourceDemande1 = new RessourceDemande();
        ressourceDemande1.setId(1L);
        RessourceDemande ressourceDemande2 = new RessourceDemande();
        ressourceDemande2.setId(ressourceDemande1.getId());
        assertThat(ressourceDemande1).isEqualTo(ressourceDemande2);
        ressourceDemande2.setId(2L);
        assertThat(ressourceDemande1).isNotEqualTo(ressourceDemande2);
        ressourceDemande1.setId(null);
        assertThat(ressourceDemande1).isNotEqualTo(ressourceDemande2);
    }
}
