package com.optimgov.cvs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class BanqueQualificationTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BanqueQualification.class);
        BanqueQualification banqueQualification1 = new BanqueQualification();
        banqueQualification1.setId(1L);
        BanqueQualification banqueQualification2 = new BanqueQualification();
        banqueQualification2.setId(banqueQualification1.getId());
        assertThat(banqueQualification1).isEqualTo(banqueQualification2);
        banqueQualification2.setId(2L);
        assertThat(banqueQualification1).isNotEqualTo(banqueQualification2);
        banqueQualification1.setId(null);
        assertThat(banqueQualification1).isNotEqualTo(banqueQualification2);
    }
}
