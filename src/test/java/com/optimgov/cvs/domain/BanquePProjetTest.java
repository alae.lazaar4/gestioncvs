package com.optimgov.cvs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class BanquePProjetTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BanquePProjet.class);
        BanquePProjet banquePProjet1 = new BanquePProjet();
        banquePProjet1.setId(1L);
        BanquePProjet banquePProjet2 = new BanquePProjet();
        banquePProjet2.setId(banquePProjet1.getId());
        assertThat(banquePProjet1).isEqualTo(banquePProjet2);
        banquePProjet2.setId(2L);
        assertThat(banquePProjet1).isNotEqualTo(banquePProjet2);
        banquePProjet1.setId(null);
        assertThat(banquePProjet1).isNotEqualTo(banquePProjet2);
    }
}
