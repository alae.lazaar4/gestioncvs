package com.optimgov.cvs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class BanquePProfilTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BanquePProfil.class);
        BanquePProfil banquePProfil1 = new BanquePProfil();
        banquePProfil1.setId(1L);
        BanquePProfil banquePProfil2 = new BanquePProfil();
        banquePProfil2.setId(banquePProfil1.getId());
        assertThat(banquePProfil1).isEqualTo(banquePProfil2);
        banquePProfil2.setId(2L);
        assertThat(banquePProfil1).isNotEqualTo(banquePProfil2);
        banquePProfil1.setId(null);
        assertThat(banquePProfil1).isNotEqualTo(banquePProfil2);
    }
}
