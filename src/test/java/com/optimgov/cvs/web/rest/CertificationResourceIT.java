package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.GestioncvsApp;
import com.optimgov.cvs.domain.Certification;
import com.optimgov.cvs.repository.CertificationRepository;
import com.optimgov.cvs.service.CertificationService;
import com.optimgov.cvs.service.dto.CertificationDTO;
import com.optimgov.cvs.service.mapper.CertificationMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CertificationResource} REST controller.
 */
@SpringBootTest(classes = GestioncvsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CertificationResourceIT {

    private static final String DEFAULT_INTITULE = "AAAAAAAAAA";
    private static final String UPDATED_INTITULE = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISME = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISME = "BBBBBBBBBB";

    private static final Instant DEFAULT_ANNEE_OBTENTION = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_ANNEE_OBTENTION = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_DOMAINE = "AAAAAAAAAA";
    private static final String UPDATED_DOMAINE = "BBBBBBBBBB";

    @Autowired
    private CertificationRepository certificationRepository;

    @Autowired
    private CertificationMapper certificationMapper;

    @Autowired
    private CertificationService certificationService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCertificationMockMvc;

    private Certification certification;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Certification createEntity(EntityManager em) {
        Certification certification = new Certification()
            .intitule(DEFAULT_INTITULE)
            .organisme(DEFAULT_ORGANISME)
            .anneeObtention(DEFAULT_ANNEE_OBTENTION)
            .domaine(DEFAULT_DOMAINE);
        return certification;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Certification createUpdatedEntity(EntityManager em) {
        Certification certification = new Certification()
            .intitule(UPDATED_INTITULE)
            .organisme(UPDATED_ORGANISME)
            .anneeObtention(UPDATED_ANNEE_OBTENTION)
            .domaine(UPDATED_DOMAINE);
        return certification;
    }

    @BeforeEach
    public void initTest() {
        certification = createEntity(em);
    }

    @Test
    @Transactional
    public void createCertification() throws Exception {
        int databaseSizeBeforeCreate = certificationRepository.findAll().size();
        // Create the Certification
        CertificationDTO certificationDTO = certificationMapper.toDto(certification);
        restCertificationMockMvc.perform(post("/api/certifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(certificationDTO)))
            .andExpect(status().isCreated());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeCreate + 1);
        Certification testCertification = certificationList.get(certificationList.size() - 1);
        assertThat(testCertification.getIntitule()).isEqualTo(DEFAULT_INTITULE);
        assertThat(testCertification.getOrganisme()).isEqualTo(DEFAULT_ORGANISME);
        assertThat(testCertification.getAnneeObtention()).isEqualTo(DEFAULT_ANNEE_OBTENTION);
        assertThat(testCertification.getDomaine()).isEqualTo(DEFAULT_DOMAINE);
    }

    @Test
    @Transactional
    public void createCertificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = certificationRepository.findAll().size();

        // Create the Certification with an existing ID
        certification.setId(1L);
        CertificationDTO certificationDTO = certificationMapper.toDto(certification);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCertificationMockMvc.perform(post("/api/certifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(certificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCertifications() throws Exception {
        // Initialize the database
        certificationRepository.saveAndFlush(certification);

        // Get all the certificationList
        restCertificationMockMvc.perform(get("/api/certifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(certification.getId().intValue())))
            .andExpect(jsonPath("$.[*].intitule").value(hasItem(DEFAULT_INTITULE)))
            .andExpect(jsonPath("$.[*].organisme").value(hasItem(DEFAULT_ORGANISME)))
            .andExpect(jsonPath("$.[*].anneeObtention").value(hasItem(DEFAULT_ANNEE_OBTENTION.toString())))
            .andExpect(jsonPath("$.[*].domaine").value(hasItem(DEFAULT_DOMAINE)));
    }
    
    @Test
    @Transactional
    public void getCertification() throws Exception {
        // Initialize the database
        certificationRepository.saveAndFlush(certification);

        // Get the certification
        restCertificationMockMvc.perform(get("/api/certifications/{id}", certification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(certification.getId().intValue()))
            .andExpect(jsonPath("$.intitule").value(DEFAULT_INTITULE))
            .andExpect(jsonPath("$.organisme").value(DEFAULT_ORGANISME))
            .andExpect(jsonPath("$.anneeObtention").value(DEFAULT_ANNEE_OBTENTION.toString()))
            .andExpect(jsonPath("$.domaine").value(DEFAULT_DOMAINE));
    }
    @Test
    @Transactional
    public void getNonExistingCertification() throws Exception {
        // Get the certification
        restCertificationMockMvc.perform(get("/api/certifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCertification() throws Exception {
        // Initialize the database
        certificationRepository.saveAndFlush(certification);

        int databaseSizeBeforeUpdate = certificationRepository.findAll().size();

        // Update the certification
        Certification updatedCertification = certificationRepository.findById(certification.getId()).get();
        // Disconnect from session so that the updates on updatedCertification are not directly saved in db
        em.detach(updatedCertification);
        updatedCertification
            .intitule(UPDATED_INTITULE)
            .organisme(UPDATED_ORGANISME)
            .anneeObtention(UPDATED_ANNEE_OBTENTION)
            .domaine(UPDATED_DOMAINE);
        CertificationDTO certificationDTO = certificationMapper.toDto(updatedCertification);

        restCertificationMockMvc.perform(put("/api/certifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(certificationDTO)))
            .andExpect(status().isOk());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeUpdate);
        Certification testCertification = certificationList.get(certificationList.size() - 1);
        assertThat(testCertification.getIntitule()).isEqualTo(UPDATED_INTITULE);
        assertThat(testCertification.getOrganisme()).isEqualTo(UPDATED_ORGANISME);
        assertThat(testCertification.getAnneeObtention()).isEqualTo(UPDATED_ANNEE_OBTENTION);
        assertThat(testCertification.getDomaine()).isEqualTo(UPDATED_DOMAINE);
    }

    @Test
    @Transactional
    public void updateNonExistingCertification() throws Exception {
        int databaseSizeBeforeUpdate = certificationRepository.findAll().size();

        // Create the Certification
        CertificationDTO certificationDTO = certificationMapper.toDto(certification);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCertificationMockMvc.perform(put("/api/certifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(certificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Certification in the database
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCertification() throws Exception {
        // Initialize the database
        certificationRepository.saveAndFlush(certification);

        int databaseSizeBeforeDelete = certificationRepository.findAll().size();

        // Delete the certification
        restCertificationMockMvc.perform(delete("/api/certifications/{id}", certification.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Certification> certificationList = certificationRepository.findAll();
        assertThat(certificationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
