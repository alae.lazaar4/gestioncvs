package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.GestioncvsApp;
import com.optimgov.cvs.domain.BanqueQualification;
import com.optimgov.cvs.repository.BanqueQualificationRepository;
import com.optimgov.cvs.service.BanqueQualificationService;
import com.optimgov.cvs.service.dto.BanqueQualificationDTO;
import com.optimgov.cvs.service.mapper.BanqueQualificationMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BanqueQualificationResource} REST controller.
 */
@SpringBootTest(classes = GestioncvsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class BanqueQualificationResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private BanqueQualificationRepository banqueQualificationRepository;

    @Autowired
    private BanqueQualificationMapper banqueQualificationMapper;

    @Autowired
    private BanqueQualificationService banqueQualificationService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBanqueQualificationMockMvc;

    private BanqueQualification banqueQualification;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BanqueQualification createEntity(EntityManager em) {
        BanqueQualification banqueQualification = new BanqueQualification()
            .libelle(DEFAULT_LIBELLE)
            .type(DEFAULT_TYPE)
            .description(DEFAULT_DESCRIPTION);
        return banqueQualification;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BanqueQualification createUpdatedEntity(EntityManager em) {
        BanqueQualification banqueQualification = new BanqueQualification()
            .libelle(UPDATED_LIBELLE)
            .type(UPDATED_TYPE)
            .description(UPDATED_DESCRIPTION);
        return banqueQualification;
    }

    @BeforeEach
    public void initTest() {
        banqueQualification = createEntity(em);
    }

    @Test
    @Transactional
    public void createBanqueQualification() throws Exception {
        int databaseSizeBeforeCreate = banqueQualificationRepository.findAll().size();
        // Create the BanqueQualification
        BanqueQualificationDTO banqueQualificationDTO = banqueQualificationMapper.toDto(banqueQualification);
        restBanqueQualificationMockMvc.perform(post("/api/banque-qualifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banqueQualificationDTO)))
            .andExpect(status().isCreated());

        // Validate the BanqueQualification in the database
        List<BanqueQualification> banqueQualificationList = banqueQualificationRepository.findAll();
        assertThat(banqueQualificationList).hasSize(databaseSizeBeforeCreate + 1);
        BanqueQualification testBanqueQualification = banqueQualificationList.get(banqueQualificationList.size() - 1);
        assertThat(testBanqueQualification.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testBanqueQualification.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testBanqueQualification.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createBanqueQualificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = banqueQualificationRepository.findAll().size();

        // Create the BanqueQualification with an existing ID
        banqueQualification.setId(1L);
        BanqueQualificationDTO banqueQualificationDTO = banqueQualificationMapper.toDto(banqueQualification);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBanqueQualificationMockMvc.perform(post("/api/banque-qualifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banqueQualificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BanqueQualification in the database
        List<BanqueQualification> banqueQualificationList = banqueQualificationRepository.findAll();
        assertThat(banqueQualificationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBanqueQualifications() throws Exception {
        // Initialize the database
        banqueQualificationRepository.saveAndFlush(banqueQualification);

        // Get all the banqueQualificationList
        restBanqueQualificationMockMvc.perform(get("/api/banque-qualifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(banqueQualification.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getBanqueQualification() throws Exception {
        // Initialize the database
        banqueQualificationRepository.saveAndFlush(banqueQualification);

        // Get the banqueQualification
        restBanqueQualificationMockMvc.perform(get("/api/banque-qualifications/{id}", banqueQualification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(banqueQualification.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }
    @Test
    @Transactional
    public void getNonExistingBanqueQualification() throws Exception {
        // Get the banqueQualification
        restBanqueQualificationMockMvc.perform(get("/api/banque-qualifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBanqueQualification() throws Exception {
        // Initialize the database
        banqueQualificationRepository.saveAndFlush(banqueQualification);

        int databaseSizeBeforeUpdate = banqueQualificationRepository.findAll().size();

        // Update the banqueQualification
        BanqueQualification updatedBanqueQualification = banqueQualificationRepository.findById(banqueQualification.getId()).get();
        // Disconnect from session so that the updates on updatedBanqueQualification are not directly saved in db
        em.detach(updatedBanqueQualification);
        updatedBanqueQualification
            .libelle(UPDATED_LIBELLE)
            .type(UPDATED_TYPE)
            .description(UPDATED_DESCRIPTION);
        BanqueQualificationDTO banqueQualificationDTO = banqueQualificationMapper.toDto(updatedBanqueQualification);

        restBanqueQualificationMockMvc.perform(put("/api/banque-qualifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banqueQualificationDTO)))
            .andExpect(status().isOk());

        // Validate the BanqueQualification in the database
        List<BanqueQualification> banqueQualificationList = banqueQualificationRepository.findAll();
        assertThat(banqueQualificationList).hasSize(databaseSizeBeforeUpdate);
        BanqueQualification testBanqueQualification = banqueQualificationList.get(banqueQualificationList.size() - 1);
        assertThat(testBanqueQualification.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testBanqueQualification.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testBanqueQualification.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingBanqueQualification() throws Exception {
        int databaseSizeBeforeUpdate = banqueQualificationRepository.findAll().size();

        // Create the BanqueQualification
        BanqueQualificationDTO banqueQualificationDTO = banqueQualificationMapper.toDto(banqueQualification);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBanqueQualificationMockMvc.perform(put("/api/banque-qualifications")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banqueQualificationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BanqueQualification in the database
        List<BanqueQualification> banqueQualificationList = banqueQualificationRepository.findAll();
        assertThat(banqueQualificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBanqueQualification() throws Exception {
        // Initialize the database
        banqueQualificationRepository.saveAndFlush(banqueQualification);

        int databaseSizeBeforeDelete = banqueQualificationRepository.findAll().size();

        // Delete the banqueQualification
        restBanqueQualificationMockMvc.perform(delete("/api/banque-qualifications/{id}", banqueQualification.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BanqueQualification> banqueQualificationList = banqueQualificationRepository.findAll();
        assertThat(banqueQualificationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
