package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.GestioncvsApp;
import com.optimgov.cvs.domain.EnvironTech;
import com.optimgov.cvs.repository.EnvironTechRepository;
import com.optimgov.cvs.service.EnvironTechService;
import com.optimgov.cvs.service.dto.EnvironTechDTO;
import com.optimgov.cvs.service.mapper.EnvironTechMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link EnvironTechResource} REST controller.
 */
@SpringBootTest(classes = GestioncvsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class EnvironTechResourceIT {

    private static final String DEFAULT_WEB_TECHNLOGIES = "AAAAAAAAAA";
    private static final String UPDATED_WEB_TECHNLOGIES = "BBBBBBBBBB";

    private static final String DEFAULT_WEB_CLIENT_TECHNOLOGIES = "AAAAAAAAAA";
    private static final String UPDATED_WEB_CLIENT_TECHNOLOGIES = "BBBBBBBBBB";

    private static final String DEFAULT_BASE_DONNEE = "AAAAAAAAAA";
    private static final String UPDATED_BASE_DONNEE = "BBBBBBBBBB";

    private static final String DEFAULT_PROJET_MANAGEMENT_BUILDING = "AAAAAAAAAA";
    private static final String UPDATED_PROJET_MANAGEMENT_BUILDING = "BBBBBBBBBB";

    private static final String DEFAULT_ARCHITECHTURE = "AAAAAAAAAA";
    private static final String UPDATED_ARCHITECHTURE = "BBBBBBBBBB";

    @Autowired
    private EnvironTechRepository environTechRepository;

    @Autowired
    private EnvironTechMapper environTechMapper;

    @Autowired
    private EnvironTechService environTechService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEnvironTechMockMvc;

    private EnvironTech environTech;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EnvironTech createEntity(EntityManager em) {
        EnvironTech environTech = new EnvironTech()
            .webTechnlogies(DEFAULT_WEB_TECHNLOGIES)
            .webClientTechnologies(DEFAULT_WEB_CLIENT_TECHNOLOGIES)
            .baseDonnee(DEFAULT_BASE_DONNEE)
            .projetManagementBuilding(DEFAULT_PROJET_MANAGEMENT_BUILDING)
            .architechture(DEFAULT_ARCHITECHTURE);
        return environTech;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EnvironTech createUpdatedEntity(EntityManager em) {
        EnvironTech environTech = new EnvironTech()
            .webTechnlogies(UPDATED_WEB_TECHNLOGIES)
            .webClientTechnologies(UPDATED_WEB_CLIENT_TECHNOLOGIES)
            .baseDonnee(UPDATED_BASE_DONNEE)
            .projetManagementBuilding(UPDATED_PROJET_MANAGEMENT_BUILDING)
            .architechture(UPDATED_ARCHITECHTURE);
        return environTech;
    }

    @BeforeEach
    public void initTest() {
        environTech = createEntity(em);
    }

    @Test
    @Transactional
    public void createEnvironTech() throws Exception {
        int databaseSizeBeforeCreate = environTechRepository.findAll().size();
        // Create the EnvironTech
        EnvironTechDTO environTechDTO = environTechMapper.toDto(environTech);
        restEnvironTechMockMvc.perform(post("/api/environ-teches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(environTechDTO)))
            .andExpect(status().isCreated());

        // Validate the EnvironTech in the database
        List<EnvironTech> environTechList = environTechRepository.findAll();
        assertThat(environTechList).hasSize(databaseSizeBeforeCreate + 1);
        EnvironTech testEnvironTech = environTechList.get(environTechList.size() - 1);
        assertThat(testEnvironTech.getWebTechnlogies()).isEqualTo(DEFAULT_WEB_TECHNLOGIES);
        assertThat(testEnvironTech.getWebClientTechnologies()).isEqualTo(DEFAULT_WEB_CLIENT_TECHNOLOGIES);
        assertThat(testEnvironTech.getBaseDonnee()).isEqualTo(DEFAULT_BASE_DONNEE);
        assertThat(testEnvironTech.getProjetManagementBuilding()).isEqualTo(DEFAULT_PROJET_MANAGEMENT_BUILDING);
        assertThat(testEnvironTech.getArchitechture()).isEqualTo(DEFAULT_ARCHITECHTURE);
    }

    @Test
    @Transactional
    public void createEnvironTechWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = environTechRepository.findAll().size();

        // Create the EnvironTech with an existing ID
        environTech.setId(1L);
        EnvironTechDTO environTechDTO = environTechMapper.toDto(environTech);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEnvironTechMockMvc.perform(post("/api/environ-teches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(environTechDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EnvironTech in the database
        List<EnvironTech> environTechList = environTechRepository.findAll();
        assertThat(environTechList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllEnvironTeches() throws Exception {
        // Initialize the database
        environTechRepository.saveAndFlush(environTech);

        // Get all the environTechList
        restEnvironTechMockMvc.perform(get("/api/environ-teches?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(environTech.getId().intValue())))
            .andExpect(jsonPath("$.[*].webTechnlogies").value(hasItem(DEFAULT_WEB_TECHNLOGIES)))
            .andExpect(jsonPath("$.[*].webClientTechnologies").value(hasItem(DEFAULT_WEB_CLIENT_TECHNOLOGIES)))
            .andExpect(jsonPath("$.[*].baseDonnee").value(hasItem(DEFAULT_BASE_DONNEE)))
            .andExpect(jsonPath("$.[*].projetManagementBuilding").value(hasItem(DEFAULT_PROJET_MANAGEMENT_BUILDING)))
            .andExpect(jsonPath("$.[*].architechture").value(hasItem(DEFAULT_ARCHITECHTURE)));
    }
    
    @Test
    @Transactional
    public void getEnvironTech() throws Exception {
        // Initialize the database
        environTechRepository.saveAndFlush(environTech);

        // Get the environTech
        restEnvironTechMockMvc.perform(get("/api/environ-teches/{id}", environTech.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(environTech.getId().intValue()))
            .andExpect(jsonPath("$.webTechnlogies").value(DEFAULT_WEB_TECHNLOGIES))
            .andExpect(jsonPath("$.webClientTechnologies").value(DEFAULT_WEB_CLIENT_TECHNOLOGIES))
            .andExpect(jsonPath("$.baseDonnee").value(DEFAULT_BASE_DONNEE))
            .andExpect(jsonPath("$.projetManagementBuilding").value(DEFAULT_PROJET_MANAGEMENT_BUILDING))
            .andExpect(jsonPath("$.architechture").value(DEFAULT_ARCHITECHTURE));
    }
    @Test
    @Transactional
    public void getNonExistingEnvironTech() throws Exception {
        // Get the environTech
        restEnvironTechMockMvc.perform(get("/api/environ-teches/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEnvironTech() throws Exception {
        // Initialize the database
        environTechRepository.saveAndFlush(environTech);

        int databaseSizeBeforeUpdate = environTechRepository.findAll().size();

        // Update the environTech
        EnvironTech updatedEnvironTech = environTechRepository.findById(environTech.getId()).get();
        // Disconnect from session so that the updates on updatedEnvironTech are not directly saved in db
        em.detach(updatedEnvironTech);
        updatedEnvironTech
            .webTechnlogies(UPDATED_WEB_TECHNLOGIES)
            .webClientTechnologies(UPDATED_WEB_CLIENT_TECHNOLOGIES)
            .baseDonnee(UPDATED_BASE_DONNEE)
            .projetManagementBuilding(UPDATED_PROJET_MANAGEMENT_BUILDING)
            .architechture(UPDATED_ARCHITECHTURE);
        EnvironTechDTO environTechDTO = environTechMapper.toDto(updatedEnvironTech);

        restEnvironTechMockMvc.perform(put("/api/environ-teches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(environTechDTO)))
            .andExpect(status().isOk());

        // Validate the EnvironTech in the database
        List<EnvironTech> environTechList = environTechRepository.findAll();
        assertThat(environTechList).hasSize(databaseSizeBeforeUpdate);
        EnvironTech testEnvironTech = environTechList.get(environTechList.size() - 1);
        assertThat(testEnvironTech.getWebTechnlogies()).isEqualTo(UPDATED_WEB_TECHNLOGIES);
        assertThat(testEnvironTech.getWebClientTechnologies()).isEqualTo(UPDATED_WEB_CLIENT_TECHNOLOGIES);
        assertThat(testEnvironTech.getBaseDonnee()).isEqualTo(UPDATED_BASE_DONNEE);
        assertThat(testEnvironTech.getProjetManagementBuilding()).isEqualTo(UPDATED_PROJET_MANAGEMENT_BUILDING);
        assertThat(testEnvironTech.getArchitechture()).isEqualTo(UPDATED_ARCHITECHTURE);
    }

    @Test
    @Transactional
    public void updateNonExistingEnvironTech() throws Exception {
        int databaseSizeBeforeUpdate = environTechRepository.findAll().size();

        // Create the EnvironTech
        EnvironTechDTO environTechDTO = environTechMapper.toDto(environTech);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEnvironTechMockMvc.perform(put("/api/environ-teches")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(environTechDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EnvironTech in the database
        List<EnvironTech> environTechList = environTechRepository.findAll();
        assertThat(environTechList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEnvironTech() throws Exception {
        // Initialize the database
        environTechRepository.saveAndFlush(environTech);

        int databaseSizeBeforeDelete = environTechRepository.findAll().size();

        // Delete the environTech
        restEnvironTechMockMvc.perform(delete("/api/environ-teches/{id}", environTech.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EnvironTech> environTechList = environTechRepository.findAll();
        assertThat(environTechList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
