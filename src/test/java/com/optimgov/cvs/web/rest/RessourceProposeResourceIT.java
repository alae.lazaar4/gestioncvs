package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.GestioncvsApp;
import com.optimgov.cvs.domain.RessourcePropose;
import com.optimgov.cvs.repository.RessourceProposeRepository;
import com.optimgov.cvs.service.RessourceProposeService;
import com.optimgov.cvs.service.dto.RessourceProposeDTO;
import com.optimgov.cvs.service.mapper.RessourceProposeMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RessourceProposeResource} REST controller.
 */
@SpringBootTest(classes = GestioncvsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class RessourceProposeResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_POSTE_ACTUEL = "AAAAAAAAAA";
    private static final String UPDATED_POSTE_ACTUEL = "BBBBBBBBBB";

    private static final Integer DEFAULT_ANCIENNETE = 1;
    private static final Integer UPDATED_ANCIENNETE = 2;

    @Autowired
    private RessourceProposeRepository ressourceProposeRepository;

    @Autowired
    private RessourceProposeMapper ressourceProposeMapper;

    @Autowired
    private RessourceProposeService ressourceProposeService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRessourceProposeMockMvc;

    private RessourcePropose ressourcePropose;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RessourcePropose createEntity(EntityManager em) {
        RessourcePropose ressourcePropose = new RessourcePropose()
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM)
            .posteActuel(DEFAULT_POSTE_ACTUEL)
            .anciennete(DEFAULT_ANCIENNETE);
        return ressourcePropose;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RessourcePropose createUpdatedEntity(EntityManager em) {
        RessourcePropose ressourcePropose = new RessourcePropose()
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .posteActuel(UPDATED_POSTE_ACTUEL)
            .anciennete(UPDATED_ANCIENNETE);
        return ressourcePropose;
    }

    @BeforeEach
    public void initTest() {
        ressourcePropose = createEntity(em);
    }

    @Test
    @Transactional
    public void createRessourcePropose() throws Exception {
        int databaseSizeBeforeCreate = ressourceProposeRepository.findAll().size();
        // Create the RessourcePropose
        RessourceProposeDTO ressourceProposeDTO = ressourceProposeMapper.toDto(ressourcePropose);
        restRessourceProposeMockMvc.perform(post("/api/ressource-proposes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ressourceProposeDTO)))
            .andExpect(status().isCreated());

        // Validate the RessourcePropose in the database
        List<RessourcePropose> ressourceProposeList = ressourceProposeRepository.findAll();
        assertThat(ressourceProposeList).hasSize(databaseSizeBeforeCreate + 1);
        RessourcePropose testRessourcePropose = ressourceProposeList.get(ressourceProposeList.size() - 1);
        assertThat(testRessourcePropose.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testRessourcePropose.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testRessourcePropose.getPosteActuel()).isEqualTo(DEFAULT_POSTE_ACTUEL);
        assertThat(testRessourcePropose.getAnciennete()).isEqualTo(DEFAULT_ANCIENNETE);
    }

    @Test
    @Transactional
    public void createRessourceProposeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ressourceProposeRepository.findAll().size();

        // Create the RessourcePropose with an existing ID
        ressourcePropose.setId(1L);
        RessourceProposeDTO ressourceProposeDTO = ressourceProposeMapper.toDto(ressourcePropose);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRessourceProposeMockMvc.perform(post("/api/ressource-proposes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ressourceProposeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RessourcePropose in the database
        List<RessourcePropose> ressourceProposeList = ressourceProposeRepository.findAll();
        assertThat(ressourceProposeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNomIsRequired() throws Exception {
        int databaseSizeBeforeTest = ressourceProposeRepository.findAll().size();
        // set the field null
        ressourcePropose.setNom(null);

        // Create the RessourcePropose, which fails.
        RessourceProposeDTO ressourceProposeDTO = ressourceProposeMapper.toDto(ressourcePropose);


        restRessourceProposeMockMvc.perform(post("/api/ressource-proposes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ressourceProposeDTO)))
            .andExpect(status().isBadRequest());

        List<RessourcePropose> ressourceProposeList = ressourceProposeRepository.findAll();
        assertThat(ressourceProposeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRessourceProposes() throws Exception {
        // Initialize the database
        ressourceProposeRepository.saveAndFlush(ressourcePropose);

        // Get all the ressourceProposeList
        restRessourceProposeMockMvc.perform(get("/api/ressource-proposes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ressourcePropose.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].posteActuel").value(hasItem(DEFAULT_POSTE_ACTUEL)))
            .andExpect(jsonPath("$.[*].anciennete").value(hasItem(DEFAULT_ANCIENNETE)));
    }
    
    @Test
    @Transactional
    public void getRessourcePropose() throws Exception {
        // Initialize the database
        ressourceProposeRepository.saveAndFlush(ressourcePropose);

        // Get the ressourcePropose
        restRessourceProposeMockMvc.perform(get("/api/ressource-proposes/{id}", ressourcePropose.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ressourcePropose.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.posteActuel").value(DEFAULT_POSTE_ACTUEL))
            .andExpect(jsonPath("$.anciennete").value(DEFAULT_ANCIENNETE));
    }
    @Test
    @Transactional
    public void getNonExistingRessourcePropose() throws Exception {
        // Get the ressourcePropose
        restRessourceProposeMockMvc.perform(get("/api/ressource-proposes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRessourcePropose() throws Exception {
        // Initialize the database
        ressourceProposeRepository.saveAndFlush(ressourcePropose);

        int databaseSizeBeforeUpdate = ressourceProposeRepository.findAll().size();

        // Update the ressourcePropose
        RessourcePropose updatedRessourcePropose = ressourceProposeRepository.findById(ressourcePropose.getId()).get();
        // Disconnect from session so that the updates on updatedRessourcePropose are not directly saved in db
        em.detach(updatedRessourcePropose);
        updatedRessourcePropose
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .posteActuel(UPDATED_POSTE_ACTUEL)
            .anciennete(UPDATED_ANCIENNETE);
        RessourceProposeDTO ressourceProposeDTO = ressourceProposeMapper.toDto(updatedRessourcePropose);

        restRessourceProposeMockMvc.perform(put("/api/ressource-proposes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ressourceProposeDTO)))
            .andExpect(status().isOk());

        // Validate the RessourcePropose in the database
        List<RessourcePropose> ressourceProposeList = ressourceProposeRepository.findAll();
        assertThat(ressourceProposeList).hasSize(databaseSizeBeforeUpdate);
        RessourcePropose testRessourcePropose = ressourceProposeList.get(ressourceProposeList.size() - 1);
        assertThat(testRessourcePropose.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testRessourcePropose.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testRessourcePropose.getPosteActuel()).isEqualTo(UPDATED_POSTE_ACTUEL);
        assertThat(testRessourcePropose.getAnciennete()).isEqualTo(UPDATED_ANCIENNETE);
    }

    @Test
    @Transactional
    public void updateNonExistingRessourcePropose() throws Exception {
        int databaseSizeBeforeUpdate = ressourceProposeRepository.findAll().size();

        // Create the RessourcePropose
        RessourceProposeDTO ressourceProposeDTO = ressourceProposeMapper.toDto(ressourcePropose);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRessourceProposeMockMvc.perform(put("/api/ressource-proposes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ressourceProposeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RessourcePropose in the database
        List<RessourcePropose> ressourceProposeList = ressourceProposeRepository.findAll();
        assertThat(ressourceProposeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRessourcePropose() throws Exception {
        // Initialize the database
        ressourceProposeRepository.saveAndFlush(ressourcePropose);

        int databaseSizeBeforeDelete = ressourceProposeRepository.findAll().size();

        // Delete the ressourcePropose
        restRessourceProposeMockMvc.perform(delete("/api/ressource-proposes/{id}", ressourcePropose.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RessourcePropose> ressourceProposeList = ressourceProposeRepository.findAll();
        assertThat(ressourceProposeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
