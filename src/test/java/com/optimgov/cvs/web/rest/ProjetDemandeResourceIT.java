package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.GestioncvsApp;
import com.optimgov.cvs.domain.ProjetDemande;
import com.optimgov.cvs.repository.ProjetDemandeRepository;
import com.optimgov.cvs.service.ProjetDemandeService;
import com.optimgov.cvs.service.dto.ProjetDemandeDTO;
import com.optimgov.cvs.service.mapper.ProjetDemandeMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProjetDemandeResource} REST controller.
 */
@SpringBootTest(classes = GestioncvsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ProjetDemandeResourceIT {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_ENTREPRISE = "AAAAAAAAAA";
    private static final String UPDATED_ENTREPRISE = "BBBBBBBBBB";

    private static final String DEFAULT_DUREE = "AAAAAAAAAA";
    private static final String UPDATED_DUREE = "BBBBBBBBBB";

    @Autowired
    private ProjetDemandeRepository projetDemandeRepository;

    @Autowired
    private ProjetDemandeMapper projetDemandeMapper;

    @Autowired
    private ProjetDemandeService projetDemandeService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProjetDemandeMockMvc;

    private ProjetDemande projetDemande;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProjetDemande createEntity(EntityManager em) {
        ProjetDemande projetDemande = new ProjetDemande()
            .description(DEFAULT_DESCRIPTION)
            .entreprise(DEFAULT_ENTREPRISE)
            .duree(DEFAULT_DUREE);
        return projetDemande;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProjetDemande createUpdatedEntity(EntityManager em) {
        ProjetDemande projetDemande = new ProjetDemande()
            .description(UPDATED_DESCRIPTION)
            .entreprise(UPDATED_ENTREPRISE)
            .duree(UPDATED_DUREE);
        return projetDemande;
    }

    @BeforeEach
    public void initTest() {
        projetDemande = createEntity(em);
    }

    @Test
    @Transactional
    public void createProjetDemande() throws Exception {
        int databaseSizeBeforeCreate = projetDemandeRepository.findAll().size();
        // Create the ProjetDemande
        ProjetDemandeDTO projetDemandeDTO = projetDemandeMapper.toDto(projetDemande);
        restProjetDemandeMockMvc.perform(post("/api/projet-demandes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(projetDemandeDTO)))
            .andExpect(status().isCreated());

        // Validate the ProjetDemande in the database
        List<ProjetDemande> projetDemandeList = projetDemandeRepository.findAll();
        assertThat(projetDemandeList).hasSize(databaseSizeBeforeCreate + 1);
        ProjetDemande testProjetDemande = projetDemandeList.get(projetDemandeList.size() - 1);
        assertThat(testProjetDemande.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testProjetDemande.getEntreprise()).isEqualTo(DEFAULT_ENTREPRISE);
        assertThat(testProjetDemande.getDuree()).isEqualTo(DEFAULT_DUREE);
    }

    @Test
    @Transactional
    public void createProjetDemandeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = projetDemandeRepository.findAll().size();

        // Create the ProjetDemande with an existing ID
        projetDemande.setId(1L);
        ProjetDemandeDTO projetDemandeDTO = projetDemandeMapper.toDto(projetDemande);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProjetDemandeMockMvc.perform(post("/api/projet-demandes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(projetDemandeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProjetDemande in the database
        List<ProjetDemande> projetDemandeList = projetDemandeRepository.findAll();
        assertThat(projetDemandeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProjetDemandes() throws Exception {
        // Initialize the database
        projetDemandeRepository.saveAndFlush(projetDemande);

        // Get all the projetDemandeList
        restProjetDemandeMockMvc.perform(get("/api/projet-demandes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(projetDemande.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].entreprise").value(hasItem(DEFAULT_ENTREPRISE)))
            .andExpect(jsonPath("$.[*].duree").value(hasItem(DEFAULT_DUREE)));
    }
    
    @Test
    @Transactional
    public void getProjetDemande() throws Exception {
        // Initialize the database
        projetDemandeRepository.saveAndFlush(projetDemande);

        // Get the projetDemande
        restProjetDemandeMockMvc.perform(get("/api/projet-demandes/{id}", projetDemande.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(projetDemande.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.entreprise").value(DEFAULT_ENTREPRISE))
            .andExpect(jsonPath("$.duree").value(DEFAULT_DUREE));
    }
    @Test
    @Transactional
    public void getNonExistingProjetDemande() throws Exception {
        // Get the projetDemande
        restProjetDemandeMockMvc.perform(get("/api/projet-demandes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProjetDemande() throws Exception {
        // Initialize the database
        projetDemandeRepository.saveAndFlush(projetDemande);

        int databaseSizeBeforeUpdate = projetDemandeRepository.findAll().size();

        // Update the projetDemande
        ProjetDemande updatedProjetDemande = projetDemandeRepository.findById(projetDemande.getId()).get();
        // Disconnect from session so that the updates on updatedProjetDemande are not directly saved in db
        em.detach(updatedProjetDemande);
        updatedProjetDemande
            .description(UPDATED_DESCRIPTION)
            .entreprise(UPDATED_ENTREPRISE)
            .duree(UPDATED_DUREE);
        ProjetDemandeDTO projetDemandeDTO = projetDemandeMapper.toDto(updatedProjetDemande);

        restProjetDemandeMockMvc.perform(put("/api/projet-demandes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(projetDemandeDTO)))
            .andExpect(status().isOk());

        // Validate the ProjetDemande in the database
        List<ProjetDemande> projetDemandeList = projetDemandeRepository.findAll();
        assertThat(projetDemandeList).hasSize(databaseSizeBeforeUpdate);
        ProjetDemande testProjetDemande = projetDemandeList.get(projetDemandeList.size() - 1);
        assertThat(testProjetDemande.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProjetDemande.getEntreprise()).isEqualTo(UPDATED_ENTREPRISE);
        assertThat(testProjetDemande.getDuree()).isEqualTo(UPDATED_DUREE);
    }

    @Test
    @Transactional
    public void updateNonExistingProjetDemande() throws Exception {
        int databaseSizeBeforeUpdate = projetDemandeRepository.findAll().size();

        // Create the ProjetDemande
        ProjetDemandeDTO projetDemandeDTO = projetDemandeMapper.toDto(projetDemande);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProjetDemandeMockMvc.perform(put("/api/projet-demandes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(projetDemandeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProjetDemande in the database
        List<ProjetDemande> projetDemandeList = projetDemandeRepository.findAll();
        assertThat(projetDemandeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProjetDemande() throws Exception {
        // Initialize the database
        projetDemandeRepository.saveAndFlush(projetDemande);

        int databaseSizeBeforeDelete = projetDemandeRepository.findAll().size();

        // Delete the projetDemande
        restProjetDemandeMockMvc.perform(delete("/api/projet-demandes/{id}", projetDemande.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProjetDemande> projetDemandeList = projetDemandeRepository.findAll();
        assertThat(projetDemandeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
