package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.GestioncvsApp;
import com.optimgov.cvs.domain.BanquePProjet;
import com.optimgov.cvs.repository.BanquePProjetRepository;
import com.optimgov.cvs.service.BanquePProjetService;
import com.optimgov.cvs.service.dto.BanquePProjetDTO;
import com.optimgov.cvs.service.mapper.BanquePProjetMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BanquePProjetResource} REST controller.
 */
@SpringBootTest(classes = GestioncvsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class BanquePProjetResourceIT {

    @Autowired
    private BanquePProjetRepository banquePProjetRepository;

    @Autowired
    private BanquePProjetMapper banquePProjetMapper;

    @Autowired
    private BanquePProjetService banquePProjetService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBanquePProjetMockMvc;

    private BanquePProjet banquePProjet;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BanquePProjet createEntity(EntityManager em) {
        BanquePProjet banquePProjet = new BanquePProjet();
        return banquePProjet;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BanquePProjet createUpdatedEntity(EntityManager em) {
        BanquePProjet banquePProjet = new BanquePProjet();
        return banquePProjet;
    }

    @BeforeEach
    public void initTest() {
        banquePProjet = createEntity(em);
    }

    @Test
    @Transactional
    public void createBanquePProjet() throws Exception {
        int databaseSizeBeforeCreate = banquePProjetRepository.findAll().size();
        // Create the BanquePProjet
        BanquePProjetDTO banquePProjetDTO = banquePProjetMapper.toDto(banquePProjet);
        restBanquePProjetMockMvc.perform(post("/api/banque-p-projets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banquePProjetDTO)))
            .andExpect(status().isCreated());

        // Validate the BanquePProjet in the database
        List<BanquePProjet> banquePProjetList = banquePProjetRepository.findAll();
        assertThat(banquePProjetList).hasSize(databaseSizeBeforeCreate + 1);
        BanquePProjet testBanquePProjet = banquePProjetList.get(banquePProjetList.size() - 1);
    }

    @Test
    @Transactional
    public void createBanquePProjetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = banquePProjetRepository.findAll().size();

        // Create the BanquePProjet with an existing ID
        banquePProjet.setId(1L);
        BanquePProjetDTO banquePProjetDTO = banquePProjetMapper.toDto(banquePProjet);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBanquePProjetMockMvc.perform(post("/api/banque-p-projets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banquePProjetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BanquePProjet in the database
        List<BanquePProjet> banquePProjetList = banquePProjetRepository.findAll();
        assertThat(banquePProjetList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBanquePProjets() throws Exception {
        // Initialize the database
        banquePProjetRepository.saveAndFlush(banquePProjet);

        // Get all the banquePProjetList
        restBanquePProjetMockMvc.perform(get("/api/banque-p-projets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(banquePProjet.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getBanquePProjet() throws Exception {
        // Initialize the database
        banquePProjetRepository.saveAndFlush(banquePProjet);

        // Get the banquePProjet
        restBanquePProjetMockMvc.perform(get("/api/banque-p-projets/{id}", banquePProjet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(banquePProjet.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingBanquePProjet() throws Exception {
        // Get the banquePProjet
        restBanquePProjetMockMvc.perform(get("/api/banque-p-projets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBanquePProjet() throws Exception {
        // Initialize the database
        banquePProjetRepository.saveAndFlush(banquePProjet);

        int databaseSizeBeforeUpdate = banquePProjetRepository.findAll().size();

        // Update the banquePProjet
        BanquePProjet updatedBanquePProjet = banquePProjetRepository.findById(banquePProjet.getId()).get();
        // Disconnect from session so that the updates on updatedBanquePProjet are not directly saved in db
        em.detach(updatedBanquePProjet);
        BanquePProjetDTO banquePProjetDTO = banquePProjetMapper.toDto(updatedBanquePProjet);

        restBanquePProjetMockMvc.perform(put("/api/banque-p-projets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banquePProjetDTO)))
            .andExpect(status().isOk());

        // Validate the BanquePProjet in the database
        List<BanquePProjet> banquePProjetList = banquePProjetRepository.findAll();
        assertThat(banquePProjetList).hasSize(databaseSizeBeforeUpdate);
        BanquePProjet testBanquePProjet = banquePProjetList.get(banquePProjetList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingBanquePProjet() throws Exception {
        int databaseSizeBeforeUpdate = banquePProjetRepository.findAll().size();

        // Create the BanquePProjet
        BanquePProjetDTO banquePProjetDTO = banquePProjetMapper.toDto(banquePProjet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBanquePProjetMockMvc.perform(put("/api/banque-p-projets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banquePProjetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BanquePProjet in the database
        List<BanquePProjet> banquePProjetList = banquePProjetRepository.findAll();
        assertThat(banquePProjetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBanquePProjet() throws Exception {
        // Initialize the database
        banquePProjetRepository.saveAndFlush(banquePProjet);

        int databaseSizeBeforeDelete = banquePProjetRepository.findAll().size();

        // Delete the banquePProjet
        restBanquePProjetMockMvc.perform(delete("/api/banque-p-projets/{id}", banquePProjet.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BanquePProjet> banquePProjetList = banquePProjetRepository.findAll();
        assertThat(banquePProjetList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
