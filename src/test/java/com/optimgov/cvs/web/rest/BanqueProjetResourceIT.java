package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.GestioncvsApp;
import com.optimgov.cvs.domain.BanqueProjet;
import com.optimgov.cvs.repository.BanqueProjetRepository;
import com.optimgov.cvs.service.BanqueProjetService;
import com.optimgov.cvs.service.dto.BanqueProjetDTO;
import com.optimgov.cvs.service.mapper.BanqueProjetMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BanqueProjetResource} REST controller.
 */
@SpringBootTest(classes = GestioncvsApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class BanqueProjetResourceIT {

    private static final String DEFAULT_DESIGNATION = "AAAAAAAAAA";
    private static final String UPDATED_DESIGNATION = "BBBBBBBBBB";

    private static final String DEFAULT_ORGANISME = "AAAAAAAAAA";
    private static final String UPDATED_ORGANISME = "BBBBBBBBBB";

    private static final String DEFAULT_BUDGET = "AAAAAAAAAA";
    private static final String UPDATED_BUDGET = "BBBBBBBBBB";

    private static final Instant DEFAULT_ANNEE_PROJET = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_ANNEE_PROJET = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_DUREE = "AAAAAAAAAA";
    private static final String UPDATED_DUREE = "BBBBBBBBBB";

    private static final String DEFAULT_RESULTAT = "AAAAAAAAAA";
    private static final String UPDATED_RESULTAT = "BBBBBBBBBB";

    private static final String DEFAULT_MISSION = "AAAAAAAAAA";
    private static final String UPDATED_MISSION = "BBBBBBBBBB";

    @Autowired
    private BanqueProjetRepository banqueProjetRepository;

    @Mock
    private BanqueProjetRepository banqueProjetRepositoryMock;

    @Autowired
    private BanqueProjetMapper banqueProjetMapper;

    @Mock
    private BanqueProjetService banqueProjetServiceMock;

    @Autowired
    private BanqueProjetService banqueProjetService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBanqueProjetMockMvc;

    private BanqueProjet banqueProjet;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BanqueProjet createEntity(EntityManager em) {
        BanqueProjet banqueProjet = new BanqueProjet()
            .designation(DEFAULT_DESIGNATION)
            .organisme(DEFAULT_ORGANISME)
            .budget(DEFAULT_BUDGET)
            .anneeProjet(DEFAULT_ANNEE_PROJET)
            .duree(DEFAULT_DUREE)
            .resultat(DEFAULT_RESULTAT)
            .mission(DEFAULT_MISSION);
        return banqueProjet;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BanqueProjet createUpdatedEntity(EntityManager em) {
        BanqueProjet banqueProjet = new BanqueProjet()
            .designation(UPDATED_DESIGNATION)
            .organisme(UPDATED_ORGANISME)
            .budget(UPDATED_BUDGET)
            .anneeProjet(UPDATED_ANNEE_PROJET)
            .duree(UPDATED_DUREE)
            .resultat(UPDATED_RESULTAT)
            .mission(UPDATED_MISSION);
        return banqueProjet;
    }

    @BeforeEach
    public void initTest() {
        banqueProjet = createEntity(em);
    }

    @Test
    @Transactional
    public void createBanqueProjet() throws Exception {
        int databaseSizeBeforeCreate = banqueProjetRepository.findAll().size();
        // Create the BanqueProjet
        BanqueProjetDTO banqueProjetDTO = banqueProjetMapper.toDto(banqueProjet);
        restBanqueProjetMockMvc.perform(post("/api/banque-projets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banqueProjetDTO)))
            .andExpect(status().isCreated());

        // Validate the BanqueProjet in the database
        List<BanqueProjet> banqueProjetList = banqueProjetRepository.findAll();
        assertThat(banqueProjetList).hasSize(databaseSizeBeforeCreate + 1);
        BanqueProjet testBanqueProjet = banqueProjetList.get(banqueProjetList.size() - 1);
        assertThat(testBanqueProjet.getDesignation()).isEqualTo(DEFAULT_DESIGNATION);
        assertThat(testBanqueProjet.getOrganisme()).isEqualTo(DEFAULT_ORGANISME);
        assertThat(testBanqueProjet.getBudget()).isEqualTo(DEFAULT_BUDGET);
        assertThat(testBanqueProjet.getAnneeProjet()).isEqualTo(DEFAULT_ANNEE_PROJET);
        assertThat(testBanqueProjet.getDuree()).isEqualTo(DEFAULT_DUREE);
        assertThat(testBanqueProjet.getResultat()).isEqualTo(DEFAULT_RESULTAT);
        assertThat(testBanqueProjet.getMission()).isEqualTo(DEFAULT_MISSION);
    }

    @Test
    @Transactional
    public void createBanqueProjetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = banqueProjetRepository.findAll().size();

        // Create the BanqueProjet with an existing ID
        banqueProjet.setId(1L);
        BanqueProjetDTO banqueProjetDTO = banqueProjetMapper.toDto(banqueProjet);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBanqueProjetMockMvc.perform(post("/api/banque-projets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banqueProjetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BanqueProjet in the database
        List<BanqueProjet> banqueProjetList = banqueProjetRepository.findAll();
        assertThat(banqueProjetList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBanqueProjets() throws Exception {
        // Initialize the database
        banqueProjetRepository.saveAndFlush(banqueProjet);

        // Get all the banqueProjetList
        restBanqueProjetMockMvc.perform(get("/api/banque-projets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(banqueProjet.getId().intValue())))
            .andExpect(jsonPath("$.[*].designation").value(hasItem(DEFAULT_DESIGNATION)))
            .andExpect(jsonPath("$.[*].organisme").value(hasItem(DEFAULT_ORGANISME)))
            .andExpect(jsonPath("$.[*].budget").value(hasItem(DEFAULT_BUDGET)))
            .andExpect(jsonPath("$.[*].anneeProjet").value(hasItem(DEFAULT_ANNEE_PROJET.toString())))
            .andExpect(jsonPath("$.[*].duree").value(hasItem(DEFAULT_DUREE)))
            .andExpect(jsonPath("$.[*].resultat").value(hasItem(DEFAULT_RESULTAT)))
            .andExpect(jsonPath("$.[*].mission").value(hasItem(DEFAULT_MISSION)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllBanqueProjetsWithEagerRelationshipsIsEnabled() throws Exception {
        when(banqueProjetServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restBanqueProjetMockMvc.perform(get("/api/banque-projets?eagerload=true"))
            .andExpect(status().isOk());

        verify(banqueProjetServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllBanqueProjetsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(banqueProjetServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restBanqueProjetMockMvc.perform(get("/api/banque-projets?eagerload=true"))
            .andExpect(status().isOk());

        verify(banqueProjetServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getBanqueProjet() throws Exception {
        // Initialize the database
        banqueProjetRepository.saveAndFlush(banqueProjet);

        // Get the banqueProjet
        restBanqueProjetMockMvc.perform(get("/api/banque-projets/{id}", banqueProjet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(banqueProjet.getId().intValue()))
            .andExpect(jsonPath("$.designation").value(DEFAULT_DESIGNATION))
            .andExpect(jsonPath("$.organisme").value(DEFAULT_ORGANISME))
            .andExpect(jsonPath("$.budget").value(DEFAULT_BUDGET))
            .andExpect(jsonPath("$.anneeProjet").value(DEFAULT_ANNEE_PROJET.toString()))
            .andExpect(jsonPath("$.duree").value(DEFAULT_DUREE))
            .andExpect(jsonPath("$.resultat").value(DEFAULT_RESULTAT))
            .andExpect(jsonPath("$.mission").value(DEFAULT_MISSION));
    }
    @Test
    @Transactional
    public void getNonExistingBanqueProjet() throws Exception {
        // Get the banqueProjet
        restBanqueProjetMockMvc.perform(get("/api/banque-projets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBanqueProjet() throws Exception {
        // Initialize the database
        banqueProjetRepository.saveAndFlush(banqueProjet);

        int databaseSizeBeforeUpdate = banqueProjetRepository.findAll().size();

        // Update the banqueProjet
        BanqueProjet updatedBanqueProjet = banqueProjetRepository.findById(banqueProjet.getId()).get();
        // Disconnect from session so that the updates on updatedBanqueProjet are not directly saved in db
        em.detach(updatedBanqueProjet);
        updatedBanqueProjet
            .designation(UPDATED_DESIGNATION)
            .organisme(UPDATED_ORGANISME)
            .budget(UPDATED_BUDGET)
            .anneeProjet(UPDATED_ANNEE_PROJET)
            .duree(UPDATED_DUREE)
            .resultat(UPDATED_RESULTAT)
            .mission(UPDATED_MISSION);
        BanqueProjetDTO banqueProjetDTO = banqueProjetMapper.toDto(updatedBanqueProjet);

        restBanqueProjetMockMvc.perform(put("/api/banque-projets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banqueProjetDTO)))
            .andExpect(status().isOk());

        // Validate the BanqueProjet in the database
        List<BanqueProjet> banqueProjetList = banqueProjetRepository.findAll();
        assertThat(banqueProjetList).hasSize(databaseSizeBeforeUpdate);
        BanqueProjet testBanqueProjet = banqueProjetList.get(banqueProjetList.size() - 1);
        assertThat(testBanqueProjet.getDesignation()).isEqualTo(UPDATED_DESIGNATION);
        assertThat(testBanqueProjet.getOrganisme()).isEqualTo(UPDATED_ORGANISME);
        assertThat(testBanqueProjet.getBudget()).isEqualTo(UPDATED_BUDGET);
        assertThat(testBanqueProjet.getAnneeProjet()).isEqualTo(UPDATED_ANNEE_PROJET);
        assertThat(testBanqueProjet.getDuree()).isEqualTo(UPDATED_DUREE);
        assertThat(testBanqueProjet.getResultat()).isEqualTo(UPDATED_RESULTAT);
        assertThat(testBanqueProjet.getMission()).isEqualTo(UPDATED_MISSION);
    }

    @Test
    @Transactional
    public void updateNonExistingBanqueProjet() throws Exception {
        int databaseSizeBeforeUpdate = banqueProjetRepository.findAll().size();

        // Create the BanqueProjet
        BanqueProjetDTO banqueProjetDTO = banqueProjetMapper.toDto(banqueProjet);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBanqueProjetMockMvc.perform(put("/api/banque-projets")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banqueProjetDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BanqueProjet in the database
        List<BanqueProjet> banqueProjetList = banqueProjetRepository.findAll();
        assertThat(banqueProjetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBanqueProjet() throws Exception {
        // Initialize the database
        banqueProjetRepository.saveAndFlush(banqueProjet);

        int databaseSizeBeforeDelete = banqueProjetRepository.findAll().size();

        // Delete the banqueProjet
        restBanqueProjetMockMvc.perform(delete("/api/banque-projets/{id}", banqueProjet.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BanqueProjet> banqueProjetList = banqueProjetRepository.findAll();
        assertThat(banqueProjetList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
