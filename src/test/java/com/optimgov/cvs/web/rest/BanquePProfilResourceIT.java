package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.GestioncvsApp;
import com.optimgov.cvs.domain.BanquePProfil;
import com.optimgov.cvs.repository.BanquePProfilRepository;
import com.optimgov.cvs.service.BanquePProfilService;
import com.optimgov.cvs.service.dto.BanquePProfilDTO;
import com.optimgov.cvs.service.mapper.BanquePProfilMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BanquePProfilResource} REST controller.
 */
@SpringBootTest(classes = GestioncvsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class BanquePProfilResourceIT {

    @Autowired
    private BanquePProfilRepository banquePProfilRepository;

    @Autowired
    private BanquePProfilMapper banquePProfilMapper;

    @Autowired
    private BanquePProfilService banquePProfilService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBanquePProfilMockMvc;

    private BanquePProfil banquePProfil;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BanquePProfil createEntity(EntityManager em) {
        BanquePProfil banquePProfil = new BanquePProfil();
        return banquePProfil;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BanquePProfil createUpdatedEntity(EntityManager em) {
        BanquePProfil banquePProfil = new BanquePProfil();
        return banquePProfil;
    }

    @BeforeEach
    public void initTest() {
        banquePProfil = createEntity(em);
    }

    @Test
    @Transactional
    public void createBanquePProfil() throws Exception {
        int databaseSizeBeforeCreate = banquePProfilRepository.findAll().size();
        // Create the BanquePProfil
        BanquePProfilDTO banquePProfilDTO = banquePProfilMapper.toDto(banquePProfil);
        restBanquePProfilMockMvc.perform(post("/api/banque-p-profils")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banquePProfilDTO)))
            .andExpect(status().isCreated());

        // Validate the BanquePProfil in the database
        List<BanquePProfil> banquePProfilList = banquePProfilRepository.findAll();
        assertThat(banquePProfilList).hasSize(databaseSizeBeforeCreate + 1);
        BanquePProfil testBanquePProfil = banquePProfilList.get(banquePProfilList.size() - 1);
    }

    @Test
    @Transactional
    public void createBanquePProfilWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = banquePProfilRepository.findAll().size();

        // Create the BanquePProfil with an existing ID
        banquePProfil.setId(1L);
        BanquePProfilDTO banquePProfilDTO = banquePProfilMapper.toDto(banquePProfil);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBanquePProfilMockMvc.perform(post("/api/banque-p-profils")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banquePProfilDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BanquePProfil in the database
        List<BanquePProfil> banquePProfilList = banquePProfilRepository.findAll();
        assertThat(banquePProfilList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBanquePProfils() throws Exception {
        // Initialize the database
        banquePProfilRepository.saveAndFlush(banquePProfil);

        // Get all the banquePProfilList
        restBanquePProfilMockMvc.perform(get("/api/banque-p-profils?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(banquePProfil.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getBanquePProfil() throws Exception {
        // Initialize the database
        banquePProfilRepository.saveAndFlush(banquePProfil);

        // Get the banquePProfil
        restBanquePProfilMockMvc.perform(get("/api/banque-p-profils/{id}", banquePProfil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(banquePProfil.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingBanquePProfil() throws Exception {
        // Get the banquePProfil
        restBanquePProfilMockMvc.perform(get("/api/banque-p-profils/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBanquePProfil() throws Exception {
        // Initialize the database
        banquePProfilRepository.saveAndFlush(banquePProfil);

        int databaseSizeBeforeUpdate = banquePProfilRepository.findAll().size();

        // Update the banquePProfil
        BanquePProfil updatedBanquePProfil = banquePProfilRepository.findById(banquePProfil.getId()).get();
        // Disconnect from session so that the updates on updatedBanquePProfil are not directly saved in db
        em.detach(updatedBanquePProfil);
        BanquePProfilDTO banquePProfilDTO = banquePProfilMapper.toDto(updatedBanquePProfil);

        restBanquePProfilMockMvc.perform(put("/api/banque-p-profils")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banquePProfilDTO)))
            .andExpect(status().isOk());

        // Validate the BanquePProfil in the database
        List<BanquePProfil> banquePProfilList = banquePProfilRepository.findAll();
        assertThat(banquePProfilList).hasSize(databaseSizeBeforeUpdate);
        BanquePProfil testBanquePProfil = banquePProfilList.get(banquePProfilList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingBanquePProfil() throws Exception {
        int databaseSizeBeforeUpdate = banquePProfilRepository.findAll().size();

        // Create the BanquePProfil
        BanquePProfilDTO banquePProfilDTO = banquePProfilMapper.toDto(banquePProfil);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBanquePProfilMockMvc.perform(put("/api/banque-p-profils")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(banquePProfilDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BanquePProfil in the database
        List<BanquePProfil> banquePProfilList = banquePProfilRepository.findAll();
        assertThat(banquePProfilList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBanquePProfil() throws Exception {
        // Initialize the database
        banquePProfilRepository.saveAndFlush(banquePProfil);

        int databaseSizeBeforeDelete = banquePProfilRepository.findAll().size();

        // Delete the banquePProfil
        restBanquePProfilMockMvc.perform(delete("/api/banque-p-profils/{id}", banquePProfil.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BanquePProfil> banquePProfilList = banquePProfilRepository.findAll();
        assertThat(banquePProfilList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
