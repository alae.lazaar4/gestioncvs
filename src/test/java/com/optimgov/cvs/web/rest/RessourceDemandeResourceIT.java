package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.GestioncvsApp;
import com.optimgov.cvs.domain.RessourceDemande;
import com.optimgov.cvs.repository.RessourceDemandeRepository;
import com.optimgov.cvs.service.RessourceDemandeService;
import com.optimgov.cvs.service.dto.RessourceDemandeDTO;
import com.optimgov.cvs.service.mapper.RessourceDemandeMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RessourceDemandeResource} REST controller.
 */
@SpringBootTest(classes = GestioncvsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class RessourceDemandeResourceIT {

    private static final String DEFAULT_EMPLOI_ACTUEL = "AAAAAAAAAA";
    private static final String UPDATED_EMPLOI_ACTUEL = "BBBBBBBBBB";

    private static final String DEFAULT_ANCIENNETE_ETUDE = "AAAAAAAAAA";
    private static final String UPDATED_ANCIENNETE_ETUDE = "BBBBBBBBBB";

    private static final String DEFAULT_ANCIENNETE_EMPLOI = "AAAAAAAAAA";
    private static final String UPDATED_ANCIENNETE_EMPLOI = "BBBBBBBBBB";

    private static final String DEFAULT_FONCTION = "AAAAAAAAAA";
    private static final String UPDATED_FONCTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_NB_RESSOURCES = 1;
    private static final Integer UPDATED_NB_RESSOURCES = 2;

    @Autowired
    private RessourceDemandeRepository ressourceDemandeRepository;

    @Autowired
    private RessourceDemandeMapper ressourceDemandeMapper;

    @Autowired
    private RessourceDemandeService ressourceDemandeService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRessourceDemandeMockMvc;

    private RessourceDemande ressourceDemande;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RessourceDemande createEntity(EntityManager em) {
        RessourceDemande ressourceDemande = new RessourceDemande()
            .emploiActuel(DEFAULT_EMPLOI_ACTUEL)
            .ancienneteEtude(DEFAULT_ANCIENNETE_ETUDE)
            .ancienneteEmploi(DEFAULT_ANCIENNETE_EMPLOI)
            .fonction(DEFAULT_FONCTION)
            .nbRessources(DEFAULT_NB_RESSOURCES);
        return ressourceDemande;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RessourceDemande createUpdatedEntity(EntityManager em) {
        RessourceDemande ressourceDemande = new RessourceDemande()
            .emploiActuel(UPDATED_EMPLOI_ACTUEL)
            .ancienneteEtude(UPDATED_ANCIENNETE_ETUDE)
            .ancienneteEmploi(UPDATED_ANCIENNETE_EMPLOI)
            .fonction(UPDATED_FONCTION)
            .nbRessources(UPDATED_NB_RESSOURCES);
        return ressourceDemande;
    }

    @BeforeEach
    public void initTest() {
        ressourceDemande = createEntity(em);
    }

    @Test
    @Transactional
    public void createRessourceDemande() throws Exception {
        int databaseSizeBeforeCreate = ressourceDemandeRepository.findAll().size();
        // Create the RessourceDemande
        RessourceDemandeDTO ressourceDemandeDTO = ressourceDemandeMapper.toDto(ressourceDemande);
        restRessourceDemandeMockMvc.perform(post("/api/ressource-demandes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ressourceDemandeDTO)))
            .andExpect(status().isCreated());

        // Validate the RessourceDemande in the database
        List<RessourceDemande> ressourceDemandeList = ressourceDemandeRepository.findAll();
        assertThat(ressourceDemandeList).hasSize(databaseSizeBeforeCreate + 1);
        RessourceDemande testRessourceDemande = ressourceDemandeList.get(ressourceDemandeList.size() - 1);
        assertThat(testRessourceDemande.getEmploiActuel()).isEqualTo(DEFAULT_EMPLOI_ACTUEL);
        assertThat(testRessourceDemande.getAncienneteEtude()).isEqualTo(DEFAULT_ANCIENNETE_ETUDE);
        assertThat(testRessourceDemande.getAncienneteEmploi()).isEqualTo(DEFAULT_ANCIENNETE_EMPLOI);
        assertThat(testRessourceDemande.getFonction()).isEqualTo(DEFAULT_FONCTION);
        assertThat(testRessourceDemande.getNbRessources()).isEqualTo(DEFAULT_NB_RESSOURCES);
    }

    @Test
    @Transactional
    public void createRessourceDemandeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ressourceDemandeRepository.findAll().size();

        // Create the RessourceDemande with an existing ID
        ressourceDemande.setId(1L);
        RessourceDemandeDTO ressourceDemandeDTO = ressourceDemandeMapper.toDto(ressourceDemande);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRessourceDemandeMockMvc.perform(post("/api/ressource-demandes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ressourceDemandeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RessourceDemande in the database
        List<RessourceDemande> ressourceDemandeList = ressourceDemandeRepository.findAll();
        assertThat(ressourceDemandeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRessourceDemandes() throws Exception {
        // Initialize the database
        ressourceDemandeRepository.saveAndFlush(ressourceDemande);

        // Get all the ressourceDemandeList
        restRessourceDemandeMockMvc.perform(get("/api/ressource-demandes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ressourceDemande.getId().intValue())))
            .andExpect(jsonPath("$.[*].emploiActuel").value(hasItem(DEFAULT_EMPLOI_ACTUEL)))
            .andExpect(jsonPath("$.[*].ancienneteEtude").value(hasItem(DEFAULT_ANCIENNETE_ETUDE)))
            .andExpect(jsonPath("$.[*].ancienneteEmploi").value(hasItem(DEFAULT_ANCIENNETE_EMPLOI)))
            .andExpect(jsonPath("$.[*].fonction").value(hasItem(DEFAULT_FONCTION)))
            .andExpect(jsonPath("$.[*].nbRessources").value(hasItem(DEFAULT_NB_RESSOURCES)));
    }
    
    @Test
    @Transactional
    public void getRessourceDemande() throws Exception {
        // Initialize the database
        ressourceDemandeRepository.saveAndFlush(ressourceDemande);

        // Get the ressourceDemande
        restRessourceDemandeMockMvc.perform(get("/api/ressource-demandes/{id}", ressourceDemande.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ressourceDemande.getId().intValue()))
            .andExpect(jsonPath("$.emploiActuel").value(DEFAULT_EMPLOI_ACTUEL))
            .andExpect(jsonPath("$.ancienneteEtude").value(DEFAULT_ANCIENNETE_ETUDE))
            .andExpect(jsonPath("$.ancienneteEmploi").value(DEFAULT_ANCIENNETE_EMPLOI))
            .andExpect(jsonPath("$.fonction").value(DEFAULT_FONCTION))
            .andExpect(jsonPath("$.nbRessources").value(DEFAULT_NB_RESSOURCES));
    }
    @Test
    @Transactional
    public void getNonExistingRessourceDemande() throws Exception {
        // Get the ressourceDemande
        restRessourceDemandeMockMvc.perform(get("/api/ressource-demandes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRessourceDemande() throws Exception {
        // Initialize the database
        ressourceDemandeRepository.saveAndFlush(ressourceDemande);

        int databaseSizeBeforeUpdate = ressourceDemandeRepository.findAll().size();

        // Update the ressourceDemande
        RessourceDemande updatedRessourceDemande = ressourceDemandeRepository.findById(ressourceDemande.getId()).get();
        // Disconnect from session so that the updates on updatedRessourceDemande are not directly saved in db
        em.detach(updatedRessourceDemande);
        updatedRessourceDemande
            .emploiActuel(UPDATED_EMPLOI_ACTUEL)
            .ancienneteEtude(UPDATED_ANCIENNETE_ETUDE)
            .ancienneteEmploi(UPDATED_ANCIENNETE_EMPLOI)
            .fonction(UPDATED_FONCTION)
            .nbRessources(UPDATED_NB_RESSOURCES);
        RessourceDemandeDTO ressourceDemandeDTO = ressourceDemandeMapper.toDto(updatedRessourceDemande);

        restRessourceDemandeMockMvc.perform(put("/api/ressource-demandes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ressourceDemandeDTO)))
            .andExpect(status().isOk());

        // Validate the RessourceDemande in the database
        List<RessourceDemande> ressourceDemandeList = ressourceDemandeRepository.findAll();
        assertThat(ressourceDemandeList).hasSize(databaseSizeBeforeUpdate);
        RessourceDemande testRessourceDemande = ressourceDemandeList.get(ressourceDemandeList.size() - 1);
        assertThat(testRessourceDemande.getEmploiActuel()).isEqualTo(UPDATED_EMPLOI_ACTUEL);
        assertThat(testRessourceDemande.getAncienneteEtude()).isEqualTo(UPDATED_ANCIENNETE_ETUDE);
        assertThat(testRessourceDemande.getAncienneteEmploi()).isEqualTo(UPDATED_ANCIENNETE_EMPLOI);
        assertThat(testRessourceDemande.getFonction()).isEqualTo(UPDATED_FONCTION);
        assertThat(testRessourceDemande.getNbRessources()).isEqualTo(UPDATED_NB_RESSOURCES);
    }

    @Test
    @Transactional
    public void updateNonExistingRessourceDemande() throws Exception {
        int databaseSizeBeforeUpdate = ressourceDemandeRepository.findAll().size();

        // Create the RessourceDemande
        RessourceDemandeDTO ressourceDemandeDTO = ressourceDemandeMapper.toDto(ressourceDemande);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRessourceDemandeMockMvc.perform(put("/api/ressource-demandes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(ressourceDemandeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RessourceDemande in the database
        List<RessourceDemande> ressourceDemandeList = ressourceDemandeRepository.findAll();
        assertThat(ressourceDemandeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRessourceDemande() throws Exception {
        // Initialize the database
        ressourceDemandeRepository.saveAndFlush(ressourceDemande);

        int databaseSizeBeforeDelete = ressourceDemandeRepository.findAll().size();

        // Delete the ressourceDemande
        restRessourceDemandeMockMvc.perform(delete("/api/ressource-demandes/{id}", ressourceDemande.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RessourceDemande> ressourceDemandeList = ressourceDemandeRepository.findAll();
        assertThat(ressourceDemandeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
