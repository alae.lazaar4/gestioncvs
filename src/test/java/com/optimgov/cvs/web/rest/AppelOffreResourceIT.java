package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.GestioncvsApp;
import com.optimgov.cvs.domain.AppelOffre;
import com.optimgov.cvs.repository.AppelOffreRepository;
import com.optimgov.cvs.service.AppelOffreService;
import com.optimgov.cvs.service.dto.AppelOffreDTO;
import com.optimgov.cvs.service.mapper.AppelOffreMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AppelOffreResource} REST controller.
 */
@SpringBootTest(classes = GestioncvsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AppelOffreResourceIT {

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_BUDGET = "AAAAAAAAAA";
    private static final String UPDATED_BUDGET = "BBBBBBBBBB";

    private static final Integer DEFAULT_DUREE = 1;
    private static final Integer UPDATED_DUREE = 2;

    private static final String DEFAULT_CONTEXT = "AAAAAAAAAA";
    private static final String UPDATED_CONTEXT = "BBBBBBBBBB";

    @Autowired
    private AppelOffreRepository appelOffreRepository;

    @Autowired
    private AppelOffreMapper appelOffreMapper;

    @Autowired
    private AppelOffreService appelOffreService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAppelOffreMockMvc;

    private AppelOffre appelOffre;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AppelOffre createEntity(EntityManager em) {
        AppelOffre appelOffre = new AppelOffre()
            .date(DEFAULT_DATE)
            .budget(DEFAULT_BUDGET)
            .duree(DEFAULT_DUREE)
            .context(DEFAULT_CONTEXT);
        return appelOffre;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AppelOffre createUpdatedEntity(EntityManager em) {
        AppelOffre appelOffre = new AppelOffre()
            .date(UPDATED_DATE)
            .budget(UPDATED_BUDGET)
            .duree(UPDATED_DUREE)
            .context(UPDATED_CONTEXT);
        return appelOffre;
    }

    @BeforeEach
    public void initTest() {
        appelOffre = createEntity(em);
    }

    @Test
    @Transactional
    public void createAppelOffre() throws Exception {
        int databaseSizeBeforeCreate = appelOffreRepository.findAll().size();
        // Create the AppelOffre
        AppelOffreDTO appelOffreDTO = appelOffreMapper.toDto(appelOffre);
        restAppelOffreMockMvc.perform(post("/api/appel-offres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(appelOffreDTO)))
            .andExpect(status().isCreated());

        // Validate the AppelOffre in the database
        List<AppelOffre> appelOffreList = appelOffreRepository.findAll();
        assertThat(appelOffreList).hasSize(databaseSizeBeforeCreate + 1);
        AppelOffre testAppelOffre = appelOffreList.get(appelOffreList.size() - 1);
        assertThat(testAppelOffre.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testAppelOffre.getBudget()).isEqualTo(DEFAULT_BUDGET);
        assertThat(testAppelOffre.getDuree()).isEqualTo(DEFAULT_DUREE);
        assertThat(testAppelOffre.getContext()).isEqualTo(DEFAULT_CONTEXT);
    }

    @Test
    @Transactional
    public void createAppelOffreWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = appelOffreRepository.findAll().size();

        // Create the AppelOffre with an existing ID
        appelOffre.setId(1L);
        AppelOffreDTO appelOffreDTO = appelOffreMapper.toDto(appelOffre);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAppelOffreMockMvc.perform(post("/api/appel-offres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(appelOffreDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AppelOffre in the database
        List<AppelOffre> appelOffreList = appelOffreRepository.findAll();
        assertThat(appelOffreList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAppelOffres() throws Exception {
        // Initialize the database
        appelOffreRepository.saveAndFlush(appelOffre);

        // Get all the appelOffreList
        restAppelOffreMockMvc.perform(get("/api/appel-offres?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(appelOffre.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].budget").value(hasItem(DEFAULT_BUDGET)))
            .andExpect(jsonPath("$.[*].duree").value(hasItem(DEFAULT_DUREE)))
            .andExpect(jsonPath("$.[*].context").value(hasItem(DEFAULT_CONTEXT)));
    }
    
    @Test
    @Transactional
    public void getAppelOffre() throws Exception {
        // Initialize the database
        appelOffreRepository.saveAndFlush(appelOffre);

        // Get the appelOffre
        restAppelOffreMockMvc.perform(get("/api/appel-offres/{id}", appelOffre.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(appelOffre.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.budget").value(DEFAULT_BUDGET))
            .andExpect(jsonPath("$.duree").value(DEFAULT_DUREE))
            .andExpect(jsonPath("$.context").value(DEFAULT_CONTEXT));
    }
    @Test
    @Transactional
    public void getNonExistingAppelOffre() throws Exception {
        // Get the appelOffre
        restAppelOffreMockMvc.perform(get("/api/appel-offres/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAppelOffre() throws Exception {
        // Initialize the database
        appelOffreRepository.saveAndFlush(appelOffre);

        int databaseSizeBeforeUpdate = appelOffreRepository.findAll().size();

        // Update the appelOffre
        AppelOffre updatedAppelOffre = appelOffreRepository.findById(appelOffre.getId()).get();
        // Disconnect from session so that the updates on updatedAppelOffre are not directly saved in db
        em.detach(updatedAppelOffre);
        updatedAppelOffre
            .date(UPDATED_DATE)
            .budget(UPDATED_BUDGET)
            .duree(UPDATED_DUREE)
            .context(UPDATED_CONTEXT);
        AppelOffreDTO appelOffreDTO = appelOffreMapper.toDto(updatedAppelOffre);

        restAppelOffreMockMvc.perform(put("/api/appel-offres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(appelOffreDTO)))
            .andExpect(status().isOk());

        // Validate the AppelOffre in the database
        List<AppelOffre> appelOffreList = appelOffreRepository.findAll();
        assertThat(appelOffreList).hasSize(databaseSizeBeforeUpdate);
        AppelOffre testAppelOffre = appelOffreList.get(appelOffreList.size() - 1);
        assertThat(testAppelOffre.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testAppelOffre.getBudget()).isEqualTo(UPDATED_BUDGET);
        assertThat(testAppelOffre.getDuree()).isEqualTo(UPDATED_DUREE);
        assertThat(testAppelOffre.getContext()).isEqualTo(UPDATED_CONTEXT);
    }

    @Test
    @Transactional
    public void updateNonExistingAppelOffre() throws Exception {
        int databaseSizeBeforeUpdate = appelOffreRepository.findAll().size();

        // Create the AppelOffre
        AppelOffreDTO appelOffreDTO = appelOffreMapper.toDto(appelOffre);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAppelOffreMockMvc.perform(put("/api/appel-offres")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(appelOffreDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AppelOffre in the database
        List<AppelOffre> appelOffreList = appelOffreRepository.findAll();
        assertThat(appelOffreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAppelOffre() throws Exception {
        // Initialize the database
        appelOffreRepository.saveAndFlush(appelOffre);

        int databaseSizeBeforeDelete = appelOffreRepository.findAll().size();

        // Delete the appelOffre
        restAppelOffreMockMvc.perform(delete("/api/appel-offres/{id}", appelOffre.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AppelOffre> appelOffreList = appelOffreRepository.findAll();
        assertThat(appelOffreList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
