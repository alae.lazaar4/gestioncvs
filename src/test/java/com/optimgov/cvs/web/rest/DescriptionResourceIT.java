package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.GestioncvsApp;
import com.optimgov.cvs.domain.Description;
import com.optimgov.cvs.repository.DescriptionRepository;
import com.optimgov.cvs.service.DescriptionService;
import com.optimgov.cvs.service.dto.DescriptionDTO;
import com.optimgov.cvs.service.mapper.DescriptionMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DescriptionResource} REST controller.
 */
@SpringBootTest(classes = GestioncvsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class DescriptionResourceIT {

    private static final String DEFAULT_CONTEXT = "AAAAAAAAAA";
    private static final String UPDATED_CONTEXT = "BBBBBBBBBB";

    private static final String DEFAULT_MISSION = "AAAAAAAAAA";
    private static final String UPDATED_MISSION = "BBBBBBBBBB";

    private static final String DEFAULT_ENVIRONNEMENT_TECHNIQUE = "AAAAAAAAAA";
    private static final String UPDATED_ENVIRONNEMENT_TECHNIQUE = "BBBBBBBBBB";

    @Autowired
    private DescriptionRepository descriptionRepository;

    @Autowired
    private DescriptionMapper descriptionMapper;

    @Autowired
    private DescriptionService descriptionService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDescriptionMockMvc;

    private Description description;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Description createEntity(EntityManager em) {
        Description description = new Description()
            .context(DEFAULT_CONTEXT)
            .mission(DEFAULT_MISSION)
            .environnementTechnique(DEFAULT_ENVIRONNEMENT_TECHNIQUE);
        return description;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Description createUpdatedEntity(EntityManager em) {
        Description description = new Description()
            .context(UPDATED_CONTEXT)
            .mission(UPDATED_MISSION)
            .environnementTechnique(UPDATED_ENVIRONNEMENT_TECHNIQUE);
        return description;
    }

    @BeforeEach
    public void initTest() {
        description = createEntity(em);
    }

    @Test
    @Transactional
    public void createDescription() throws Exception {
        int databaseSizeBeforeCreate = descriptionRepository.findAll().size();
        // Create the Description
        DescriptionDTO descriptionDTO = descriptionMapper.toDto(description);
        restDescriptionMockMvc.perform(post("/api/descriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(descriptionDTO)))
            .andExpect(status().isCreated());

        // Validate the Description in the database
        List<Description> descriptionList = descriptionRepository.findAll();
        assertThat(descriptionList).hasSize(databaseSizeBeforeCreate + 1);
        Description testDescription = descriptionList.get(descriptionList.size() - 1);
        assertThat(testDescription.getContext()).isEqualTo(DEFAULT_CONTEXT);
        assertThat(testDescription.getMission()).isEqualTo(DEFAULT_MISSION);
        assertThat(testDescription.getEnvironnementTechnique()).isEqualTo(DEFAULT_ENVIRONNEMENT_TECHNIQUE);
    }

    @Test
    @Transactional
    public void createDescriptionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = descriptionRepository.findAll().size();

        // Create the Description with an existing ID
        description.setId(1L);
        DescriptionDTO descriptionDTO = descriptionMapper.toDto(description);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDescriptionMockMvc.perform(post("/api/descriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(descriptionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Description in the database
        List<Description> descriptionList = descriptionRepository.findAll();
        assertThat(descriptionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDescriptions() throws Exception {
        // Initialize the database
        descriptionRepository.saveAndFlush(description);

        // Get all the descriptionList
        restDescriptionMockMvc.perform(get("/api/descriptions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(description.getId().intValue())))
            .andExpect(jsonPath("$.[*].context").value(hasItem(DEFAULT_CONTEXT)))
            .andExpect(jsonPath("$.[*].mission").value(hasItem(DEFAULT_MISSION)))
            .andExpect(jsonPath("$.[*].environnementTechnique").value(hasItem(DEFAULT_ENVIRONNEMENT_TECHNIQUE)));
    }
    
    @Test
    @Transactional
    public void getDescription() throws Exception {
        // Initialize the database
        descriptionRepository.saveAndFlush(description);

        // Get the description
        restDescriptionMockMvc.perform(get("/api/descriptions/{id}", description.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(description.getId().intValue()))
            .andExpect(jsonPath("$.context").value(DEFAULT_CONTEXT))
            .andExpect(jsonPath("$.mission").value(DEFAULT_MISSION))
            .andExpect(jsonPath("$.environnementTechnique").value(DEFAULT_ENVIRONNEMENT_TECHNIQUE));
    }
    @Test
    @Transactional
    public void getNonExistingDescription() throws Exception {
        // Get the description
        restDescriptionMockMvc.perform(get("/api/descriptions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDescription() throws Exception {
        // Initialize the database
        descriptionRepository.saveAndFlush(description);

        int databaseSizeBeforeUpdate = descriptionRepository.findAll().size();

        // Update the description
        Description updatedDescription = descriptionRepository.findById(description.getId()).get();
        // Disconnect from session so that the updates on updatedDescription are not directly saved in db
        em.detach(updatedDescription);
        updatedDescription
            .context(UPDATED_CONTEXT)
            .mission(UPDATED_MISSION)
            .environnementTechnique(UPDATED_ENVIRONNEMENT_TECHNIQUE);
        DescriptionDTO descriptionDTO = descriptionMapper.toDto(updatedDescription);

        restDescriptionMockMvc.perform(put("/api/descriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(descriptionDTO)))
            .andExpect(status().isOk());

        // Validate the Description in the database
        List<Description> descriptionList = descriptionRepository.findAll();
        assertThat(descriptionList).hasSize(databaseSizeBeforeUpdate);
        Description testDescription = descriptionList.get(descriptionList.size() - 1);
        assertThat(testDescription.getContext()).isEqualTo(UPDATED_CONTEXT);
        assertThat(testDescription.getMission()).isEqualTo(UPDATED_MISSION);
        assertThat(testDescription.getEnvironnementTechnique()).isEqualTo(UPDATED_ENVIRONNEMENT_TECHNIQUE);
    }

    @Test
    @Transactional
    public void updateNonExistingDescription() throws Exception {
        int databaseSizeBeforeUpdate = descriptionRepository.findAll().size();

        // Create the Description
        DescriptionDTO descriptionDTO = descriptionMapper.toDto(description);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDescriptionMockMvc.perform(put("/api/descriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(descriptionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Description in the database
        List<Description> descriptionList = descriptionRepository.findAll();
        assertThat(descriptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDescription() throws Exception {
        // Initialize the database
        descriptionRepository.saveAndFlush(description);

        int databaseSizeBeforeDelete = descriptionRepository.findAll().size();

        // Delete the description
        restDescriptionMockMvc.perform(delete("/api/descriptions/{id}", description.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Description> descriptionList = descriptionRepository.findAll();
        assertThat(descriptionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
