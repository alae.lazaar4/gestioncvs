package com.optimgov.cvs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RessourceProposeMapperTest {

    private RessourceProposeMapper ressourceProposeMapper;

    @BeforeEach
    public void setUp() {
        ressourceProposeMapper = new RessourceProposeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(ressourceProposeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(ressourceProposeMapper.fromId(null)).isNull();
    }
}
