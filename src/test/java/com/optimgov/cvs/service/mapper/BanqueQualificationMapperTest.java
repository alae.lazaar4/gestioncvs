package com.optimgov.cvs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BanqueQualificationMapperTest {

    private BanqueQualificationMapper banqueQualificationMapper;

    @BeforeEach
    public void setUp() {
        banqueQualificationMapper = new BanqueQualificationMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(banqueQualificationMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(banqueQualificationMapper.fromId(null)).isNull();
    }
}
