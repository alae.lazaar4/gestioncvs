package com.optimgov.cvs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ProjetDemandeMapperTest {

    private ProjetDemandeMapper projetDemandeMapper;

    @BeforeEach
    public void setUp() {
        projetDemandeMapper = new ProjetDemandeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(projetDemandeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(projetDemandeMapper.fromId(null)).isNull();
    }
}
