package com.optimgov.cvs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RessourceDemandeMapperTest {

    private RessourceDemandeMapper ressourceDemandeMapper;

    @BeforeEach
    public void setUp() {
        ressourceDemandeMapper = new RessourceDemandeMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(ressourceDemandeMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(ressourceDemandeMapper.fromId(null)).isNull();
    }
}
