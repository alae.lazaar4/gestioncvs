package com.optimgov.cvs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BanquePProjetMapperTest {

    private BanquePProjetMapper banquePProjetMapper;

    @BeforeEach
    public void setUp() {
        banquePProjetMapper = new BanquePProjetMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(banquePProjetMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(banquePProjetMapper.fromId(null)).isNull();
    }
}
