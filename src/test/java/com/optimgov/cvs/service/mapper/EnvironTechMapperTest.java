package com.optimgov.cvs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class EnvironTechMapperTest {

    private EnvironTechMapper environTechMapper;

    @BeforeEach
    public void setUp() {
        environTechMapper = new EnvironTechMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(environTechMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(environTechMapper.fromId(null)).isNull();
    }
}
