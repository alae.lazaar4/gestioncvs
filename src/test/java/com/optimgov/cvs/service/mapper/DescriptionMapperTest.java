package com.optimgov.cvs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class DescriptionMapperTest {

    private DescriptionMapper descriptionMapper;

    @BeforeEach
    public void setUp() {
        descriptionMapper = new DescriptionMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(descriptionMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(descriptionMapper.fromId(null)).isNull();
    }
}
