package com.optimgov.cvs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BanquePProfilMapperTest {

    private BanquePProfilMapper banquePProfilMapper;

    @BeforeEach
    public void setUp() {
        banquePProfilMapper = new BanquePProfilMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(banquePProfilMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(banquePProfilMapper.fromId(null)).isNull();
    }
}
