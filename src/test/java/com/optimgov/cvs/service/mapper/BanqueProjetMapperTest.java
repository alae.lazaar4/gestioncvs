package com.optimgov.cvs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BanqueProjetMapperTest {

    private BanqueProjetMapper banqueProjetMapper;

    @BeforeEach
    public void setUp() {
        banqueProjetMapper = new BanqueProjetMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(banqueProjetMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(banqueProjetMapper.fromId(null)).isNull();
    }
}
