package com.optimgov.cvs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class ProjetDemandeDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProjetDemandeDTO.class);
        ProjetDemandeDTO projetDemandeDTO1 = new ProjetDemandeDTO();
        projetDemandeDTO1.setId(1L);
        ProjetDemandeDTO projetDemandeDTO2 = new ProjetDemandeDTO();
        assertThat(projetDemandeDTO1).isNotEqualTo(projetDemandeDTO2);
        projetDemandeDTO2.setId(projetDemandeDTO1.getId());
        assertThat(projetDemandeDTO1).isEqualTo(projetDemandeDTO2);
        projetDemandeDTO2.setId(2L);
        assertThat(projetDemandeDTO1).isNotEqualTo(projetDemandeDTO2);
        projetDemandeDTO1.setId(null);
        assertThat(projetDemandeDTO1).isNotEqualTo(projetDemandeDTO2);
    }
}
