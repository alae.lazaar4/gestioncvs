package com.optimgov.cvs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class EnvironTechDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EnvironTechDTO.class);
        EnvironTechDTO environTechDTO1 = new EnvironTechDTO();
        environTechDTO1.setId(1L);
        EnvironTechDTO environTechDTO2 = new EnvironTechDTO();
        assertThat(environTechDTO1).isNotEqualTo(environTechDTO2);
        environTechDTO2.setId(environTechDTO1.getId());
        assertThat(environTechDTO1).isEqualTo(environTechDTO2);
        environTechDTO2.setId(2L);
        assertThat(environTechDTO1).isNotEqualTo(environTechDTO2);
        environTechDTO1.setId(null);
        assertThat(environTechDTO1).isNotEqualTo(environTechDTO2);
    }
}
