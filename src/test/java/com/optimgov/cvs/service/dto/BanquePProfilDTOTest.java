package com.optimgov.cvs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class BanquePProfilDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BanquePProfilDTO.class);
        BanquePProfilDTO banquePProfilDTO1 = new BanquePProfilDTO();
        banquePProfilDTO1.setId(1L);
        BanquePProfilDTO banquePProfilDTO2 = new BanquePProfilDTO();
        assertThat(banquePProfilDTO1).isNotEqualTo(banquePProfilDTO2);
        banquePProfilDTO2.setId(banquePProfilDTO1.getId());
        assertThat(banquePProfilDTO1).isEqualTo(banquePProfilDTO2);
        banquePProfilDTO2.setId(2L);
        assertThat(banquePProfilDTO1).isNotEqualTo(banquePProfilDTO2);
        banquePProfilDTO1.setId(null);
        assertThat(banquePProfilDTO1).isNotEqualTo(banquePProfilDTO2);
    }
}
