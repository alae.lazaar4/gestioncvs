package com.optimgov.cvs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class RessourceDemandeDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RessourceDemandeDTO.class);
        RessourceDemandeDTO ressourceDemandeDTO1 = new RessourceDemandeDTO();
        ressourceDemandeDTO1.setId(1L);
        RessourceDemandeDTO ressourceDemandeDTO2 = new RessourceDemandeDTO();
        assertThat(ressourceDemandeDTO1).isNotEqualTo(ressourceDemandeDTO2);
        ressourceDemandeDTO2.setId(ressourceDemandeDTO1.getId());
        assertThat(ressourceDemandeDTO1).isEqualTo(ressourceDemandeDTO2);
        ressourceDemandeDTO2.setId(2L);
        assertThat(ressourceDemandeDTO1).isNotEqualTo(ressourceDemandeDTO2);
        ressourceDemandeDTO1.setId(null);
        assertThat(ressourceDemandeDTO1).isNotEqualTo(ressourceDemandeDTO2);
    }
}
