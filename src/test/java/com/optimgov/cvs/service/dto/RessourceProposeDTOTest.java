package com.optimgov.cvs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class RessourceProposeDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RessourceProposeDTO.class);
        RessourceProposeDTO ressourceProposeDTO1 = new RessourceProposeDTO();
        ressourceProposeDTO1.setId(1L);
        RessourceProposeDTO ressourceProposeDTO2 = new RessourceProposeDTO();
        assertThat(ressourceProposeDTO1).isNotEqualTo(ressourceProposeDTO2);
        ressourceProposeDTO2.setId(ressourceProposeDTO1.getId());
        assertThat(ressourceProposeDTO1).isEqualTo(ressourceProposeDTO2);
        ressourceProposeDTO2.setId(2L);
        assertThat(ressourceProposeDTO1).isNotEqualTo(ressourceProposeDTO2);
        ressourceProposeDTO1.setId(null);
        assertThat(ressourceProposeDTO1).isNotEqualTo(ressourceProposeDTO2);
    }
}
