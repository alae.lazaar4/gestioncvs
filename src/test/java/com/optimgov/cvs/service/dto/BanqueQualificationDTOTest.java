package com.optimgov.cvs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class BanqueQualificationDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BanqueQualificationDTO.class);
        BanqueQualificationDTO banqueQualificationDTO1 = new BanqueQualificationDTO();
        banqueQualificationDTO1.setId(1L);
        BanqueQualificationDTO banqueQualificationDTO2 = new BanqueQualificationDTO();
        assertThat(banqueQualificationDTO1).isNotEqualTo(banqueQualificationDTO2);
        banqueQualificationDTO2.setId(banqueQualificationDTO1.getId());
        assertThat(banqueQualificationDTO1).isEqualTo(banqueQualificationDTO2);
        banqueQualificationDTO2.setId(2L);
        assertThat(banqueQualificationDTO1).isNotEqualTo(banqueQualificationDTO2);
        banqueQualificationDTO1.setId(null);
        assertThat(banqueQualificationDTO1).isNotEqualTo(banqueQualificationDTO2);
    }
}
