package com.optimgov.cvs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class BanquePProjetDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BanquePProjetDTO.class);
        BanquePProjetDTO banquePProjetDTO1 = new BanquePProjetDTO();
        banquePProjetDTO1.setId(1L);
        BanquePProjetDTO banquePProjetDTO2 = new BanquePProjetDTO();
        assertThat(banquePProjetDTO1).isNotEqualTo(banquePProjetDTO2);
        banquePProjetDTO2.setId(banquePProjetDTO1.getId());
        assertThat(banquePProjetDTO1).isEqualTo(banquePProjetDTO2);
        banquePProjetDTO2.setId(2L);
        assertThat(banquePProjetDTO1).isNotEqualTo(banquePProjetDTO2);
        banquePProjetDTO1.setId(null);
        assertThat(banquePProjetDTO1).isNotEqualTo(banquePProjetDTO2);
    }
}
