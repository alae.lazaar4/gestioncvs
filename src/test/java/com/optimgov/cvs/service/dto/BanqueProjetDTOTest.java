package com.optimgov.cvs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.optimgov.cvs.web.rest.TestUtil;

public class BanqueProjetDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BanqueProjetDTO.class);
        BanqueProjetDTO banqueProjetDTO1 = new BanqueProjetDTO();
        banqueProjetDTO1.setId(1L);
        BanqueProjetDTO banqueProjetDTO2 = new BanqueProjetDTO();
        assertThat(banqueProjetDTO1).isNotEqualTo(banqueProjetDTO2);
        banqueProjetDTO2.setId(banqueProjetDTO1.getId());
        assertThat(banqueProjetDTO1).isEqualTo(banqueProjetDTO2);
        banqueProjetDTO2.setId(2L);
        assertThat(banqueProjetDTO1).isNotEqualTo(banqueProjetDTO2);
        banqueProjetDTO1.setId(null);
        assertThat(banqueProjetDTO1).isNotEqualTo(banqueProjetDTO2);
    }
}
