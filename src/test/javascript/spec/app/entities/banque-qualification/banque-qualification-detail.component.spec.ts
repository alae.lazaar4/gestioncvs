import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { BanqueQualificationDetailComponent } from 'app/entities/banque-qualification/banque-qualification-detail.component';
import { BanqueQualification } from 'app/shared/model/banque-qualification.model';

describe('Component Tests', () => {
  describe('BanqueQualification Management Detail Component', () => {
    let comp: BanqueQualificationDetailComponent;
    let fixture: ComponentFixture<BanqueQualificationDetailComponent>;
    const route = ({ data: of({ banqueQualification: new BanqueQualification(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [BanqueQualificationDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BanqueQualificationDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BanqueQualificationDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load banqueQualification on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.banqueQualification).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
