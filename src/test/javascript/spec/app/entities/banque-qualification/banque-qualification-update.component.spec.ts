import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { BanqueQualificationUpdateComponent } from 'app/entities/banque-qualification/banque-qualification-update.component';
import { BanqueQualificationService } from 'app/entities/banque-qualification/banque-qualification.service';
import { BanqueQualification } from 'app/shared/model/banque-qualification.model';

describe('Component Tests', () => {
  describe('BanqueQualification Management Update Component', () => {
    let comp: BanqueQualificationUpdateComponent;
    let fixture: ComponentFixture<BanqueQualificationUpdateComponent>;
    let service: BanqueQualificationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [BanqueQualificationUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BanqueQualificationUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BanqueQualificationUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BanqueQualificationService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BanqueQualification(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BanqueQualification();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
