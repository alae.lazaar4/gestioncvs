import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';

import { GestioncvsTestModule } from '../../../test.module';
import { BanqueQualificationComponent } from 'app/entities/banque-qualification/banque-qualification.component';
import { BanqueQualificationService } from 'app/entities/banque-qualification/banque-qualification.service';
import { BanqueQualification } from 'app/shared/model/banque-qualification.model';

describe('Component Tests', () => {
  describe('BanqueQualification Management Component', () => {
    let comp: BanqueQualificationComponent;
    let fixture: ComponentFixture<BanqueQualificationComponent>;
    let service: BanqueQualificationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [BanqueQualificationComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: of({
                defaultSort: 'id,asc',
              }),
              queryParamMap: of(
                convertToParamMap({
                  page: '1',
                  size: '1',
                  sort: 'id,desc',
                })
              ),
            },
          },
        ],
      })
        .overrideTemplate(BanqueQualificationComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BanqueQualificationComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BanqueQualificationService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BanqueQualification(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.banqueQualifications && comp.banqueQualifications[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BanqueQualification(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.banqueQualifications && comp.banqueQualifications[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
