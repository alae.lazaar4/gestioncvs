import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { RessourceProposeDetailComponent } from 'app/entities/ressource-propose/ressource-propose-detail.component';
import { RessourcePropose } from 'app/shared/model/ressource-propose.model';

describe('Component Tests', () => {
  describe('RessourcePropose Management Detail Component', () => {
    let comp: RessourceProposeDetailComponent;
    let fixture: ComponentFixture<RessourceProposeDetailComponent>;
    const route = ({ data: of({ ressourcePropose: new RessourcePropose(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [RessourceProposeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(RessourceProposeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RessourceProposeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load ressourcePropose on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.ressourcePropose).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
