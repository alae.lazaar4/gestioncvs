import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { RessourceProposeUpdateComponent } from 'app/entities/ressource-propose/ressource-propose-update.component';
import { RessourceProposeService } from 'app/entities/ressource-propose/ressource-propose.service';
import { RessourcePropose } from 'app/shared/model/ressource-propose.model';

describe('Component Tests', () => {
  describe('RessourcePropose Management Update Component', () => {
    let comp: RessourceProposeUpdateComponent;
    let fixture: ComponentFixture<RessourceProposeUpdateComponent>;
    let service: RessourceProposeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [RessourceProposeUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(RessourceProposeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RessourceProposeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RessourceProposeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new RessourcePropose(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new RessourcePropose();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
