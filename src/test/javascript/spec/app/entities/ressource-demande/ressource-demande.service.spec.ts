import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RessourceDemandeService } from 'app/entities/ressource-demande/ressource-demande.service';
import { IRessourceDemande, RessourceDemande } from 'app/shared/model/ressource-demande.model';

describe('Service Tests', () => {
  describe('RessourceDemande Service', () => {
    let injector: TestBed;
    let service: RessourceDemandeService;
    let httpMock: HttpTestingController;
    let elemDefault: IRessourceDemande;
    let expectedResult: IRessourceDemande | IRessourceDemande[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(RessourceDemandeService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new RessourceDemande(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a RessourceDemande', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new RessourceDemande()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a RessourceDemande', () => {
        const returnedFromService = Object.assign(
          {
            emploiActuel: 'BBBBBB',
            ancienneteEtude: 'BBBBBB',
            ancienneteEmploi: 'BBBBBB',
            fonction: 'BBBBBB',
            nbRessources: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of RessourceDemande', () => {
        const returnedFromService = Object.assign(
          {
            emploiActuel: 'BBBBBB',
            ancienneteEtude: 'BBBBBB',
            ancienneteEmploi: 'BBBBBB',
            fonction: 'BBBBBB',
            nbRessources: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a RessourceDemande', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
