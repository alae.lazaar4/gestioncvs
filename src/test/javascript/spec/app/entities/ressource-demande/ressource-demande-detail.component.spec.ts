import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { RessourceDemandeDetailComponent } from 'app/entities/ressource-demande/ressource-demande-detail.component';
import { RessourceDemande } from 'app/shared/model/ressource-demande.model';

describe('Component Tests', () => {
  describe('RessourceDemande Management Detail Component', () => {
    let comp: RessourceDemandeDetailComponent;
    let fixture: ComponentFixture<RessourceDemandeDetailComponent>;
    const route = ({ data: of({ ressourceDemande: new RessourceDemande(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [RessourceDemandeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(RessourceDemandeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RessourceDemandeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load ressourceDemande on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.ressourceDemande).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
