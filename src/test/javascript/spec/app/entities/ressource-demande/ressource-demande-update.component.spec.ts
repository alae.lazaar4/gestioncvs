import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { RessourceDemandeUpdateComponent } from 'app/entities/ressource-demande/ressource-demande-update.component';
import { RessourceDemandeService } from 'app/entities/ressource-demande/ressource-demande.service';
import { RessourceDemande } from 'app/shared/model/ressource-demande.model';

describe('Component Tests', () => {
  describe('RessourceDemande Management Update Component', () => {
    let comp: RessourceDemandeUpdateComponent;
    let fixture: ComponentFixture<RessourceDemandeUpdateComponent>;
    let service: RessourceDemandeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [RessourceDemandeUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(RessourceDemandeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RessourceDemandeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RessourceDemandeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new RessourceDemande(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new RessourceDemande();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
