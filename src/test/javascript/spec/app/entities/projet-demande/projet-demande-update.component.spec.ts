import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { ProjetDemandeUpdateComponent } from 'app/entities/projet-demande/projet-demande-update.component';
import { ProjetDemandeService } from 'app/entities/projet-demande/projet-demande.service';
import { ProjetDemande } from 'app/shared/model/projet-demande.model';

describe('Component Tests', () => {
  describe('ProjetDemande Management Update Component', () => {
    let comp: ProjetDemandeUpdateComponent;
    let fixture: ComponentFixture<ProjetDemandeUpdateComponent>;
    let service: ProjetDemandeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [ProjetDemandeUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(ProjetDemandeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ProjetDemandeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ProjetDemandeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProjetDemande(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new ProjetDemande();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
