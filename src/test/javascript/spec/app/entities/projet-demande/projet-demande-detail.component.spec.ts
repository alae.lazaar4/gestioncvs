import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { ProjetDemandeDetailComponent } from 'app/entities/projet-demande/projet-demande-detail.component';
import { ProjetDemande } from 'app/shared/model/projet-demande.model';

describe('Component Tests', () => {
  describe('ProjetDemande Management Detail Component', () => {
    let comp: ProjetDemandeDetailComponent;
    let fixture: ComponentFixture<ProjetDemandeDetailComponent>;
    const route = ({ data: of({ projetDemande: new ProjetDemande(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [ProjetDemandeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(ProjetDemandeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ProjetDemandeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load projetDemande on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.projetDemande).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
