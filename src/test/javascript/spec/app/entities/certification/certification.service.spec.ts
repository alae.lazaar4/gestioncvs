import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { CertificationService } from 'app/entities/certification/certification.service';
import { ICertification, Certification } from 'app/shared/model/certification.model';

describe('Service Tests', () => {
  describe('Certification Service', () => {
    let injector: TestBed;
    let service: CertificationService;
    let httpMock: HttpTestingController;
    let elemDefault: ICertification;
    let expectedResult: ICertification | ICertification[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(CertificationService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Certification(0, 'AAAAAAA', 'AAAAAAA', currentDate, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            anneeObtention: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Certification', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            anneeObtention: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            anneeObtention: currentDate,
          },
          returnedFromService
        );

        service.create(new Certification()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Certification', () => {
        const returnedFromService = Object.assign(
          {
            intitule: 'BBBBBB',
            organisme: 'BBBBBB',
            anneeObtention: currentDate.format(DATE_TIME_FORMAT),
            domaine: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            anneeObtention: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Certification', () => {
        const returnedFromService = Object.assign(
          {
            intitule: 'BBBBBB',
            organisme: 'BBBBBB',
            anneeObtention: currentDate.format(DATE_TIME_FORMAT),
            domaine: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            anneeObtention: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Certification', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
