import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { BanquePProjetUpdateComponent } from 'app/entities/banque-p-projet/banque-p-projet-update.component';
import { BanquePProjetService } from 'app/entities/banque-p-projet/banque-p-projet.service';
import { BanquePProjet } from 'app/shared/model/banque-p-projet.model';

describe('Component Tests', () => {
  describe('BanquePProjet Management Update Component', () => {
    let comp: BanquePProjetUpdateComponent;
    let fixture: ComponentFixture<BanquePProjetUpdateComponent>;
    let service: BanquePProjetService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [BanquePProjetUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BanquePProjetUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BanquePProjetUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BanquePProjetService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BanquePProjet(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BanquePProjet();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
