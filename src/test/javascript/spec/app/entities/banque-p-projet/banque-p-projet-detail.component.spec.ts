import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { BanquePProjetDetailComponent } from 'app/entities/banque-p-projet/banque-p-projet-detail.component';
import { BanquePProjet } from 'app/shared/model/banque-p-projet.model';

describe('Component Tests', () => {
  describe('BanquePProjet Management Detail Component', () => {
    let comp: BanquePProjetDetailComponent;
    let fixture: ComponentFixture<BanquePProjetDetailComponent>;
    const route = ({ data: of({ banquePProjet: new BanquePProjet(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [BanquePProjetDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BanquePProjetDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BanquePProjetDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load banquePProjet on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.banquePProjet).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
