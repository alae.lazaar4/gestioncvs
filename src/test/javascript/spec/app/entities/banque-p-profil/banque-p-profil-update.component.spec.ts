import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { BanquePProfilUpdateComponent } from 'app/entities/banque-p-profil/banque-p-profil-update.component';
import { BanquePProfilService } from 'app/entities/banque-p-profil/banque-p-profil.service';
import { BanquePProfil } from 'app/shared/model/banque-p-profil.model';

describe('Component Tests', () => {
  describe('BanquePProfil Management Update Component', () => {
    let comp: BanquePProfilUpdateComponent;
    let fixture: ComponentFixture<BanquePProfilUpdateComponent>;
    let service: BanquePProfilService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [BanquePProfilUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BanquePProfilUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BanquePProfilUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BanquePProfilService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BanquePProfil(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BanquePProfil();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
