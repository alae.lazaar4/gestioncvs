import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { BanquePProfilDetailComponent } from 'app/entities/banque-p-profil/banque-p-profil-detail.component';
import { BanquePProfil } from 'app/shared/model/banque-p-profil.model';

describe('Component Tests', () => {
  describe('BanquePProfil Management Detail Component', () => {
    let comp: BanquePProfilDetailComponent;
    let fixture: ComponentFixture<BanquePProfilDetailComponent>;
    const route = ({ data: of({ banquePProfil: new BanquePProfil(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [BanquePProfilDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BanquePProfilDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BanquePProfilDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load banquePProfil on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.banquePProfil).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
