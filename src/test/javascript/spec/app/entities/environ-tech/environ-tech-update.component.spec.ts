import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { EnvironTechUpdateComponent } from 'app/entities/environ-tech/environ-tech-update.component';
import { EnvironTechService } from 'app/entities/environ-tech/environ-tech.service';
import { EnvironTech } from 'app/shared/model/environ-tech.model';

describe('Component Tests', () => {
  describe('EnvironTech Management Update Component', () => {
    let comp: EnvironTechUpdateComponent;
    let fixture: ComponentFixture<EnvironTechUpdateComponent>;
    let service: EnvironTechService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [EnvironTechUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(EnvironTechUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EnvironTechUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EnvironTechService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new EnvironTech(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new EnvironTech();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
