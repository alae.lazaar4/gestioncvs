import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { EnvironTechDetailComponent } from 'app/entities/environ-tech/environ-tech-detail.component';
import { EnvironTech } from 'app/shared/model/environ-tech.model';

describe('Component Tests', () => {
  describe('EnvironTech Management Detail Component', () => {
    let comp: EnvironTechDetailComponent;
    let fixture: ComponentFixture<EnvironTechDetailComponent>;
    const route = ({ data: of({ environTech: new EnvironTech(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [EnvironTechDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(EnvironTechDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(EnvironTechDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load environTech on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.environTech).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
