import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';

import { GestioncvsTestModule } from '../../../test.module';
import { EnvironTechComponent } from 'app/entities/environ-tech/environ-tech.component';
import { EnvironTechService } from 'app/entities/environ-tech/environ-tech.service';
import { EnvironTech } from 'app/shared/model/environ-tech.model';

describe('Component Tests', () => {
  describe('EnvironTech Management Component', () => {
    let comp: EnvironTechComponent;
    let fixture: ComponentFixture<EnvironTechComponent>;
    let service: EnvironTechService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [EnvironTechComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: of({
                defaultSort: 'id,asc',
              }),
              queryParamMap: of(
                convertToParamMap({
                  page: '1',
                  size: '1',
                  sort: 'id,desc',
                })
              ),
            },
          },
        ],
      })
        .overrideTemplate(EnvironTechComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EnvironTechComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(EnvironTechService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new EnvironTech(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.environTeches && comp.environTeches[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new EnvironTech(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.environTeches && comp.environTeches[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
