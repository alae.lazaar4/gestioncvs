import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { EnvironTechService } from 'app/entities/environ-tech/environ-tech.service';
import { IEnvironTech, EnvironTech } from 'app/shared/model/environ-tech.model';

describe('Service Tests', () => {
  describe('EnvironTech Service', () => {
    let injector: TestBed;
    let service: EnvironTechService;
    let httpMock: HttpTestingController;
    let elemDefault: IEnvironTech;
    let expectedResult: IEnvironTech | IEnvironTech[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(EnvironTechService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new EnvironTech(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a EnvironTech', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new EnvironTech()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a EnvironTech', () => {
        const returnedFromService = Object.assign(
          {
            webTechnlogies: 'BBBBBB',
            webClientTechnologies: 'BBBBBB',
            baseDonnee: 'BBBBBB',
            projetManagementBuilding: 'BBBBBB',
            architechture: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of EnvironTech', () => {
        const returnedFromService = Object.assign(
          {
            webTechnlogies: 'BBBBBB',
            webClientTechnologies: 'BBBBBB',
            baseDonnee: 'BBBBBB',
            projetManagementBuilding: 'BBBBBB',
            architechture: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a EnvironTech', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
