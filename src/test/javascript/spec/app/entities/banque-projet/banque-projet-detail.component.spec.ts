import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { BanqueProjetDetailComponent } from 'app/entities/banque-projet/banque-projet-detail.component';
import { BanqueProjet } from 'app/shared/model/banque-projet.model';

describe('Component Tests', () => {
  describe('BanqueProjet Management Detail Component', () => {
    let comp: BanqueProjetDetailComponent;
    let fixture: ComponentFixture<BanqueProjetDetailComponent>;
    const route = ({ data: of({ banqueProjet: new BanqueProjet(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [BanqueProjetDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BanqueProjetDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BanqueProjetDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load banqueProjet on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.banqueProjet).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
