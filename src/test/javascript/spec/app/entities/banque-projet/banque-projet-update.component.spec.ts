import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { GestioncvsTestModule } from '../../../test.module';
import { BanqueProjetUpdateComponent } from 'app/entities/banque-projet/banque-projet-update.component';
import { BanqueProjetService } from 'app/entities/banque-projet/banque-projet.service';
import { BanqueProjet } from 'app/shared/model/banque-projet.model';

describe('Component Tests', () => {
  describe('BanqueProjet Management Update Component', () => {
    let comp: BanqueProjetUpdateComponent;
    let fixture: ComponentFixture<BanqueProjetUpdateComponent>;
    let service: BanqueProjetService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [GestioncvsTestModule],
        declarations: [BanqueProjetUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BanqueProjetUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BanqueProjetUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BanqueProjetService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BanqueProjet(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BanqueProjet();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
