import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { BanqueProjetService } from 'app/entities/banque-projet/banque-projet.service';
import { IBanqueProjet, BanqueProjet } from 'app/shared/model/banque-projet.model';

describe('Service Tests', () => {
  describe('BanqueProjet Service', () => {
    let injector: TestBed;
    let service: BanqueProjetService;
    let httpMock: HttpTestingController;
    let elemDefault: IBanqueProjet;
    let expectedResult: IBanqueProjet | IBanqueProjet[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(BanqueProjetService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new BanqueProjet(0, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA', currentDate, 'AAAAAAA', 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            anneeProjet: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a BanqueProjet', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            anneeProjet: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            anneeProjet: currentDate,
          },
          returnedFromService
        );

        service.create(new BanqueProjet()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a BanqueProjet', () => {
        const returnedFromService = Object.assign(
          {
            designation: 'BBBBBB',
            organisme: 'BBBBBB',
            budget: 'BBBBBB',
            anneeProjet: currentDate.format(DATE_TIME_FORMAT),
            duree: 'BBBBBB',
            resultat: 'BBBBBB',
            mission: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            anneeProjet: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of BanqueProjet', () => {
        const returnedFromService = Object.assign(
          {
            designation: 'BBBBBB',
            organisme: 'BBBBBB',
            budget: 'BBBBBB',
            anneeProjet: currentDate.format(DATE_TIME_FORMAT),
            duree: 'BBBBBB',
            resultat: 'BBBBBB',
            mission: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            anneeProjet: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BanqueProjet', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
