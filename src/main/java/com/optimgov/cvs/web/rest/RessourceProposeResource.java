package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.service.RessourceProposeService;
import com.optimgov.cvs.web.rest.errors.BadRequestAlertException;
import com.optimgov.cvs.service.dto.RessourceProposeDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.optimgov.cvs.domain.RessourcePropose}.
 */
@RestController
@RequestMapping("/api")
public class RessourceProposeResource {

    private final Logger log = LoggerFactory.getLogger(RessourceProposeResource.class);

    private static final String ENTITY_NAME = "ressourcePropose";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RessourceProposeService ressourceProposeService;

    public RessourceProposeResource(RessourceProposeService ressourceProposeService) {
        this.ressourceProposeService = ressourceProposeService;
    }

    /**
     * {@code POST  /ressource-proposes} : Create a new ressourcePropose.
     *
     * @param ressourceProposeDTO the ressourceProposeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ressourceProposeDTO, or with status {@code 400 (Bad Request)} if the ressourcePropose has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ressource-proposes")
    public ResponseEntity<RessourceProposeDTO> createRessourcePropose(@Valid @RequestBody RessourceProposeDTO ressourceProposeDTO) throws URISyntaxException {
        log.debug("REST request to save RessourcePropose : {}", ressourceProposeDTO);
        if (ressourceProposeDTO.getId() != null) {
            throw new BadRequestAlertException("A new ressourcePropose cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RessourceProposeDTO result = ressourceProposeService.save(ressourceProposeDTO);
        return ResponseEntity.created(new URI("/api/ressource-proposes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ressource-proposes} : Updates an existing ressourcePropose.
     *
     * @param ressourceProposeDTO the ressourceProposeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ressourceProposeDTO,
     * or with status {@code 400 (Bad Request)} if the ressourceProposeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ressourceProposeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ressource-proposes")
    public ResponseEntity<RessourceProposeDTO> updateRessourcePropose(@Valid @RequestBody RessourceProposeDTO ressourceProposeDTO) throws URISyntaxException {
        log.debug("REST request to update RessourcePropose : {}", ressourceProposeDTO);
        if (ressourceProposeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RessourceProposeDTO result = ressourceProposeService.save(ressourceProposeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ressourceProposeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ressource-proposes} : get all the ressourceProposes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ressourceProposes in body.
     */
    @GetMapping("/ressource-proposes")
    public ResponseEntity<List<RessourceProposeDTO>> getAllRessourceProposes(Pageable pageable) {
        log.debug("REST request to get a page of RessourceProposes");
        Page<RessourceProposeDTO> page = ressourceProposeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /ressource-proposes/:id} : get the "id" ressourcePropose.
     *
     * @param id the id of the ressourceProposeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ressourceProposeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ressource-proposes/{id}")
    public ResponseEntity<RessourceProposeDTO> getRessourcePropose(@PathVariable Long id) {
        log.debug("REST request to get RessourcePropose : {}", id);
        Optional<RessourceProposeDTO> ressourceProposeDTO = ressourceProposeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ressourceProposeDTO);
    }

    /**
     * {@code DELETE  /ressource-proposes/:id} : delete the "id" ressourcePropose.
     *
     * @param id the id of the ressourceProposeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ressource-proposes/{id}")
    public ResponseEntity<Void> deleteRessourcePropose(@PathVariable Long id) {
        log.debug("REST request to delete RessourcePropose : {}", id);
        ressourceProposeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
