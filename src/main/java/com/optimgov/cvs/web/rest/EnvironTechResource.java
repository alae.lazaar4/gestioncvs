package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.service.EnvironTechService;
import com.optimgov.cvs.web.rest.errors.BadRequestAlertException;
import com.optimgov.cvs.service.dto.EnvironTechDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.optimgov.cvs.domain.EnvironTech}.
 */
@RestController
@RequestMapping("/api")
public class EnvironTechResource {

    private final Logger log = LoggerFactory.getLogger(EnvironTechResource.class);

    private static final String ENTITY_NAME = "environTech";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EnvironTechService environTechService;

    public EnvironTechResource(EnvironTechService environTechService) {
        this.environTechService = environTechService;
    }

    /**
     * {@code POST  /environ-teches} : Create a new environTech.
     *
     * @param environTechDTO the environTechDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new environTechDTO, or with status {@code 400 (Bad Request)} if the environTech has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/environ-teches")
    public ResponseEntity<EnvironTechDTO> createEnvironTech(@RequestBody EnvironTechDTO environTechDTO) throws URISyntaxException {
        log.debug("REST request to save EnvironTech : {}", environTechDTO);
        if (environTechDTO.getId() != null) {
            throw new BadRequestAlertException("A new environTech cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EnvironTechDTO result = environTechService.save(environTechDTO);
        return ResponseEntity.created(new URI("/api/environ-teches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /environ-teches} : Updates an existing environTech.
     *
     * @param environTechDTO the environTechDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated environTechDTO,
     * or with status {@code 400 (Bad Request)} if the environTechDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the environTechDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/environ-teches")
    public ResponseEntity<EnvironTechDTO> updateEnvironTech(@RequestBody EnvironTechDTO environTechDTO) throws URISyntaxException {
        log.debug("REST request to update EnvironTech : {}", environTechDTO);
        if (environTechDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EnvironTechDTO result = environTechService.save(environTechDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, environTechDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /environ-teches} : get all the environTeches.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of environTeches in body.
     */
    @GetMapping("/environ-teches")
    public ResponseEntity<List<EnvironTechDTO>> getAllEnvironTeches(Pageable pageable) {
        log.debug("REST request to get a page of EnvironTeches");
        Page<EnvironTechDTO> page = environTechService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /environ-teches/:id} : get the "id" environTech.
     *
     * @param id the id of the environTechDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the environTechDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/environ-teches/{id}")
    public ResponseEntity<EnvironTechDTO> getEnvironTech(@PathVariable Long id) {
        log.debug("REST request to get EnvironTech : {}", id);
        Optional<EnvironTechDTO> environTechDTO = environTechService.findOne(id);
        return ResponseUtil.wrapOrNotFound(environTechDTO);
    }

    /**
     * {@code DELETE  /environ-teches/:id} : delete the "id" environTech.
     *
     * @param id the id of the environTechDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/environ-teches/{id}")
    public ResponseEntity<Void> deleteEnvironTech(@PathVariable Long id) {
        log.debug("REST request to delete EnvironTech : {}", id);
        environTechService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
