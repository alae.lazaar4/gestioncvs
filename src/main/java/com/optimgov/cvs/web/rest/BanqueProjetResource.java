package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.service.BanqueProjetService;
import com.optimgov.cvs.web.rest.errors.BadRequestAlertException;
import com.optimgov.cvs.service.dto.BanqueProjetDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.optimgov.cvs.domain.BanqueProjet}.
 */
@RestController
@RequestMapping("/api")
public class BanqueProjetResource {

    private final Logger log = LoggerFactory.getLogger(BanqueProjetResource.class);

    private static final String ENTITY_NAME = "banqueProjet";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BanqueProjetService banqueProjetService;

    public BanqueProjetResource(BanqueProjetService banqueProjetService) {
        this.banqueProjetService = banqueProjetService;
    }

    /**
     * {@code POST  /banque-projets} : Create a new banqueProjet.
     *
     * @param banqueProjetDTO the banqueProjetDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new banqueProjetDTO, or with status {@code 400 (Bad Request)} if the banqueProjet has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/banque-projets")
    public ResponseEntity<BanqueProjetDTO> createBanqueProjet(@RequestBody BanqueProjetDTO banqueProjetDTO) throws URISyntaxException {
        log.debug("REST request to save BanqueProjet : {}", banqueProjetDTO);
        if (banqueProjetDTO.getId() != null) {
            throw new BadRequestAlertException("A new banqueProjet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BanqueProjetDTO result = banqueProjetService.save(banqueProjetDTO);
        return ResponseEntity.created(new URI("/api/banque-projets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /banque-projets} : Updates an existing banqueProjet.
     *
     * @param banqueProjetDTO the banqueProjetDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated banqueProjetDTO,
     * or with status {@code 400 (Bad Request)} if the banqueProjetDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the banqueProjetDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/banque-projets")
    public ResponseEntity<BanqueProjetDTO> updateBanqueProjet(@RequestBody BanqueProjetDTO banqueProjetDTO) throws URISyntaxException {
        log.debug("REST request to update BanqueProjet : {}", banqueProjetDTO);
        if (banqueProjetDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BanqueProjetDTO result = banqueProjetService.save(banqueProjetDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, banqueProjetDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /banque-projets} : get all the banqueProjets.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of banqueProjets in body.
     */
    @GetMapping("/banque-projets")
    public ResponseEntity<List<BanqueProjetDTO>> getAllBanqueProjets(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of BanqueProjets");
        Page<BanqueProjetDTO> page;
        if (eagerload) {
            page = banqueProjetService.findAllWithEagerRelationships(pageable);
        } else {
            page = banqueProjetService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /banque-projets/:id} : get the "id" banqueProjet.
     *
     * @param id the id of the banqueProjetDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the banqueProjetDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/banque-projets/{id}")
    public ResponseEntity<BanqueProjetDTO> getBanqueProjet(@PathVariable Long id) {
        log.debug("REST request to get BanqueProjet : {}", id);
        Optional<BanqueProjetDTO> banqueProjetDTO = banqueProjetService.findOne(id);
        return ResponseUtil.wrapOrNotFound(banqueProjetDTO);
    }

    /**
     * {@code DELETE  /banque-projets/:id} : delete the "id" banqueProjet.
     *
     * @param id the id of the banqueProjetDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/banque-projets/{id}")
    public ResponseEntity<Void> deleteBanqueProjet(@PathVariable Long id) {
        log.debug("REST request to delete BanqueProjet : {}", id);
        banqueProjetService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
