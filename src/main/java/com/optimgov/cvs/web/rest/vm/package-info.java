/**
 * View Models used by Spring MVC REST controllers.
 */
package com.optimgov.cvs.web.rest.vm;
