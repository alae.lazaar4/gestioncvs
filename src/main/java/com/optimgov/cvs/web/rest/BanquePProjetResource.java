package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.service.BanquePProjetService;
import com.optimgov.cvs.web.rest.errors.BadRequestAlertException;
import com.optimgov.cvs.service.dto.BanquePProjetDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.optimgov.cvs.domain.BanquePProjet}.
 */
@RestController
@RequestMapping("/api")
public class BanquePProjetResource {

    private final Logger log = LoggerFactory.getLogger(BanquePProjetResource.class);

    private static final String ENTITY_NAME = "banquePProjet";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BanquePProjetService banquePProjetService;

    public BanquePProjetResource(BanquePProjetService banquePProjetService) {
        this.banquePProjetService = banquePProjetService;
    }

    /**
     * {@code POST  /banque-p-projets} : Create a new banquePProjet.
     *
     * @param banquePProjetDTO the banquePProjetDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new banquePProjetDTO, or with status {@code 400 (Bad Request)} if the banquePProjet has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/banque-p-projets")
    public ResponseEntity<BanquePProjetDTO> createBanquePProjet(@RequestBody BanquePProjetDTO banquePProjetDTO) throws URISyntaxException {
        log.debug("REST request to save BanquePProjet : {}", banquePProjetDTO);
        if (banquePProjetDTO.getId() != null) {
            throw new BadRequestAlertException("A new banquePProjet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BanquePProjetDTO result = banquePProjetService.save(banquePProjetDTO);
        return ResponseEntity.created(new URI("/api/banque-p-projets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /banque-p-projets} : Updates an existing banquePProjet.
     *
     * @param banquePProjetDTO the banquePProjetDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated banquePProjetDTO,
     * or with status {@code 400 (Bad Request)} if the banquePProjetDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the banquePProjetDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/banque-p-projets")
    public ResponseEntity<BanquePProjetDTO> updateBanquePProjet(@RequestBody BanquePProjetDTO banquePProjetDTO) throws URISyntaxException {
        log.debug("REST request to update BanquePProjet : {}", banquePProjetDTO);
        if (banquePProjetDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BanquePProjetDTO result = banquePProjetService.save(banquePProjetDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, banquePProjetDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /banque-p-projets} : get all the banquePProjets.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of banquePProjets in body.
     */
    @GetMapping("/banque-p-projets")
    public ResponseEntity<List<BanquePProjetDTO>> getAllBanquePProjets(Pageable pageable) {
        log.debug("REST request to get a page of BanquePProjets");
        Page<BanquePProjetDTO> page = banquePProjetService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /banque-p-projets/:id} : get the "id" banquePProjet.
     *
     * @param id the id of the banquePProjetDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the banquePProjetDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/banque-p-projets/{id}")
    public ResponseEntity<BanquePProjetDTO> getBanquePProjet(@PathVariable Long id) {
        log.debug("REST request to get BanquePProjet : {}", id);
        Optional<BanquePProjetDTO> banquePProjetDTO = banquePProjetService.findOne(id);
        return ResponseUtil.wrapOrNotFound(banquePProjetDTO);
    }

    /**
     * {@code DELETE  /banque-p-projets/:id} : delete the "id" banquePProjet.
     *
     * @param id the id of the banquePProjetDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/banque-p-projets/{id}")
    public ResponseEntity<Void> deleteBanquePProjet(@PathVariable Long id) {
        log.debug("REST request to delete BanquePProjet : {}", id);
        banquePProjetService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
