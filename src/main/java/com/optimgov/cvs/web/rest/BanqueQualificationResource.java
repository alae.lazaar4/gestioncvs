package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.service.BanqueQualificationService;
import com.optimgov.cvs.web.rest.errors.BadRequestAlertException;
import com.optimgov.cvs.service.dto.BanqueQualificationDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.optimgov.cvs.domain.BanqueQualification}.
 */
@RestController
@RequestMapping("/api")
public class BanqueQualificationResource {

    private final Logger log = LoggerFactory.getLogger(BanqueQualificationResource.class);

    private static final String ENTITY_NAME = "banqueQualification";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BanqueQualificationService banqueQualificationService;

    public BanqueQualificationResource(BanqueQualificationService banqueQualificationService) {
        this.banqueQualificationService = banqueQualificationService;
    }

    /**
     * {@code POST  /banque-qualifications} : Create a new banqueQualification.
     *
     * @param banqueQualificationDTO the banqueQualificationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new banqueQualificationDTO, or with status {@code 400 (Bad Request)} if the banqueQualification has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/banque-qualifications")
    public ResponseEntity<BanqueQualificationDTO> createBanqueQualification(@RequestBody BanqueQualificationDTO banqueQualificationDTO) throws URISyntaxException {
        log.debug("REST request to save BanqueQualification : {}", banqueQualificationDTO);
        if (banqueQualificationDTO.getId() != null) {
            throw new BadRequestAlertException("A new banqueQualification cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BanqueQualificationDTO result = banqueQualificationService.save(banqueQualificationDTO);
        return ResponseEntity.created(new URI("/api/banque-qualifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /banque-qualifications} : Updates an existing banqueQualification.
     *
     * @param banqueQualificationDTO the banqueQualificationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated banqueQualificationDTO,
     * or with status {@code 400 (Bad Request)} if the banqueQualificationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the banqueQualificationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/banque-qualifications")
    public ResponseEntity<BanqueQualificationDTO> updateBanqueQualification(@RequestBody BanqueQualificationDTO banqueQualificationDTO) throws URISyntaxException {
        log.debug("REST request to update BanqueQualification : {}", banqueQualificationDTO);
        if (banqueQualificationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BanqueQualificationDTO result = banqueQualificationService.save(banqueQualificationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, banqueQualificationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /banque-qualifications} : get all the banqueQualifications.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of banqueQualifications in body.
     */
    @GetMapping("/banque-qualifications")
    public ResponseEntity<List<BanqueQualificationDTO>> getAllBanqueQualifications(Pageable pageable) {
        log.debug("REST request to get a page of BanqueQualifications");
        Page<BanqueQualificationDTO> page = banqueQualificationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /banque-qualifications/:id} : get the "id" banqueQualification.
     *
     * @param id the id of the banqueQualificationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the banqueQualificationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/banque-qualifications/{id}")
    public ResponseEntity<BanqueQualificationDTO> getBanqueQualification(@PathVariable Long id) {
        log.debug("REST request to get BanqueQualification : {}", id);
        Optional<BanqueQualificationDTO> banqueQualificationDTO = banqueQualificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(banqueQualificationDTO);
    }

    /**
     * {@code DELETE  /banque-qualifications/:id} : delete the "id" banqueQualification.
     *
     * @param id the id of the banqueQualificationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/banque-qualifications/{id}")
    public ResponseEntity<Void> deleteBanqueQualification(@PathVariable Long id) {
        log.debug("REST request to delete BanqueQualification : {}", id);
        banqueQualificationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
