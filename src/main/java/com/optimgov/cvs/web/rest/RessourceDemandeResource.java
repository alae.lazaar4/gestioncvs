package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.service.RessourceDemandeService;
import com.optimgov.cvs.web.rest.errors.BadRequestAlertException;
import com.optimgov.cvs.service.dto.RessourceDemandeDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.optimgov.cvs.domain.RessourceDemande}.
 */
@RestController
@RequestMapping("/api")
public class RessourceDemandeResource {

    private final Logger log = LoggerFactory.getLogger(RessourceDemandeResource.class);

    private static final String ENTITY_NAME = "ressourceDemande";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RessourceDemandeService ressourceDemandeService;

    public RessourceDemandeResource(RessourceDemandeService ressourceDemandeService) {
        this.ressourceDemandeService = ressourceDemandeService;
    }

    /**
     * {@code POST  /ressource-demandes} : Create a new ressourceDemande.
     *
     * @param ressourceDemandeDTO the ressourceDemandeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ressourceDemandeDTO, or with status {@code 400 (Bad Request)} if the ressourceDemande has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/ressource-demandes")
    public ResponseEntity<RessourceDemandeDTO> createRessourceDemande(@RequestBody RessourceDemandeDTO ressourceDemandeDTO) throws URISyntaxException {
        log.debug("REST request to save RessourceDemande : {}", ressourceDemandeDTO);
        if (ressourceDemandeDTO.getId() != null) {
            throw new BadRequestAlertException("A new ressourceDemande cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RessourceDemandeDTO result = ressourceDemandeService.save(ressourceDemandeDTO);
        return ResponseEntity.created(new URI("/api/ressource-demandes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ressource-demandes} : Updates an existing ressourceDemande.
     *
     * @param ressourceDemandeDTO the ressourceDemandeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ressourceDemandeDTO,
     * or with status {@code 400 (Bad Request)} if the ressourceDemandeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ressourceDemandeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/ressource-demandes")
    public ResponseEntity<RessourceDemandeDTO> updateRessourceDemande(@RequestBody RessourceDemandeDTO ressourceDemandeDTO) throws URISyntaxException {
        log.debug("REST request to update RessourceDemande : {}", ressourceDemandeDTO);
        if (ressourceDemandeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RessourceDemandeDTO result = ressourceDemandeService.save(ressourceDemandeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ressourceDemandeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /ressource-demandes} : get all the ressourceDemandes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ressourceDemandes in body.
     */
    @GetMapping("/ressource-demandes")
    public ResponseEntity<List<RessourceDemandeDTO>> getAllRessourceDemandes(Pageable pageable) {
        log.debug("REST request to get a page of RessourceDemandes");
        Page<RessourceDemandeDTO> page = ressourceDemandeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /ressource-demandes/:id} : get the "id" ressourceDemande.
     *
     * @param id the id of the ressourceDemandeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ressourceDemandeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/ressource-demandes/{id}")
    public ResponseEntity<RessourceDemandeDTO> getRessourceDemande(@PathVariable Long id) {
        log.debug("REST request to get RessourceDemande : {}", id);
        Optional<RessourceDemandeDTO> ressourceDemandeDTO = ressourceDemandeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ressourceDemandeDTO);
    }

    /**
     * {@code DELETE  /ressource-demandes/:id} : delete the "id" ressourceDemande.
     *
     * @param id the id of the ressourceDemandeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/ressource-demandes/{id}")
    public ResponseEntity<Void> deleteRessourceDemande(@PathVariable Long id) {
        log.debug("REST request to delete RessourceDemande : {}", id);
        ressourceDemandeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
