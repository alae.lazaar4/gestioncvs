package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.service.BanquePProfilService;
import com.optimgov.cvs.web.rest.errors.BadRequestAlertException;
import com.optimgov.cvs.service.dto.BanquePProfilDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.optimgov.cvs.domain.BanquePProfil}.
 */
@RestController
@RequestMapping("/api")
public class BanquePProfilResource {

    private final Logger log = LoggerFactory.getLogger(BanquePProfilResource.class);

    private static final String ENTITY_NAME = "banquePProfil";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BanquePProfilService banquePProfilService;

    public BanquePProfilResource(BanquePProfilService banquePProfilService) {
        this.banquePProfilService = banquePProfilService;
    }

    /**
     * {@code POST  /banque-p-profils} : Create a new banquePProfil.
     *
     * @param banquePProfilDTO the banquePProfilDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new banquePProfilDTO, or with status {@code 400 (Bad Request)} if the banquePProfil has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/banque-p-profils")
    public ResponseEntity<BanquePProfilDTO> createBanquePProfil(@RequestBody BanquePProfilDTO banquePProfilDTO) throws URISyntaxException {
        log.debug("REST request to save BanquePProfil : {}", banquePProfilDTO);
        if (banquePProfilDTO.getId() != null) {
            throw new BadRequestAlertException("A new banquePProfil cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BanquePProfilDTO result = banquePProfilService.save(banquePProfilDTO);
        return ResponseEntity.created(new URI("/api/banque-p-profils/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /banque-p-profils} : Updates an existing banquePProfil.
     *
     * @param banquePProfilDTO the banquePProfilDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated banquePProfilDTO,
     * or with status {@code 400 (Bad Request)} if the banquePProfilDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the banquePProfilDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/banque-p-profils")
    public ResponseEntity<BanquePProfilDTO> updateBanquePProfil(@RequestBody BanquePProfilDTO banquePProfilDTO) throws URISyntaxException {
        log.debug("REST request to update BanquePProfil : {}", banquePProfilDTO);
        if (banquePProfilDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BanquePProfilDTO result = banquePProfilService.save(banquePProfilDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, banquePProfilDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /banque-p-profils} : get all the banquePProfils.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of banquePProfils in body.
     */
    @GetMapping("/banque-p-profils")
    public ResponseEntity<List<BanquePProfilDTO>> getAllBanquePProfils(Pageable pageable) {
        log.debug("REST request to get a page of BanquePProfils");
        Page<BanquePProfilDTO> page = banquePProfilService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /banque-p-profils/:id} : get the "id" banquePProfil.
     *
     * @param id the id of the banquePProfilDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the banquePProfilDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/banque-p-profils/{id}")
    public ResponseEntity<BanquePProfilDTO> getBanquePProfil(@PathVariable Long id) {
        log.debug("REST request to get BanquePProfil : {}", id);
        Optional<BanquePProfilDTO> banquePProfilDTO = banquePProfilService.findOne(id);
        return ResponseUtil.wrapOrNotFound(banquePProfilDTO);
    }

    /**
     * {@code DELETE  /banque-p-profils/:id} : delete the "id" banquePProfil.
     *
     * @param id the id of the banquePProfilDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/banque-p-profils/{id}")
    public ResponseEntity<Void> deleteBanquePProfil(@PathVariable Long id) {
        log.debug("REST request to delete BanquePProfil : {}", id);
        banquePProfilService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
