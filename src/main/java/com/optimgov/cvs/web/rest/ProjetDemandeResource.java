package com.optimgov.cvs.web.rest;

import com.optimgov.cvs.service.ProjetDemandeService;
import com.optimgov.cvs.web.rest.errors.BadRequestAlertException;
import com.optimgov.cvs.service.dto.ProjetDemandeDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.optimgov.cvs.domain.ProjetDemande}.
 */
@RestController
@RequestMapping("/api")
public class ProjetDemandeResource {

    private final Logger log = LoggerFactory.getLogger(ProjetDemandeResource.class);

    private static final String ENTITY_NAME = "projetDemande";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProjetDemandeService projetDemandeService;

    public ProjetDemandeResource(ProjetDemandeService projetDemandeService) {
        this.projetDemandeService = projetDemandeService;
    }

    /**
     * {@code POST  /projet-demandes} : Create a new projetDemande.
     *
     * @param projetDemandeDTO the projetDemandeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new projetDemandeDTO, or with status {@code 400 (Bad Request)} if the projetDemande has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/projet-demandes")
    public ResponseEntity<ProjetDemandeDTO> createProjetDemande(@RequestBody ProjetDemandeDTO projetDemandeDTO) throws URISyntaxException {
        log.debug("REST request to save ProjetDemande : {}", projetDemandeDTO);
        if (projetDemandeDTO.getId() != null) {
            throw new BadRequestAlertException("A new projetDemande cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProjetDemandeDTO result = projetDemandeService.save(projetDemandeDTO);
        return ResponseEntity.created(new URI("/api/projet-demandes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /projet-demandes} : Updates an existing projetDemande.
     *
     * @param projetDemandeDTO the projetDemandeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated projetDemandeDTO,
     * or with status {@code 400 (Bad Request)} if the projetDemandeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the projetDemandeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/projet-demandes")
    public ResponseEntity<ProjetDemandeDTO> updateProjetDemande(@RequestBody ProjetDemandeDTO projetDemandeDTO) throws URISyntaxException {
        log.debug("REST request to update ProjetDemande : {}", projetDemandeDTO);
        if (projetDemandeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProjetDemandeDTO result = projetDemandeService.save(projetDemandeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, projetDemandeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /projet-demandes} : get all the projetDemandes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of projetDemandes in body.
     */
    @GetMapping("/projet-demandes")
    public ResponseEntity<List<ProjetDemandeDTO>> getAllProjetDemandes(Pageable pageable) {
        log.debug("REST request to get a page of ProjetDemandes");
        Page<ProjetDemandeDTO> page = projetDemandeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /projet-demandes/:id} : get the "id" projetDemande.
     *
     * @param id the id of the projetDemandeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the projetDemandeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/projet-demandes/{id}")
    public ResponseEntity<ProjetDemandeDTO> getProjetDemande(@PathVariable Long id) {
        log.debug("REST request to get ProjetDemande : {}", id);
        Optional<ProjetDemandeDTO> projetDemandeDTO = projetDemandeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(projetDemandeDTO);
    }

    /**
     * {@code DELETE  /projet-demandes/:id} : delete the "id" projetDemande.
     *
     * @param id the id of the projetDemandeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/projet-demandes/{id}")
    public ResponseEntity<Void> deleteProjetDemande(@PathVariable Long id) {
        log.debug("REST request to delete ProjetDemande : {}", id);
        projetDemandeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
