package com.optimgov.cvs.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A EnvironTech.
 */
@Entity
@Table(name = "environ_tech")
public class EnvironTech implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "web_technlogies")
    private String webTechnlogies;

    @Column(name = "web_client_technologies")
    private String webClientTechnologies;

    @Column(name = "base_donnee")
    private String baseDonnee;

    @Column(name = "projet_management_building")
    private String projetManagementBuilding;

    @Column(name = "architechture")
    private String architechture;

    @OneToMany(mappedBy = "environTech")
    private Set<BanqueProjet> nomProjets = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWebTechnlogies() {
        return webTechnlogies;
    }

    public EnvironTech webTechnlogies(String webTechnlogies) {
        this.webTechnlogies = webTechnlogies;
        return this;
    }

    public void setWebTechnlogies(String webTechnlogies) {
        this.webTechnlogies = webTechnlogies;
    }

    public String getWebClientTechnologies() {
        return webClientTechnologies;
    }

    public EnvironTech webClientTechnologies(String webClientTechnologies) {
        this.webClientTechnologies = webClientTechnologies;
        return this;
    }

    public void setWebClientTechnologies(String webClientTechnologies) {
        this.webClientTechnologies = webClientTechnologies;
    }

    public String getBaseDonnee() {
        return baseDonnee;
    }

    public EnvironTech baseDonnee(String baseDonnee) {
        this.baseDonnee = baseDonnee;
        return this;
    }

    public void setBaseDonnee(String baseDonnee) {
        this.baseDonnee = baseDonnee;
    }

    public String getProjetManagementBuilding() {
        return projetManagementBuilding;
    }

    public EnvironTech projetManagementBuilding(String projetManagementBuilding) {
        this.projetManagementBuilding = projetManagementBuilding;
        return this;
    }

    public void setProjetManagementBuilding(String projetManagementBuilding) {
        this.projetManagementBuilding = projetManagementBuilding;
    }

    public String getArchitechture() {
        return architechture;
    }

    public EnvironTech architechture(String architechture) {
        this.architechture = architechture;
        return this;
    }

    public void setArchitechture(String architechture) {
        this.architechture = architechture;
    }

    public Set<BanqueProjet> getNomProjets() {
        return nomProjets;
    }

    public EnvironTech nomProjets(Set<BanqueProjet> banqueProjets) {
        this.nomProjets = banqueProjets;
        return this;
    }

    public EnvironTech addNomProjet(BanqueProjet banqueProjet) {
        this.nomProjets.add(banqueProjet);
        banqueProjet.setEnvironTech(this);
        return this;
    }

    public EnvironTech removeNomProjet(BanqueProjet banqueProjet) {
        this.nomProjets.remove(banqueProjet);
        banqueProjet.setEnvironTech(null);
        return this;
    }

    public void setNomProjets(Set<BanqueProjet> banqueProjets) {
        this.nomProjets = banqueProjets;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EnvironTech)) {
            return false;
        }
        return id != null && id.equals(((EnvironTech) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EnvironTech{" +
            "id=" + getId() +
            ", webTechnlogies='" + getWebTechnlogies() + "'" +
            ", webClientTechnologies='" + getWebClientTechnologies() + "'" +
            ", baseDonnee='" + getBaseDonnee() + "'" +
            ", projetManagementBuilding='" + getProjetManagementBuilding() + "'" +
            ", architechture='" + getArchitechture() + "'" +
            "}";
    }
}
