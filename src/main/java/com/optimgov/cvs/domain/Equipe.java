package com.optimgov.cvs.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Equipe.
 */
@Entity
@Table(name = "equipe")
public class Equipe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "chef_equipe")
    private String chefEquipe;

    @OneToMany(mappedBy = "nomEquipe")
    private Set<RessourcePropose> ressourceProposes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChefEquipe() {
        return chefEquipe;
    }

    public Equipe chefEquipe(String chefEquipe) {
        this.chefEquipe = chefEquipe;
        return this;
    }

    public void setChefEquipe(String chefEquipe) {
        this.chefEquipe = chefEquipe;
    }

    public Set<RessourcePropose> getRessourceProposes() {
        return ressourceProposes;
    }

    public Equipe ressourceProposes(Set<RessourcePropose> ressourceProposes) {
        this.ressourceProposes = ressourceProposes;
        return this;
    }

    public Equipe addRessourcePropose(RessourcePropose ressourcePropose) {
        this.ressourceProposes.add(ressourcePropose);
        ressourcePropose.setNomEquipe(this);
        return this;
    }

    public Equipe removeRessourcePropose(RessourcePropose ressourcePropose) {
        this.ressourceProposes.remove(ressourcePropose);
        ressourcePropose.setNomEquipe(null);
        return this;
    }

    public void setRessourceProposes(Set<RessourcePropose> ressourceProposes) {
        this.ressourceProposes = ressourceProposes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Equipe)) {
            return false;
        }
        return id != null && id.equals(((Equipe) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Equipe{" +
            "id=" + getId() +
            ", chefEquipe='" + getChefEquipe() + "'" +
            "}";
    }
}
