package com.optimgov.cvs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Formation.
 */
@Entity
@Table(name = "formation")
public class Formation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "etablissemnt")
    private String etablissemnt;

    @Column(name = "date_entree")
    private String dateEntree;

    @Column(name = "date_sortie")
    private String dateSortie;

    @Column(name = "ville")
    private String ville;

    @Column(name = "pays")
    private String pays;

    @Column(name = "titre")
    private String titre;

    @Column(name = "specialite")
    private String specialite;

    @ManyToOne
    @JsonIgnoreProperties(value = "formations", allowSetters = true)
    private RessourcePropose ressourcePropose;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEtablissemnt() {
        return etablissemnt;
    }

    public Formation etablissemnt(String etablissemnt) {
        this.etablissemnt = etablissemnt;
        return this;
    }

    public void setEtablissemnt(String etablissemnt) {
        this.etablissemnt = etablissemnt;
    }

    public String getDateEntree() {
        return dateEntree;
    }

    public Formation dateEntree(String dateEntree) {
        this.dateEntree = dateEntree;
        return this;
    }

    public void setDateEntree(String dateEntree) {
        this.dateEntree = dateEntree;
    }

    public String getDateSortie() {
        return dateSortie;
    }

    public Formation dateSortie(String dateSortie) {
        this.dateSortie = dateSortie;
        return this;
    }

    public void setDateSortie(String dateSortie) {
        this.dateSortie = dateSortie;
    }

    public String getVille() {
        return ville;
    }

    public Formation ville(String ville) {
        this.ville = ville;
        return this;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPays() {
        return pays;
    }

    public Formation pays(String pays) {
        this.pays = pays;
        return this;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getTitre() {
        return titre;
    }

    public Formation titre(String titre) {
        this.titre = titre;
        return this;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getSpecialite() {
        return specialite;
    }

    public Formation specialite(String specialite) {
        this.specialite = specialite;
        return this;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public RessourcePropose getRessourcePropose() {
        return ressourcePropose;
    }

    public Formation ressourcePropose(RessourcePropose ressourcePropose) {
        this.ressourcePropose = ressourcePropose;
        return this;
    }

    public void setRessourcePropose(RessourcePropose ressourcePropose) {
        this.ressourcePropose = ressourcePropose;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Formation)) {
            return false;
        }
        return id != null && id.equals(((Formation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Formation{" +
            "id=" + getId() +
            ", etablissemnt='" + getEtablissemnt() + "'" +
            ", dateEntree='" + getDateEntree() + "'" +
            ", dateSortie='" + getDateSortie() + "'" +
            ", ville='" + getVille() + "'" +
            ", pays='" + getPays() + "'" +
            ", titre='" + getTitre() + "'" +
            ", specialite='" + getSpecialite() + "'" +
            "}";
    }
}
