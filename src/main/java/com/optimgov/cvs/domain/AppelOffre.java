package com.optimgov.cvs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A AppelOffre.
 */
@Entity
@Table(name = "appel_offre")
public class AppelOffre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_date")
    private Instant date;

    @Column(name = "budget")
    private String budget;

    @Column(name = "duree")
    private Integer duree;

    @Column(name = "context")
    private String context;

    @OneToOne
    @JoinColumn(unique = true)
    private ProjetDemande projetsD;

    @OneToMany(mappedBy = "appelOffre")
    private Set<RessourceDemande> ressourceDemandes = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "appelOffres", allowSetters = true)
    private Client client;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public AppelOffre date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getBudget() {
        return budget;
    }

    public AppelOffre budget(String budget) {
        this.budget = budget;
        return this;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public Integer getDuree() {
        return duree;
    }

    public AppelOffre duree(Integer duree) {
        this.duree = duree;
        return this;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    public String getContext() {
        return context;
    }

    public AppelOffre context(String context) {
        this.context = context;
        return this;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public ProjetDemande getProjetsD() {
        return projetsD;
    }

    public AppelOffre projetsD(ProjetDemande projetDemande) {
        this.projetsD = projetDemande;
        return this;
    }

    public void setProjetsD(ProjetDemande projetDemande) {
        this.projetsD = projetDemande;
    }

    public Set<RessourceDemande> getRessourceDemandes() {
        return ressourceDemandes;
    }

    public AppelOffre ressourceDemandes(Set<RessourceDemande> ressourceDemandes) {
        this.ressourceDemandes = ressourceDemandes;
        return this;
    }

    public AppelOffre addRessourceDemande(RessourceDemande ressourceDemande) {
        this.ressourceDemandes.add(ressourceDemande);
        ressourceDemande.setAppelOffre(this);
        return this;
    }

    public AppelOffre removeRessourceDemande(RessourceDemande ressourceDemande) {
        this.ressourceDemandes.remove(ressourceDemande);
        ressourceDemande.setAppelOffre(null);
        return this;
    }

    public void setRessourceDemandes(Set<RessourceDemande> ressourceDemandes) {
        this.ressourceDemandes = ressourceDemandes;
    }

    public Client getClient() {
        return client;
    }

    public AppelOffre client(Client client) {
        this.client = client;
        return this;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AppelOffre)) {
            return false;
        }
        return id != null && id.equals(((AppelOffre) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AppelOffre{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", budget='" + getBudget() + "'" +
            ", duree=" + getDuree() +
            ", context='" + getContext() + "'" +
            "}";
    }
}
