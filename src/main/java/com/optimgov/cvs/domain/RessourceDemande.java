package com.optimgov.cvs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * not an ignored comment
 */
@Entity
@Table(name = "ressource_d")
public class RessourceDemande implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "emploi_actuel")
    private String emploiActuel;

    @Column(name = "anciennete_etude")
    private String ancienneteEtude;

    @Column(name = "anciennete_emploi")
    private String ancienneteEmploi;

    @Column(name = "fonction")
    private String fonction;

    @Column(name = "nb_ressources")
    private Integer nbRessources;

    @OneToMany(mappedBy = "ressourceDemande")
    private Set<RessourcePropose> ressourceProposes = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "ressourceDemandes", allowSetters = true)
    private AppelOffre appelOffre;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmploiActuel() {
        return emploiActuel;
    }

    public RessourceDemande emploiActuel(String emploiActuel) {
        this.emploiActuel = emploiActuel;
        return this;
    }

    public void setEmploiActuel(String emploiActuel) {
        this.emploiActuel = emploiActuel;
    }

    public String getAncienneteEtude() {
        return ancienneteEtude;
    }

    public RessourceDemande ancienneteEtude(String ancienneteEtude) {
        this.ancienneteEtude = ancienneteEtude;
        return this;
    }

    public void setAncienneteEtude(String ancienneteEtude) {
        this.ancienneteEtude = ancienneteEtude;
    }

    public String getAncienneteEmploi() {
        return ancienneteEmploi;
    }

    public RessourceDemande ancienneteEmploi(String ancienneteEmploi) {
        this.ancienneteEmploi = ancienneteEmploi;
        return this;
    }

    public void setAncienneteEmploi(String ancienneteEmploi) {
        this.ancienneteEmploi = ancienneteEmploi;
    }

    public String getFonction() {
        return fonction;
    }

    public RessourceDemande fonction(String fonction) {
        this.fonction = fonction;
        return this;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public Integer getNbRessources() {
        return nbRessources;
    }

    public RessourceDemande nbRessources(Integer nbRessources) {
        this.nbRessources = nbRessources;
        return this;
    }

    public void setNbRessources(Integer nbRessources) {
        this.nbRessources = nbRessources;
    }

    public Set<RessourcePropose> getRessourceProposes() {
        return ressourceProposes;
    }

    public RessourceDemande ressourceProposes(Set<RessourcePropose> ressourceProposes) {
        this.ressourceProposes = ressourceProposes;
        return this;
    }

    public RessourceDemande addRessourcePropose(RessourcePropose ressourcePropose) {
        this.ressourceProposes.add(ressourcePropose);
        ressourcePropose.setRessourceDemande(this);
        return this;
    }

    public RessourceDemande removeRessourcePropose(RessourcePropose ressourcePropose) {
        this.ressourceProposes.remove(ressourcePropose);
        ressourcePropose.setRessourceDemande(null);
        return this;
    }

    public void setRessourceProposes(Set<RessourcePropose> ressourceProposes) {
        this.ressourceProposes = ressourceProposes;
    }

    public AppelOffre getAppelOffre() {
        return appelOffre;
    }

    public RessourceDemande appelOffre(AppelOffre appelOffre) {
        this.appelOffre = appelOffre;
        return this;
    }

    public void setAppelOffre(AppelOffre appelOffre) {
        this.appelOffre = appelOffre;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RessourceDemande)) {
            return false;
        }
        return id != null && id.equals(((RessourceDemande) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RessourceDemande{" +
            "id=" + getId() +
            ", emploiActuel='" + getEmploiActuel() + "'" +
            ", ancienneteEtude='" + getAncienneteEtude() + "'" +
            ", ancienneteEmploi='" + getAncienneteEmploi() + "'" +
            ", fonction='" + getFonction() + "'" +
            ", nbRessources=" + getNbRessources() +
            "}";
    }
}
