package com.optimgov.cvs.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Profil.
 */
@Entity
@Table(name = "profil")
public class Profil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "activite")
    private String activite;

    @ManyToMany
    @JoinTable(name = "profil_qualifications",
               joinColumns = @JoinColumn(name = "profil_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "qualifications_id", referencedColumnName = "id"))
    private Set<BanqueQualification> qualifications = new HashSet<>();

    @ManyToMany(mappedBy = "profils")
    @JsonIgnore
    private Set<BanqueProjet> projets = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActivite() {
        return activite;
    }

    public Profil activite(String activite) {
        this.activite = activite;
        return this;
    }

    public void setActivite(String activite) {
        this.activite = activite;
    }

    public Set<BanqueQualification> getQualifications() {
        return qualifications;
    }

    public Profil qualifications(Set<BanqueQualification> banqueQualifications) {
        this.qualifications = banqueQualifications;
        return this;
    }

    public Profil addQualifications(BanqueQualification banqueQualification) {
        this.qualifications.add(banqueQualification);
        banqueQualification.getProfils().add(this);
        return this;
    }

    public Profil removeQualifications(BanqueQualification banqueQualification) {
        this.qualifications.remove(banqueQualification);
        banqueQualification.getProfils().remove(this);
        return this;
    }

    public void setQualifications(Set<BanqueQualification> banqueQualifications) {
        this.qualifications = banqueQualifications;
    }

    public Set<BanqueProjet> getProjets() {
        return projets;
    }

    public Profil projets(Set<BanqueProjet> banqueProjets) {
        this.projets = banqueProjets;
        return this;
    }

    public Profil addProjets(BanqueProjet banqueProjet) {
        this.projets.add(banqueProjet);
        banqueProjet.getProfils().add(this);
        return this;
    }

    public Profil removeProjets(BanqueProjet banqueProjet) {
        this.projets.remove(banqueProjet);
        banqueProjet.getProfils().remove(this);
        return this;
    }

    public void setProjets(Set<BanqueProjet> banqueProjets) {
        this.projets = banqueProjets;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Profil)) {
            return false;
        }
        return id != null && id.equals(((Profil) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Profil{" +
            "id=" + getId() +
            ", activite='" + getActivite() + "'" +
            "}";
    }
}
