package com.optimgov.cvs.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A ProjetDemande.
 */
@Entity
@Table(name = "projet_demande")
public class ProjetDemande implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(name = "entreprise")
    private String entreprise;

    @Column(name = "duree")
    private String duree;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public ProjetDemande description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEntreprise() {
        return entreprise;
    }

    public ProjetDemande entreprise(String entreprise) {
        this.entreprise = entreprise;
        return this;
    }

    public void setEntreprise(String entreprise) {
        this.entreprise = entreprise;
    }

    public String getDuree() {
        return duree;
    }

    public ProjetDemande duree(String duree) {
        this.duree = duree;
        return this;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProjetDemande)) {
            return false;
        }
        return id != null && id.equals(((ProjetDemande) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProjetDemande{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", entreprise='" + getEntreprise() + "'" +
            ", duree='" + getDuree() + "'" +
            "}";
    }
}
