package com.optimgov.cvs.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Client.
 */
@Entity
@Table(name = "client")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "organisme")
    private String organisme;

    @OneToMany(mappedBy = "client")
    private Set<AppelOffre> appelOffres = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganisme() {
        return organisme;
    }

    public Client organisme(String organisme) {
        this.organisme = organisme;
        return this;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    public Set<AppelOffre> getAppelOffres() {
        return appelOffres;
    }

    public Client appelOffres(Set<AppelOffre> appelOffres) {
        this.appelOffres = appelOffres;
        return this;
    }

    public Client addAppelOffre(AppelOffre appelOffre) {
        this.appelOffres.add(appelOffre);
        appelOffre.setClient(this);
        return this;
    }

    public Client removeAppelOffre(AppelOffre appelOffre) {
        this.appelOffres.remove(appelOffre);
        appelOffre.setClient(null);
        return this;
    }

    public void setAppelOffres(Set<AppelOffre> appelOffres) {
        this.appelOffres = appelOffres;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Client)) {
            return false;
        }
        return id != null && id.equals(((Client) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Client{" +
            "id=" + getId() +
            ", organisme='" + getOrganisme() + "'" +
            "}";
    }
}
