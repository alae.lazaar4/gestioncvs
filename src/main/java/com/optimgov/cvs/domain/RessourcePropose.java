package com.optimgov.cvs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A RessourcePropose.
 */
@Entity
@Table(name = "ressource_p")
public class RessourcePropose implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "poste_actuel")
    private String posteActuel;

    @Column(name = "anciennete")
    private Integer anciennete;

    @OneToMany(mappedBy = "ressourcePropose")
    private Set<Certification> certifications = new HashSet<>();

    @OneToMany(mappedBy = "ressourcePropose")
    private Set<Experience> experiences = new HashSet<>();

    @OneToMany(mappedBy = "ressourcePropose")
    private Set<Formation> formations = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "ressourceProposes", allowSetters = true)
    private RessourceDemande ressourceDemande;

    @ManyToOne
    @JsonIgnoreProperties(value = "ressourceProposes", allowSetters = true)
    private Equipe nomEquipe;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public RessourcePropose nom(String nom) {
        this.nom = nom;
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public RessourcePropose prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPosteActuel() {
        return posteActuel;
    }

    public RessourcePropose posteActuel(String posteActuel) {
        this.posteActuel = posteActuel;
        return this;
    }

    public void setPosteActuel(String posteActuel) {
        this.posteActuel = posteActuel;
    }

    public Integer getAnciennete() {
        return anciennete;
    }

    public RessourcePropose anciennete(Integer anciennete) {
        this.anciennete = anciennete;
        return this;
    }

    public void setAnciennete(Integer anciennete) {
        this.anciennete = anciennete;
    }

    public Set<Certification> getCertifications() {
        return certifications;
    }

    public RessourcePropose certifications(Set<Certification> certifications) {
        this.certifications = certifications;
        return this;
    }

    public RessourcePropose addCertification(Certification certification) {
        this.certifications.add(certification);
        certification.setRessourcePropose(this);
        return this;
    }

    public RessourcePropose removeCertification(Certification certification) {
        this.certifications.remove(certification);
        certification.setRessourcePropose(null);
        return this;
    }

    public void setCertifications(Set<Certification> certifications) {
        this.certifications = certifications;
    }

    public Set<Experience> getExperiences() {
        return experiences;
    }

    public RessourcePropose experiences(Set<Experience> experiences) {
        this.experiences = experiences;
        return this;
    }

    public RessourcePropose addExperience(Experience experience) {
        this.experiences.add(experience);
        experience.setRessourcePropose(this);
        return this;
    }

    public RessourcePropose removeExperience(Experience experience) {
        this.experiences.remove(experience);
        experience.setRessourcePropose(null);
        return this;
    }

    public void setExperiences(Set<Experience> experiences) {
        this.experiences = experiences;
    }

    public Set<Formation> getFormations() {
        return formations;
    }

    public RessourcePropose formations(Set<Formation> formations) {
        this.formations = formations;
        return this;
    }

    public RessourcePropose addFormation(Formation formation) {
        this.formations.add(formation);
        formation.setRessourcePropose(this);
        return this;
    }

    public RessourcePropose removeFormation(Formation formation) {
        this.formations.remove(formation);
        formation.setRessourcePropose(null);
        return this;
    }

    public void setFormations(Set<Formation> formations) {
        this.formations = formations;
    }

    public RessourceDemande getRessourceDemande() {
        return ressourceDemande;
    }

    public RessourcePropose ressourceDemande(RessourceDemande ressourceDemande) {
        this.ressourceDemande = ressourceDemande;
        return this;
    }

    public void setRessourceDemande(RessourceDemande ressourceDemande) {
        this.ressourceDemande = ressourceDemande;
    }

    public Equipe getNomEquipe() {
        return nomEquipe;
    }

    public RessourcePropose nomEquipe(Equipe equipe) {
        this.nomEquipe = equipe;
        return this;
    }

    public void setNomEquipe(Equipe equipe) {
        this.nomEquipe = equipe;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RessourcePropose)) {
            return false;
        }
        return id != null && id.equals(((RessourcePropose) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RessourcePropose{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", posteActuel='" + getPosteActuel() + "'" +
            ", anciennete=" + getAnciennete() +
            "}";
    }
}
