package com.optimgov.cvs.domain;


import javax.persistence.*;

import java.io.Serializable;

/**
 * A Description.
 */
@Entity
@Table(name = "description")
public class Description implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "context")
    private String context;

    @Column(name = "mission")
    private String mission;

    @Column(name = "environnement_technique")
    private String environnementTechnique;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContext() {
        return context;
    }

    public Description context(String context) {
        this.context = context;
        return this;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getMission() {
        return mission;
    }

    public Description mission(String mission) {
        this.mission = mission;
        return this;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getEnvironnementTechnique() {
        return environnementTechnique;
    }

    public Description environnementTechnique(String environnementTechnique) {
        this.environnementTechnique = environnementTechnique;
        return this;
    }

    public void setEnvironnementTechnique(String environnementTechnique) {
        this.environnementTechnique = environnementTechnique;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Description)) {
            return false;
        }
        return id != null && id.equals(((Description) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Description{" +
            "id=" + getId() +
            ", context='" + getContext() + "'" +
            ", mission='" + getMission() + "'" +
            ", environnementTechnique='" + getEnvironnementTechnique() + "'" +
            "}";
    }
}
