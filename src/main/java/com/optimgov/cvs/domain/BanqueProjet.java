package com.optimgov.cvs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A BanqueProjet.
 */
@Entity
@Table(name = "banque_projet")
public class BanqueProjet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "designation")
    private String designation;

    @Column(name = "organisme")
    private String organisme;

    @Column(name = "budget")
    private String budget;

    @Column(name = "annee_projet")
    private Instant anneeProjet;

    @Column(name = "duree")
    private String duree;

    @Column(name = "resultat")
    private String resultat;

    @Column(name = "mission")
    private String mission;

    @ManyToOne
    @JsonIgnoreProperties(value = "nomProjets", allowSetters = true)
    private EnvironTech environTech;

    @ManyToMany
    @JoinTable(name = "banque_projet_profils",
               joinColumns = @JoinColumn(name = "banque_projet_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "profils_id", referencedColumnName = "id"))
    private Set<Profil> profils = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public BanqueProjet designation(String designation) {
        this.designation = designation;
        return this;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getOrganisme() {
        return organisme;
    }

    public BanqueProjet organisme(String organisme) {
        this.organisme = organisme;
        return this;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    public String getBudget() {
        return budget;
    }

    public BanqueProjet budget(String budget) {
        this.budget = budget;
        return this;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public Instant getAnneeProjet() {
        return anneeProjet;
    }

    public BanqueProjet anneeProjet(Instant anneeProjet) {
        this.anneeProjet = anneeProjet;
        return this;
    }

    public void setAnneeProjet(Instant anneeProjet) {
        this.anneeProjet = anneeProjet;
    }

    public String getDuree() {
        return duree;
    }

    public BanqueProjet duree(String duree) {
        this.duree = duree;
        return this;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getResultat() {
        return resultat;
    }

    public BanqueProjet resultat(String resultat) {
        this.resultat = resultat;
        return this;
    }

    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    public String getMission() {
        return mission;
    }

    public BanqueProjet mission(String mission) {
        this.mission = mission;
        return this;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public EnvironTech getEnvironTech() {
        return environTech;
    }

    public BanqueProjet environTech(EnvironTech environTech) {
        this.environTech = environTech;
        return this;
    }

    public void setEnvironTech(EnvironTech environTech) {
        this.environTech = environTech;
    }

    public Set<Profil> getProfils() {
        return profils;
    }

    public BanqueProjet profils(Set<Profil> profils) {
        this.profils = profils;
        return this;
    }

    public BanqueProjet addProfils(Profil profil) {
        this.profils.add(profil);
        profil.getProjets().add(this);
        return this;
    }

    public BanqueProjet removeProfils(Profil profil) {
        this.profils.remove(profil);
        profil.getProjets().remove(this);
        return this;
    }

    public void setProfils(Set<Profil> profils) {
        this.profils = profils;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BanqueProjet)) {
            return false;
        }
        return id != null && id.equals(((BanqueProjet) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BanqueProjet{" +
            "id=" + getId() +
            ", designation='" + getDesignation() + "'" +
            ", organisme='" + getOrganisme() + "'" +
            ", budget='" + getBudget() + "'" +
            ", anneeProjet='" + getAnneeProjet() + "'" +
            ", duree='" + getDuree() + "'" +
            ", resultat='" + getResultat() + "'" +
            ", mission='" + getMission() + "'" +
            "}";
    }
}
