package com.optimgov.cvs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * Task entity.\n@author The JHipster team.
 */
@Entity
@Table(name = "certification")
public class Certification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "intitule")
    private String intitule;

    @Column(name = "organisme")
    private String organisme;

    @Column(name = "annee_obtention")
    private Instant anneeObtention;

    @Column(name = "domaine")
    private String domaine;

    @ManyToOne
    @JsonIgnoreProperties(value = "certifications", allowSetters = true)
    private RessourcePropose ressourcePropose;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public Certification intitule(String intitule) {
        this.intitule = intitule;
        return this;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getOrganisme() {
        return organisme;
    }

    public Certification organisme(String organisme) {
        this.organisme = organisme;
        return this;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    public Instant getAnneeObtention() {
        return anneeObtention;
    }

    public Certification anneeObtention(Instant anneeObtention) {
        this.anneeObtention = anneeObtention;
        return this;
    }

    public void setAnneeObtention(Instant anneeObtention) {
        this.anneeObtention = anneeObtention;
    }

    public String getDomaine() {
        return domaine;
    }

    public Certification domaine(String domaine) {
        this.domaine = domaine;
        return this;
    }

    public void setDomaine(String domaine) {
        this.domaine = domaine;
    }

    public RessourcePropose getRessourcePropose() {
        return ressourcePropose;
    }

    public Certification ressourcePropose(RessourcePropose ressourcePropose) {
        this.ressourcePropose = ressourcePropose;
        return this;
    }

    public void setRessourcePropose(RessourcePropose ressourcePropose) {
        this.ressourcePropose = ressourcePropose;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Certification)) {
            return false;
        }
        return id != null && id.equals(((Certification) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Certification{" +
            "id=" + getId() +
            ", intitule='" + getIntitule() + "'" +
            ", organisme='" + getOrganisme() + "'" +
            ", anneeObtention='" + getAnneeObtention() + "'" +
            ", domaine='" + getDomaine() + "'" +
            "}";
    }
}
