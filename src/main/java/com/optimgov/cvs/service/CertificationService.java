package com.optimgov.cvs.service;

import com.optimgov.cvs.service.dto.CertificationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.optimgov.cvs.domain.Certification}.
 */
public interface CertificationService {

    /**
     * Save a certification.
     *
     * @param certificationDTO the entity to save.
     * @return the persisted entity.
     */
    CertificationDTO save(CertificationDTO certificationDTO);

    /**
     * Get all the certifications.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CertificationDTO> findAll(Pageable pageable);


    /**
     * Get the "id" certification.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CertificationDTO> findOne(Long id);

    /**
     * Delete the "id" certification.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
