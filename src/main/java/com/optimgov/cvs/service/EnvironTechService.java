package com.optimgov.cvs.service;

import com.optimgov.cvs.service.dto.EnvironTechDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.optimgov.cvs.domain.EnvironTech}.
 */
public interface EnvironTechService {

    /**
     * Save a environTech.
     *
     * @param environTechDTO the entity to save.
     * @return the persisted entity.
     */
    EnvironTechDTO save(EnvironTechDTO environTechDTO);

    /**
     * Get all the environTeches.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EnvironTechDTO> findAll(Pageable pageable);


    /**
     * Get the "id" environTech.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EnvironTechDTO> findOne(Long id);

    /**
     * Delete the "id" environTech.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
