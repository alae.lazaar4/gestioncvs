package com.optimgov.cvs.service;

import com.optimgov.cvs.service.dto.ProjetDemandeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.optimgov.cvs.domain.ProjetDemande}.
 */
public interface ProjetDemandeService {

    /**
     * Save a projetDemande.
     *
     * @param projetDemandeDTO the entity to save.
     * @return the persisted entity.
     */
    ProjetDemandeDTO save(ProjetDemandeDTO projetDemandeDTO);

    /**
     * Get all the projetDemandes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProjetDemandeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" projetDemande.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProjetDemandeDTO> findOne(Long id);

    /**
     * Delete the "id" projetDemande.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
