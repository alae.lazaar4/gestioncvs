package com.optimgov.cvs.service;

import com.optimgov.cvs.service.dto.RessourceDemandeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.optimgov.cvs.domain.RessourceDemande}.
 */
public interface RessourceDemandeService {

    /**
     * Save a ressourceDemande.
     *
     * @param ressourceDemandeDTO the entity to save.
     * @return the persisted entity.
     */
    RessourceDemandeDTO save(RessourceDemandeDTO ressourceDemandeDTO);

    /**
     * Get all the ressourceDemandes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RessourceDemandeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" ressourceDemande.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RessourceDemandeDTO> findOne(Long id);

    /**
     * Delete the "id" ressourceDemande.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
