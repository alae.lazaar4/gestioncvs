package com.optimgov.cvs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.ProjetDemande} entity.
 */
public class ProjetDemandeDTO implements Serializable {
    
    private Long id;

    private String description;

    private String entreprise;

    private String duree;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(String entreprise) {
        this.entreprise = entreprise;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProjetDemandeDTO)) {
            return false;
        }

        return id != null && id.equals(((ProjetDemandeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProjetDemandeDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", entreprise='" + getEntreprise() + "'" +
            ", duree='" + getDuree() + "'" +
            "}";
    }
}
