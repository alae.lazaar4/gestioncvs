package com.optimgov.cvs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.EnvironTech} entity.
 */
public class EnvironTechDTO implements Serializable {
    
    private Long id;

    private String webTechnlogies;

    private String webClientTechnologies;

    private String baseDonnee;

    private String projetManagementBuilding;

    private String architechture;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWebTechnlogies() {
        return webTechnlogies;
    }

    public void setWebTechnlogies(String webTechnlogies) {
        this.webTechnlogies = webTechnlogies;
    }

    public String getWebClientTechnologies() {
        return webClientTechnologies;
    }

    public void setWebClientTechnologies(String webClientTechnologies) {
        this.webClientTechnologies = webClientTechnologies;
    }

    public String getBaseDonnee() {
        return baseDonnee;
    }

    public void setBaseDonnee(String baseDonnee) {
        this.baseDonnee = baseDonnee;
    }

    public String getProjetManagementBuilding() {
        return projetManagementBuilding;
    }

    public void setProjetManagementBuilding(String projetManagementBuilding) {
        this.projetManagementBuilding = projetManagementBuilding;
    }

    public String getArchitechture() {
        return architechture;
    }

    public void setArchitechture(String architechture) {
        this.architechture = architechture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EnvironTechDTO)) {
            return false;
        }

        return id != null && id.equals(((EnvironTechDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EnvironTechDTO{" +
            "id=" + getId() +
            ", webTechnlogies='" + getWebTechnlogies() + "'" +
            ", webClientTechnologies='" + getWebClientTechnologies() + "'" +
            ", baseDonnee='" + getBaseDonnee() + "'" +
            ", projetManagementBuilding='" + getProjetManagementBuilding() + "'" +
            ", architechture='" + getArchitechture() + "'" +
            "}";
    }
}
