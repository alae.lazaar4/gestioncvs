package com.optimgov.cvs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.Description} entity.
 */
public class DescriptionDTO implements Serializable {
    
    private Long id;

    private String context;

    private String mission;

    private String environnementTechnique;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getEnvironnementTechnique() {
        return environnementTechnique;
    }

    public void setEnvironnementTechnique(String environnementTechnique) {
        this.environnementTechnique = environnementTechnique;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DescriptionDTO)) {
            return false;
        }

        return id != null && id.equals(((DescriptionDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DescriptionDTO{" +
            "id=" + getId() +
            ", context='" + getContext() + "'" +
            ", mission='" + getMission() + "'" +
            ", environnementTechnique='" + getEnvironnementTechnique() + "'" +
            "}";
    }
}
