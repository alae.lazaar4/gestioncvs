package com.optimgov.cvs.service.dto;

import io.swagger.annotations.ApiModel;
import java.time.Instant;
import java.io.Serializable;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.Certification} entity.
 */
@ApiModel(description = "Task entity.\n@author The JHipster team.")
public class CertificationDTO implements Serializable {
    
    private Long id;

    private String intitule;

    private String organisme;

    private Instant anneeObtention;

    private String domaine;


    private Long ressourceProposeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getOrganisme() {
        return organisme;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    public Instant getAnneeObtention() {
        return anneeObtention;
    }

    public void setAnneeObtention(Instant anneeObtention) {
        this.anneeObtention = anneeObtention;
    }

    public String getDomaine() {
        return domaine;
    }

    public void setDomaine(String domaine) {
        this.domaine = domaine;
    }

    public Long getRessourceProposeId() {
        return ressourceProposeId;
    }

    public void setRessourceProposeId(Long ressourceProposeId) {
        this.ressourceProposeId = ressourceProposeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CertificationDTO)) {
            return false;
        }

        return id != null && id.equals(((CertificationDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CertificationDTO{" +
            "id=" + getId() +
            ", intitule='" + getIntitule() + "'" +
            ", organisme='" + getOrganisme() + "'" +
            ", anneeObtention='" + getAnneeObtention() + "'" +
            ", domaine='" + getDomaine() + "'" +
            ", ressourceProposeId=" + getRessourceProposeId() +
            "}";
    }
}
