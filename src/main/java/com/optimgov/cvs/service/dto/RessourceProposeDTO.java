package com.optimgov.cvs.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.RessourcePropose} entity.
 */
public class RessourceProposeDTO implements Serializable {
    
    private Long id;

    @NotNull
    private String nom;

    private String prenom;

    private String posteActuel;

    private Integer anciennete;


    private Long ressourceDemandeId;

    private Long nomEquipeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPosteActuel() {
        return posteActuel;
    }

    public void setPosteActuel(String posteActuel) {
        this.posteActuel = posteActuel;
    }

    public Integer getAnciennete() {
        return anciennete;
    }

    public void setAnciennete(Integer anciennete) {
        this.anciennete = anciennete;
    }

    public Long getRessourceDemandeId() {
        return ressourceDemandeId;
    }

    public void setRessourceDemandeId(Long ressourceDemandeId) {
        this.ressourceDemandeId = ressourceDemandeId;
    }

    public Long getNomEquipeId() {
        return nomEquipeId;
    }

    public void setNomEquipeId(Long equipeId) {
        this.nomEquipeId = equipeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RessourceProposeDTO)) {
            return false;
        }

        return id != null && id.equals(((RessourceProposeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RessourceProposeDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", posteActuel='" + getPosteActuel() + "'" +
            ", anciennete=" + getAnciennete() +
            ", ressourceDemandeId=" + getRessourceDemandeId() +
            ", nomEquipeId=" + getNomEquipeId() +
            "}";
    }
}
