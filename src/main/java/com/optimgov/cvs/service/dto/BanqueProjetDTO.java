package com.optimgov.cvs.service.dto;

import java.time.Instant;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.BanqueProjet} entity.
 */
public class BanqueProjetDTO implements Serializable {
    
    private Long id;

    private String designation;

    private String organisme;

    private String budget;

    private Instant anneeProjet;

    private String duree;

    private String resultat;

    private String mission;


    private Long environTechId;
    private Set<ProfilDTO> profils = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getOrganisme() {
        return organisme;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public Instant getAnneeProjet() {
        return anneeProjet;
    }

    public void setAnneeProjet(Instant anneeProjet) {
        this.anneeProjet = anneeProjet;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getResultat() {
        return resultat;
    }

    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public Long getEnvironTechId() {
        return environTechId;
    }

    public void setEnvironTechId(Long environTechId) {
        this.environTechId = environTechId;
    }

    public Set<ProfilDTO> getProfils() {
        return profils;
    }

    public void setProfils(Set<ProfilDTO> profils) {
        this.profils = profils;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BanqueProjetDTO)) {
            return false;
        }

        return id != null && id.equals(((BanqueProjetDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BanqueProjetDTO{" +
            "id=" + getId() +
            ", designation='" + getDesignation() + "'" +
            ", organisme='" + getOrganisme() + "'" +
            ", budget='" + getBudget() + "'" +
            ", anneeProjet='" + getAnneeProjet() + "'" +
            ", duree='" + getDuree() + "'" +
            ", resultat='" + getResultat() + "'" +
            ", mission='" + getMission() + "'" +
            ", environTechId=" + getEnvironTechId() +
            ", profils='" + getProfils() + "'" +
            "}";
    }
}
