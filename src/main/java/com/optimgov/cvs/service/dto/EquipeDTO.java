package com.optimgov.cvs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.Equipe} entity.
 */
public class EquipeDTO implements Serializable {
    
    private Long id;

    private String chefEquipe;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChefEquipe() {
        return chefEquipe;
    }

    public void setChefEquipe(String chefEquipe) {
        this.chefEquipe = chefEquipe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EquipeDTO)) {
            return false;
        }

        return id != null && id.equals(((EquipeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EquipeDTO{" +
            "id=" + getId() +
            ", chefEquipe='" + getChefEquipe() + "'" +
            "}";
    }
}
