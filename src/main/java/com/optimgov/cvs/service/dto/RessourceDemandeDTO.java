package com.optimgov.cvs.service.dto;

import io.swagger.annotations.ApiModel;
import java.io.Serializable;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.RessourceDemande} entity.
 */
@ApiModel(description = "not an ignored comment")
public class RessourceDemandeDTO implements Serializable {
    
    private Long id;

    private String emploiActuel;

    private String ancienneteEtude;

    private String ancienneteEmploi;

    private String fonction;

    private Integer nbRessources;


    private Long appelOffreId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmploiActuel() {
        return emploiActuel;
    }

    public void setEmploiActuel(String emploiActuel) {
        this.emploiActuel = emploiActuel;
    }

    public String getAncienneteEtude() {
        return ancienneteEtude;
    }

    public void setAncienneteEtude(String ancienneteEtude) {
        this.ancienneteEtude = ancienneteEtude;
    }

    public String getAncienneteEmploi() {
        return ancienneteEmploi;
    }

    public void setAncienneteEmploi(String ancienneteEmploi) {
        this.ancienneteEmploi = ancienneteEmploi;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public Integer getNbRessources() {
        return nbRessources;
    }

    public void setNbRessources(Integer nbRessources) {
        this.nbRessources = nbRessources;
    }

    public Long getAppelOffreId() {
        return appelOffreId;
    }

    public void setAppelOffreId(Long appelOffreId) {
        this.appelOffreId = appelOffreId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RessourceDemandeDTO)) {
            return false;
        }

        return id != null && id.equals(((RessourceDemandeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RessourceDemandeDTO{" +
            "id=" + getId() +
            ", emploiActuel='" + getEmploiActuel() + "'" +
            ", ancienneteEtude='" + getAncienneteEtude() + "'" +
            ", ancienneteEmploi='" + getAncienneteEmploi() + "'" +
            ", fonction='" + getFonction() + "'" +
            ", nbRessources=" + getNbRessources() +
            ", appelOffreId=" + getAppelOffreId() +
            "}";
    }
}
