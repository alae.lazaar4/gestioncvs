package com.optimgov.cvs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.Formation} entity.
 */
public class FormationDTO implements Serializable {
    
    private Long id;

    private String etablissemnt;

    private String dateEntree;

    private String dateSortie;

    private String ville;

    private String pays;

    private String titre;

    private String specialite;


    private Long ressourceProposeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEtablissemnt() {
        return etablissemnt;
    }

    public void setEtablissemnt(String etablissemnt) {
        this.etablissemnt = etablissemnt;
    }

    public String getDateEntree() {
        return dateEntree;
    }

    public void setDateEntree(String dateEntree) {
        this.dateEntree = dateEntree;
    }

    public String getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(String dateSortie) {
        this.dateSortie = dateSortie;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public Long getRessourceProposeId() {
        return ressourceProposeId;
    }

    public void setRessourceProposeId(Long ressourceProposeId) {
        this.ressourceProposeId = ressourceProposeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FormationDTO)) {
            return false;
        }

        return id != null && id.equals(((FormationDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FormationDTO{" +
            "id=" + getId() +
            ", etablissemnt='" + getEtablissemnt() + "'" +
            ", dateEntree='" + getDateEntree() + "'" +
            ", dateSortie='" + getDateSortie() + "'" +
            ", ville='" + getVille() + "'" +
            ", pays='" + getPays() + "'" +
            ", titre='" + getTitre() + "'" +
            ", specialite='" + getSpecialite() + "'" +
            ", ressourceProposeId=" + getRessourceProposeId() +
            "}";
    }
}
