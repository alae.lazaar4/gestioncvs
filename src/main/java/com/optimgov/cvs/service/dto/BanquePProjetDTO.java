package com.optimgov.cvs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.BanquePProjet} entity.
 */
public class BanquePProjetDTO implements Serializable {
    
    private Long id;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BanquePProjetDTO)) {
            return false;
        }

        return id != null && id.equals(((BanquePProjetDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BanquePProjetDTO{" +
            "id=" + getId() +
            "}";
    }
}
