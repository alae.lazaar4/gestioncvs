package com.optimgov.cvs.service.dto;

import java.time.Instant;
import java.io.Serializable;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.AppelOffre} entity.
 */
public class AppelOffreDTO implements Serializable {
    
    private Long id;

    private Instant date;

    private String budget;

    private Integer duree;

    private String context;


    private Long projetsDId;

    private Long clientId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public Long getProjetsDId() {
        return projetsDId;
    }

    public void setProjetsDId(Long projetDemandeId) {
        this.projetsDId = projetDemandeId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AppelOffreDTO)) {
            return false;
        }

        return id != null && id.equals(((AppelOffreDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AppelOffreDTO{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", budget='" + getBudget() + "'" +
            ", duree=" + getDuree() +
            ", context='" + getContext() + "'" +
            ", projetsDId=" + getProjetsDId() +
            ", clientId=" + getClientId() +
            "}";
    }
}
