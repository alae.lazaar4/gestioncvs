package com.optimgov.cvs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.Client} entity.
 */
public class ClientDTO implements Serializable {
    
    private Long id;

    private String organisme;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganisme() {
        return organisme;
    }

    public void setOrganisme(String organisme) {
        this.organisme = organisme;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientDTO)) {
            return false;
        }

        return id != null && id.equals(((ClientDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClientDTO{" +
            "id=" + getId() +
            ", organisme='" + getOrganisme() + "'" +
            "}";
    }
}
