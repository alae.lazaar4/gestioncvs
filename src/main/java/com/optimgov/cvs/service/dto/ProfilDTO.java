package com.optimgov.cvs.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link com.optimgov.cvs.domain.Profil} entity.
 */
public class ProfilDTO implements Serializable {
    
    private Long id;

    private String activite;

    private Set<BanqueQualificationDTO> qualifications = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActivite() {
        return activite;
    }

    public void setActivite(String activite) {
        this.activite = activite;
    }

    public Set<BanqueQualificationDTO> getQualifications() {
        return qualifications;
    }

    public void setQualifications(Set<BanqueQualificationDTO> banqueQualifications) {
        this.qualifications = banqueQualifications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProfilDTO)) {
            return false;
        }

        return id != null && id.equals(((ProfilDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProfilDTO{" +
            "id=" + getId() +
            ", activite='" + getActivite() + "'" +
            ", qualifications='" + getQualifications() + "'" +
            "}";
    }
}
