package com.optimgov.cvs.service;

import com.optimgov.cvs.service.dto.DescriptionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.optimgov.cvs.domain.Description}.
 */
public interface DescriptionService {

    /**
     * Save a description.
     *
     * @param descriptionDTO the entity to save.
     * @return the persisted entity.
     */
    DescriptionDTO save(DescriptionDTO descriptionDTO);

    /**
     * Get all the descriptions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DescriptionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" description.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DescriptionDTO> findOne(Long id);

    /**
     * Delete the "id" description.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
