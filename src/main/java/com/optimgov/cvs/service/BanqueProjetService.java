package com.optimgov.cvs.service;

import com.optimgov.cvs.service.dto.BanqueProjetDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.optimgov.cvs.domain.BanqueProjet}.
 */
public interface BanqueProjetService {

    /**
     * Save a banqueProjet.
     *
     * @param banqueProjetDTO the entity to save.
     * @return the persisted entity.
     */
    BanqueProjetDTO save(BanqueProjetDTO banqueProjetDTO);

    /**
     * Get all the banqueProjets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BanqueProjetDTO> findAll(Pageable pageable);

    /**
     * Get all the banqueProjets with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<BanqueProjetDTO> findAllWithEagerRelationships(Pageable pageable);


    /**
     * Get the "id" banqueProjet.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BanqueProjetDTO> findOne(Long id);

    /**
     * Delete the "id" banqueProjet.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
