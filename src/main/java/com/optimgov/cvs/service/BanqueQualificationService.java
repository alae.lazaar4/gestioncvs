package com.optimgov.cvs.service;

import com.optimgov.cvs.service.dto.BanqueQualificationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.optimgov.cvs.domain.BanqueQualification}.
 */
public interface BanqueQualificationService {

    /**
     * Save a banqueQualification.
     *
     * @param banqueQualificationDTO the entity to save.
     * @return the persisted entity.
     */
    BanqueQualificationDTO save(BanqueQualificationDTO banqueQualificationDTO);

    /**
     * Get all the banqueQualifications.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BanqueQualificationDTO> findAll(Pageable pageable);


    /**
     * Get the "id" banqueQualification.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BanqueQualificationDTO> findOne(Long id);

    /**
     * Delete the "id" banqueQualification.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
