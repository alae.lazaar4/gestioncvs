package com.optimgov.cvs.service;

import com.optimgov.cvs.service.dto.RessourceProposeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.optimgov.cvs.domain.RessourcePropose}.
 */
public interface RessourceProposeService {

    /**
     * Save a ressourcePropose.
     *
     * @param ressourceProposeDTO the entity to save.
     * @return the persisted entity.
     */
    RessourceProposeDTO save(RessourceProposeDTO ressourceProposeDTO);

    /**
     * Get all the ressourceProposes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RessourceProposeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" ressourcePropose.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RessourceProposeDTO> findOne(Long id);

    /**
     * Delete the "id" ressourcePropose.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
