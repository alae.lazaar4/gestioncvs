package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.EquipeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Equipe} and its DTO {@link EquipeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EquipeMapper extends EntityMapper<EquipeDTO, Equipe> {


    @Mapping(target = "ressourceProposes", ignore = true)
    @Mapping(target = "removeRessourcePropose", ignore = true)
    Equipe toEntity(EquipeDTO equipeDTO);

    default Equipe fromId(Long id) {
        if (id == null) {
            return null;
        }
        Equipe equipe = new Equipe();
        equipe.setId(id);
        return equipe;
    }
}
