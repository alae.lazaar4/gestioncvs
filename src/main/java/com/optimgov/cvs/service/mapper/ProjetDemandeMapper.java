package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.ProjetDemandeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProjetDemande} and its DTO {@link ProjetDemandeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProjetDemandeMapper extends EntityMapper<ProjetDemandeDTO, ProjetDemande> {



    default ProjetDemande fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProjetDemande projetDemande = new ProjetDemande();
        projetDemande.setId(id);
        return projetDemande;
    }
}
