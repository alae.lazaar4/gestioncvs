package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.CertificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Certification} and its DTO {@link CertificationDTO}.
 */
@Mapper(componentModel = "spring", uses = {RessourceProposeMapper.class})
public interface CertificationMapper extends EntityMapper<CertificationDTO, Certification> {

    @Mapping(source = "ressourcePropose.id", target = "ressourceProposeId")
    CertificationDTO toDto(Certification certification);

    @Mapping(source = "ressourceProposeId", target = "ressourcePropose")
    Certification toEntity(CertificationDTO certificationDTO);

    default Certification fromId(Long id) {
        if (id == null) {
            return null;
        }
        Certification certification = new Certification();
        certification.setId(id);
        return certification;
    }
}
