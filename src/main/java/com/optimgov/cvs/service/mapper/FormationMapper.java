package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.FormationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Formation} and its DTO {@link FormationDTO}.
 */
@Mapper(componentModel = "spring", uses = {RessourceProposeMapper.class})
public interface FormationMapper extends EntityMapper<FormationDTO, Formation> {

    @Mapping(source = "ressourcePropose.id", target = "ressourceProposeId")
    FormationDTO toDto(Formation formation);

    @Mapping(source = "ressourceProposeId", target = "ressourcePropose")
    Formation toEntity(FormationDTO formationDTO);

    default Formation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Formation formation = new Formation();
        formation.setId(id);
        return formation;
    }
}
