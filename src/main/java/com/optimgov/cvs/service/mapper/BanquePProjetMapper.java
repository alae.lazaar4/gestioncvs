package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.BanquePProjetDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BanquePProjet} and its DTO {@link BanquePProjetDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BanquePProjetMapper extends EntityMapper<BanquePProjetDTO, BanquePProjet> {



    default BanquePProjet fromId(Long id) {
        if (id == null) {
            return null;
        }
        BanquePProjet banquePProjet = new BanquePProjet();
        banquePProjet.setId(id);
        return banquePProjet;
    }
}
