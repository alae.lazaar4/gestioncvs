package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.BanqueQualificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BanqueQualification} and its DTO {@link BanqueQualificationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BanqueQualificationMapper extends EntityMapper<BanqueQualificationDTO, BanqueQualification> {


    @Mapping(target = "profils", ignore = true)
    @Mapping(target = "removeProfils", ignore = true)
    BanqueQualification toEntity(BanqueQualificationDTO banqueQualificationDTO);

    default BanqueQualification fromId(Long id) {
        if (id == null) {
            return null;
        }
        BanqueQualification banqueQualification = new BanqueQualification();
        banqueQualification.setId(id);
        return banqueQualification;
    }
}
