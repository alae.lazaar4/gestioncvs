package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.BanquePProfilDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BanquePProfil} and its DTO {@link BanquePProfilDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BanquePProfilMapper extends EntityMapper<BanquePProfilDTO, BanquePProfil> {



    default BanquePProfil fromId(Long id) {
        if (id == null) {
            return null;
        }
        BanquePProfil banquePProfil = new BanquePProfil();
        banquePProfil.setId(id);
        return banquePProfil;
    }
}
