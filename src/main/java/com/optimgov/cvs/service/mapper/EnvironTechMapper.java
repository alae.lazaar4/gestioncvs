package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.EnvironTechDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link EnvironTech} and its DTO {@link EnvironTechDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EnvironTechMapper extends EntityMapper<EnvironTechDTO, EnvironTech> {


    @Mapping(target = "nomProjets", ignore = true)
    @Mapping(target = "removeNomProjet", ignore = true)
    EnvironTech toEntity(EnvironTechDTO environTechDTO);

    default EnvironTech fromId(Long id) {
        if (id == null) {
            return null;
        }
        EnvironTech environTech = new EnvironTech();
        environTech.setId(id);
        return environTech;
    }
}
