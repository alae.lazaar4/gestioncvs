package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.ProfilDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Profil} and its DTO {@link ProfilDTO}.
 */
@Mapper(componentModel = "spring", uses = {BanqueQualificationMapper.class})
public interface ProfilMapper extends EntityMapper<ProfilDTO, Profil> {


    @Mapping(target = "removeQualifications", ignore = true)
    @Mapping(target = "projets", ignore = true)
    @Mapping(target = "removeProjets", ignore = true)
    Profil toEntity(ProfilDTO profilDTO);

    default Profil fromId(Long id) {
        if (id == null) {
            return null;
        }
        Profil profil = new Profil();
        profil.setId(id);
        return profil;
    }
}
