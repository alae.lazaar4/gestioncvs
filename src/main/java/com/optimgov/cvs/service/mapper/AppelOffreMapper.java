package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.AppelOffreDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AppelOffre} and its DTO {@link AppelOffreDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProjetDemandeMapper.class, ClientMapper.class})
public interface AppelOffreMapper extends EntityMapper<AppelOffreDTO, AppelOffre> {

    @Mapping(source = "projetsD.id", target = "projetsDId")
    @Mapping(source = "client.id", target = "clientId")
    AppelOffreDTO toDto(AppelOffre appelOffre);

    @Mapping(source = "projetsDId", target = "projetsD")
    @Mapping(target = "ressourceDemandes", ignore = true)
    @Mapping(target = "removeRessourceDemande", ignore = true)
    @Mapping(source = "clientId", target = "client")
    AppelOffre toEntity(AppelOffreDTO appelOffreDTO);

    default AppelOffre fromId(Long id) {
        if (id == null) {
            return null;
        }
        AppelOffre appelOffre = new AppelOffre();
        appelOffre.setId(id);
        return appelOffre;
    }
}
