package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.BanqueProjetDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BanqueProjet} and its DTO {@link BanqueProjetDTO}.
 */
@Mapper(componentModel = "spring", uses = {EnvironTechMapper.class, ProfilMapper.class})
public interface BanqueProjetMapper extends EntityMapper<BanqueProjetDTO, BanqueProjet> {

    @Mapping(source = "environTech.id", target = "environTechId")
    BanqueProjetDTO toDto(BanqueProjet banqueProjet);

    @Mapping(source = "environTechId", target = "environTech")
    @Mapping(target = "removeProfils", ignore = true)
    BanqueProjet toEntity(BanqueProjetDTO banqueProjetDTO);

    default BanqueProjet fromId(Long id) {
        if (id == null) {
            return null;
        }
        BanqueProjet banqueProjet = new BanqueProjet();
        banqueProjet.setId(id);
        return banqueProjet;
    }
}
