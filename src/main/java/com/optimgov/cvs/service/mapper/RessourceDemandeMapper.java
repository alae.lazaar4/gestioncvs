package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.RessourceDemandeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RessourceDemande} and its DTO {@link RessourceDemandeDTO}.
 */
@Mapper(componentModel = "spring", uses = {AppelOffreMapper.class})
public interface RessourceDemandeMapper extends EntityMapper<RessourceDemandeDTO, RessourceDemande> {

    @Mapping(source = "appelOffre.id", target = "appelOffreId")
    RessourceDemandeDTO toDto(RessourceDemande ressourceDemande);

    @Mapping(target = "ressourceProposes", ignore = true)
    @Mapping(target = "removeRessourcePropose", ignore = true)
    @Mapping(source = "appelOffreId", target = "appelOffre")
    RessourceDemande toEntity(RessourceDemandeDTO ressourceDemandeDTO);

    default RessourceDemande fromId(Long id) {
        if (id == null) {
            return null;
        }
        RessourceDemande ressourceDemande = new RessourceDemande();
        ressourceDemande.setId(id);
        return ressourceDemande;
    }
}
