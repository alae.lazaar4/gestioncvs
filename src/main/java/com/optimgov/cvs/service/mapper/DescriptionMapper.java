package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.DescriptionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Description} and its DTO {@link DescriptionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DescriptionMapper extends EntityMapper<DescriptionDTO, Description> {



    default Description fromId(Long id) {
        if (id == null) {
            return null;
        }
        Description description = new Description();
        description.setId(id);
        return description;
    }
}
