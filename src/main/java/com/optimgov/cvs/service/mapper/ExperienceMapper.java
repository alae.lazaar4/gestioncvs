package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.ExperienceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Experience} and its DTO {@link ExperienceDTO}.
 */
@Mapper(componentModel = "spring", uses = {RessourceProposeMapper.class})
public interface ExperienceMapper extends EntityMapper<ExperienceDTO, Experience> {

    @Mapping(source = "ressourcePropose.id", target = "ressourceProposeId")
    ExperienceDTO toDto(Experience experience);

    @Mapping(source = "ressourceProposeId", target = "ressourcePropose")
    Experience toEntity(ExperienceDTO experienceDTO);

    default Experience fromId(Long id) {
        if (id == null) {
            return null;
        }
        Experience experience = new Experience();
        experience.setId(id);
        return experience;
    }
}
