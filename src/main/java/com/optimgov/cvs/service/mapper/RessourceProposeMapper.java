package com.optimgov.cvs.service.mapper;


import com.optimgov.cvs.domain.*;
import com.optimgov.cvs.service.dto.RessourceProposeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RessourcePropose} and its DTO {@link RessourceProposeDTO}.
 */
@Mapper(componentModel = "spring", uses = {RessourceDemandeMapper.class, EquipeMapper.class})
public interface RessourceProposeMapper extends EntityMapper<RessourceProposeDTO, RessourcePropose> {

    @Mapping(source = "ressourceDemande.id", target = "ressourceDemandeId")
    @Mapping(source = "nomEquipe.id", target = "nomEquipeId")
    RessourceProposeDTO toDto(RessourcePropose ressourcePropose);

    @Mapping(target = "certifications", ignore = true)
    @Mapping(target = "removeCertification", ignore = true)
    @Mapping(target = "experiences", ignore = true)
    @Mapping(target = "removeExperience", ignore = true)
    @Mapping(target = "formations", ignore = true)
    @Mapping(target = "removeFormation", ignore = true)
    @Mapping(source = "ressourceDemandeId", target = "ressourceDemande")
    @Mapping(source = "nomEquipeId", target = "nomEquipe")
    RessourcePropose toEntity(RessourceProposeDTO ressourceProposeDTO);

    default RessourcePropose fromId(Long id) {
        if (id == null) {
            return null;
        }
        RessourcePropose ressourcePropose = new RessourcePropose();
        ressourcePropose.setId(id);
        return ressourcePropose;
    }
}
