package com.optimgov.cvs.service;

import com.optimgov.cvs.service.dto.BanquePProjetDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.optimgov.cvs.domain.BanquePProjet}.
 */
public interface BanquePProjetService {

    /**
     * Save a banquePProjet.
     *
     * @param banquePProjetDTO the entity to save.
     * @return the persisted entity.
     */
    BanquePProjetDTO save(BanquePProjetDTO banquePProjetDTO);

    /**
     * Get all the banquePProjets.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BanquePProjetDTO> findAll(Pageable pageable);


    /**
     * Get the "id" banquePProjet.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BanquePProjetDTO> findOne(Long id);

    /**
     * Delete the "id" banquePProjet.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
