package com.optimgov.cvs.service;

import com.optimgov.cvs.service.dto.BanquePProfilDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.optimgov.cvs.domain.BanquePProfil}.
 */
public interface BanquePProfilService {

    /**
     * Save a banquePProfil.
     *
     * @param banquePProfilDTO the entity to save.
     * @return the persisted entity.
     */
    BanquePProfilDTO save(BanquePProfilDTO banquePProfilDTO);

    /**
     * Get all the banquePProfils.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BanquePProfilDTO> findAll(Pageable pageable);


    /**
     * Get the "id" banquePProfil.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BanquePProfilDTO> findOne(Long id);

    /**
     * Delete the "id" banquePProfil.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
