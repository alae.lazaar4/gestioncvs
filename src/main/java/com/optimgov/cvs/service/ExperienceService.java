package com.optimgov.cvs.service;

import com.optimgov.cvs.service.dto.ExperienceDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.optimgov.cvs.domain.Experience}.
 */
public interface ExperienceService {

    /**
     * Save a experience.
     *
     * @param experienceDTO the entity to save.
     * @return the persisted entity.
     */
    ExperienceDTO save(ExperienceDTO experienceDTO);

    /**
     * Get all the experiences.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ExperienceDTO> findAll(Pageable pageable);


    /**
     * Get the "id" experience.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ExperienceDTO> findOne(Long id);

    /**
     * Delete the "id" experience.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
