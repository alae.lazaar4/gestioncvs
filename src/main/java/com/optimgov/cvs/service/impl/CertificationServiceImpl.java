package com.optimgov.cvs.service.impl;

import com.optimgov.cvs.service.CertificationService;
import com.optimgov.cvs.domain.Certification;
import com.optimgov.cvs.repository.CertificationRepository;
import com.optimgov.cvs.service.dto.CertificationDTO;
import com.optimgov.cvs.service.mapper.CertificationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Certification}.
 */
@Service
@Transactional
public class CertificationServiceImpl implements CertificationService {

    private final Logger log = LoggerFactory.getLogger(CertificationServiceImpl.class);

    private final CertificationRepository certificationRepository;

    private final CertificationMapper certificationMapper;

    public CertificationServiceImpl(CertificationRepository certificationRepository, CertificationMapper certificationMapper) {
        this.certificationRepository = certificationRepository;
        this.certificationMapper = certificationMapper;
    }

    @Override
    public CertificationDTO save(CertificationDTO certificationDTO) {
        log.debug("Request to save Certification : {}", certificationDTO);
        Certification certification = certificationMapper.toEntity(certificationDTO);
        certification = certificationRepository.save(certification);
        return certificationMapper.toDto(certification);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CertificationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Certifications");
        return certificationRepository.findAll(pageable)
            .map(certificationMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CertificationDTO> findOne(Long id) {
        log.debug("Request to get Certification : {}", id);
        return certificationRepository.findById(id)
            .map(certificationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Certification : {}", id);
        certificationRepository.deleteById(id);
    }
}
