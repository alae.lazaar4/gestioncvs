package com.optimgov.cvs.service.impl;

import com.optimgov.cvs.service.BanqueQualificationService;
import com.optimgov.cvs.domain.BanqueQualification;
import com.optimgov.cvs.repository.BanqueQualificationRepository;
import com.optimgov.cvs.service.dto.BanqueQualificationDTO;
import com.optimgov.cvs.service.mapper.BanqueQualificationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BanqueQualification}.
 */
@Service
@Transactional
public class BanqueQualificationServiceImpl implements BanqueQualificationService {

    private final Logger log = LoggerFactory.getLogger(BanqueQualificationServiceImpl.class);

    private final BanqueQualificationRepository banqueQualificationRepository;

    private final BanqueQualificationMapper banqueQualificationMapper;

    public BanqueQualificationServiceImpl(BanqueQualificationRepository banqueQualificationRepository, BanqueQualificationMapper banqueQualificationMapper) {
        this.banqueQualificationRepository = banqueQualificationRepository;
        this.banqueQualificationMapper = banqueQualificationMapper;
    }

    @Override
    public BanqueQualificationDTO save(BanqueQualificationDTO banqueQualificationDTO) {
        log.debug("Request to save BanqueQualification : {}", banqueQualificationDTO);
        BanqueQualification banqueQualification = banqueQualificationMapper.toEntity(banqueQualificationDTO);
        banqueQualification = banqueQualificationRepository.save(banqueQualification);
        return banqueQualificationMapper.toDto(banqueQualification);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BanqueQualificationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BanqueQualifications");
        return banqueQualificationRepository.findAll(pageable)
            .map(banqueQualificationMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<BanqueQualificationDTO> findOne(Long id) {
        log.debug("Request to get BanqueQualification : {}", id);
        return banqueQualificationRepository.findById(id)
            .map(banqueQualificationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete BanqueQualification : {}", id);
        banqueQualificationRepository.deleteById(id);
    }
}
