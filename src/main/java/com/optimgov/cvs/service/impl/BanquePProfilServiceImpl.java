package com.optimgov.cvs.service.impl;

import com.optimgov.cvs.service.BanquePProfilService;
import com.optimgov.cvs.domain.BanquePProfil;
import com.optimgov.cvs.repository.BanquePProfilRepository;
import com.optimgov.cvs.service.dto.BanquePProfilDTO;
import com.optimgov.cvs.service.mapper.BanquePProfilMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BanquePProfil}.
 */
@Service
@Transactional
public class BanquePProfilServiceImpl implements BanquePProfilService {

    private final Logger log = LoggerFactory.getLogger(BanquePProfilServiceImpl.class);

    private final BanquePProfilRepository banquePProfilRepository;

    private final BanquePProfilMapper banquePProfilMapper;

    public BanquePProfilServiceImpl(BanquePProfilRepository banquePProfilRepository, BanquePProfilMapper banquePProfilMapper) {
        this.banquePProfilRepository = banquePProfilRepository;
        this.banquePProfilMapper = banquePProfilMapper;
    }

    @Override
    public BanquePProfilDTO save(BanquePProfilDTO banquePProfilDTO) {
        log.debug("Request to save BanquePProfil : {}", banquePProfilDTO);
        BanquePProfil banquePProfil = banquePProfilMapper.toEntity(banquePProfilDTO);
        banquePProfil = banquePProfilRepository.save(banquePProfil);
        return banquePProfilMapper.toDto(banquePProfil);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BanquePProfilDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BanquePProfils");
        return banquePProfilRepository.findAll(pageable)
            .map(banquePProfilMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<BanquePProfilDTO> findOne(Long id) {
        log.debug("Request to get BanquePProfil : {}", id);
        return banquePProfilRepository.findById(id)
            .map(banquePProfilMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete BanquePProfil : {}", id);
        banquePProfilRepository.deleteById(id);
    }
}
