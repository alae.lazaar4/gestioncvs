package com.optimgov.cvs.service.impl;

import com.optimgov.cvs.service.RessourceDemandeService;
import com.optimgov.cvs.domain.RessourceDemande;
import com.optimgov.cvs.repository.RessourceDemandeRepository;
import com.optimgov.cvs.service.dto.RessourceDemandeDTO;
import com.optimgov.cvs.service.mapper.RessourceDemandeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link RessourceDemande}.
 */
@Service
@Transactional
public class RessourceDemandeServiceImpl implements RessourceDemandeService {

    private final Logger log = LoggerFactory.getLogger(RessourceDemandeServiceImpl.class);

    private final RessourceDemandeRepository ressourceDemandeRepository;

    private final RessourceDemandeMapper ressourceDemandeMapper;

    public RessourceDemandeServiceImpl(RessourceDemandeRepository ressourceDemandeRepository, RessourceDemandeMapper ressourceDemandeMapper) {
        this.ressourceDemandeRepository = ressourceDemandeRepository;
        this.ressourceDemandeMapper = ressourceDemandeMapper;
    }

    @Override
    public RessourceDemandeDTO save(RessourceDemandeDTO ressourceDemandeDTO) {
        log.debug("Request to save RessourceDemande : {}", ressourceDemandeDTO);
        RessourceDemande ressourceDemande = ressourceDemandeMapper.toEntity(ressourceDemandeDTO);
        ressourceDemande = ressourceDemandeRepository.save(ressourceDemande);
        return ressourceDemandeMapper.toDto(ressourceDemande);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RessourceDemandeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RessourceDemandes");
        return ressourceDemandeRepository.findAll(pageable)
            .map(ressourceDemandeMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<RessourceDemandeDTO> findOne(Long id) {
        log.debug("Request to get RessourceDemande : {}", id);
        return ressourceDemandeRepository.findById(id)
            .map(ressourceDemandeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RessourceDemande : {}", id);
        ressourceDemandeRepository.deleteById(id);
    }
}
