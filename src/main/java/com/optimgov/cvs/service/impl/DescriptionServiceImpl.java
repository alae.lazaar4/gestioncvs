package com.optimgov.cvs.service.impl;

import com.optimgov.cvs.service.DescriptionService;
import com.optimgov.cvs.domain.Description;
import com.optimgov.cvs.repository.DescriptionRepository;
import com.optimgov.cvs.service.dto.DescriptionDTO;
import com.optimgov.cvs.service.mapper.DescriptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Description}.
 */
@Service
@Transactional
public class DescriptionServiceImpl implements DescriptionService {

    private final Logger log = LoggerFactory.getLogger(DescriptionServiceImpl.class);

    private final DescriptionRepository descriptionRepository;

    private final DescriptionMapper descriptionMapper;

    public DescriptionServiceImpl(DescriptionRepository descriptionRepository, DescriptionMapper descriptionMapper) {
        this.descriptionRepository = descriptionRepository;
        this.descriptionMapper = descriptionMapper;
    }

    @Override
    public DescriptionDTO save(DescriptionDTO descriptionDTO) {
        log.debug("Request to save Description : {}", descriptionDTO);
        Description description = descriptionMapper.toEntity(descriptionDTO);
        description = descriptionRepository.save(description);
        return descriptionMapper.toDto(description);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DescriptionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Descriptions");
        return descriptionRepository.findAll(pageable)
            .map(descriptionMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<DescriptionDTO> findOne(Long id) {
        log.debug("Request to get Description : {}", id);
        return descriptionRepository.findById(id)
            .map(descriptionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Description : {}", id);
        descriptionRepository.deleteById(id);
    }
}
