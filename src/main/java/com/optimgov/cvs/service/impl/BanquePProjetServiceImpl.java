package com.optimgov.cvs.service.impl;

import com.optimgov.cvs.service.BanquePProjetService;
import com.optimgov.cvs.domain.BanquePProjet;
import com.optimgov.cvs.repository.BanquePProjetRepository;
import com.optimgov.cvs.service.dto.BanquePProjetDTO;
import com.optimgov.cvs.service.mapper.BanquePProjetMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BanquePProjet}.
 */
@Service
@Transactional
public class BanquePProjetServiceImpl implements BanquePProjetService {

    private final Logger log = LoggerFactory.getLogger(BanquePProjetServiceImpl.class);

    private final BanquePProjetRepository banquePProjetRepository;

    private final BanquePProjetMapper banquePProjetMapper;

    public BanquePProjetServiceImpl(BanquePProjetRepository banquePProjetRepository, BanquePProjetMapper banquePProjetMapper) {
        this.banquePProjetRepository = banquePProjetRepository;
        this.banquePProjetMapper = banquePProjetMapper;
    }

    @Override
    public BanquePProjetDTO save(BanquePProjetDTO banquePProjetDTO) {
        log.debug("Request to save BanquePProjet : {}", banquePProjetDTO);
        BanquePProjet banquePProjet = banquePProjetMapper.toEntity(banquePProjetDTO);
        banquePProjet = banquePProjetRepository.save(banquePProjet);
        return banquePProjetMapper.toDto(banquePProjet);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BanquePProjetDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BanquePProjets");
        return banquePProjetRepository.findAll(pageable)
            .map(banquePProjetMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<BanquePProjetDTO> findOne(Long id) {
        log.debug("Request to get BanquePProjet : {}", id);
        return banquePProjetRepository.findById(id)
            .map(banquePProjetMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete BanquePProjet : {}", id);
        banquePProjetRepository.deleteById(id);
    }
}
