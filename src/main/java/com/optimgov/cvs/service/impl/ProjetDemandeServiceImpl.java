package com.optimgov.cvs.service.impl;

import com.optimgov.cvs.service.ProjetDemandeService;
import com.optimgov.cvs.domain.ProjetDemande;
import com.optimgov.cvs.repository.ProjetDemandeRepository;
import com.optimgov.cvs.service.dto.ProjetDemandeDTO;
import com.optimgov.cvs.service.mapper.ProjetDemandeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProjetDemande}.
 */
@Service
@Transactional
public class ProjetDemandeServiceImpl implements ProjetDemandeService {

    private final Logger log = LoggerFactory.getLogger(ProjetDemandeServiceImpl.class);

    private final ProjetDemandeRepository projetDemandeRepository;

    private final ProjetDemandeMapper projetDemandeMapper;

    public ProjetDemandeServiceImpl(ProjetDemandeRepository projetDemandeRepository, ProjetDemandeMapper projetDemandeMapper) {
        this.projetDemandeRepository = projetDemandeRepository;
        this.projetDemandeMapper = projetDemandeMapper;
    }

    @Override
    public ProjetDemandeDTO save(ProjetDemandeDTO projetDemandeDTO) {
        log.debug("Request to save ProjetDemande : {}", projetDemandeDTO);
        ProjetDemande projetDemande = projetDemandeMapper.toEntity(projetDemandeDTO);
        projetDemande = projetDemandeRepository.save(projetDemande);
        return projetDemandeMapper.toDto(projetDemande);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ProjetDemandeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProjetDemandes");
        return projetDemandeRepository.findAll(pageable)
            .map(projetDemandeMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ProjetDemandeDTO> findOne(Long id) {
        log.debug("Request to get ProjetDemande : {}", id);
        return projetDemandeRepository.findById(id)
            .map(projetDemandeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProjetDemande : {}", id);
        projetDemandeRepository.deleteById(id);
    }
}
