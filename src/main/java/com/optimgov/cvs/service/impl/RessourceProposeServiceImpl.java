package com.optimgov.cvs.service.impl;

import com.optimgov.cvs.service.RessourceProposeService;
import com.optimgov.cvs.domain.RessourcePropose;
import com.optimgov.cvs.repository.RessourceProposeRepository;
import com.optimgov.cvs.service.dto.RessourceProposeDTO;
import com.optimgov.cvs.service.mapper.RessourceProposeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link RessourcePropose}.
 */
@Service
@Transactional
public class RessourceProposeServiceImpl implements RessourceProposeService {

    private final Logger log = LoggerFactory.getLogger(RessourceProposeServiceImpl.class);

    private final RessourceProposeRepository ressourceProposeRepository;

    private final RessourceProposeMapper ressourceProposeMapper;

    public RessourceProposeServiceImpl(RessourceProposeRepository ressourceProposeRepository, RessourceProposeMapper ressourceProposeMapper) {
        this.ressourceProposeRepository = ressourceProposeRepository;
        this.ressourceProposeMapper = ressourceProposeMapper;
    }

    @Override
    public RessourceProposeDTO save(RessourceProposeDTO ressourceProposeDTO) {
        log.debug("Request to save RessourcePropose : {}", ressourceProposeDTO);
        RessourcePropose ressourcePropose = ressourceProposeMapper.toEntity(ressourceProposeDTO);
        ressourcePropose = ressourceProposeRepository.save(ressourcePropose);
        return ressourceProposeMapper.toDto(ressourcePropose);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RessourceProposeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RessourceProposes");
        return ressourceProposeRepository.findAll(pageable)
            .map(ressourceProposeMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<RessourceProposeDTO> findOne(Long id) {
        log.debug("Request to get RessourcePropose : {}", id);
        return ressourceProposeRepository.findById(id)
            .map(ressourceProposeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RessourcePropose : {}", id);
        ressourceProposeRepository.deleteById(id);
    }
}
