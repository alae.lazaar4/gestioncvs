package com.optimgov.cvs.service.impl;

import com.optimgov.cvs.service.EnvironTechService;
import com.optimgov.cvs.domain.EnvironTech;
import com.optimgov.cvs.repository.EnvironTechRepository;
import com.optimgov.cvs.service.dto.EnvironTechDTO;
import com.optimgov.cvs.service.mapper.EnvironTechMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link EnvironTech}.
 */
@Service
@Transactional
public class EnvironTechServiceImpl implements EnvironTechService {

    private final Logger log = LoggerFactory.getLogger(EnvironTechServiceImpl.class);

    private final EnvironTechRepository environTechRepository;

    private final EnvironTechMapper environTechMapper;

    public EnvironTechServiceImpl(EnvironTechRepository environTechRepository, EnvironTechMapper environTechMapper) {
        this.environTechRepository = environTechRepository;
        this.environTechMapper = environTechMapper;
    }

    @Override
    public EnvironTechDTO save(EnvironTechDTO environTechDTO) {
        log.debug("Request to save EnvironTech : {}", environTechDTO);
        EnvironTech environTech = environTechMapper.toEntity(environTechDTO);
        environTech = environTechRepository.save(environTech);
        return environTechMapper.toDto(environTech);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EnvironTechDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EnvironTeches");
        return environTechRepository.findAll(pageable)
            .map(environTechMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<EnvironTechDTO> findOne(Long id) {
        log.debug("Request to get EnvironTech : {}", id);
        return environTechRepository.findById(id)
            .map(environTechMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete EnvironTech : {}", id);
        environTechRepository.deleteById(id);
    }
}
