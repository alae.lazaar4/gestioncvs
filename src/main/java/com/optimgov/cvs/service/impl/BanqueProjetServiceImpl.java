package com.optimgov.cvs.service.impl;

import com.optimgov.cvs.service.BanqueProjetService;
import com.optimgov.cvs.domain.BanqueProjet;
import com.optimgov.cvs.repository.BanqueProjetRepository;
import com.optimgov.cvs.service.dto.BanqueProjetDTO;
import com.optimgov.cvs.service.mapper.BanqueProjetMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BanqueProjet}.
 */
@Service
@Transactional
public class BanqueProjetServiceImpl implements BanqueProjetService {

    private final Logger log = LoggerFactory.getLogger(BanqueProjetServiceImpl.class);

    private final BanqueProjetRepository banqueProjetRepository;

    private final BanqueProjetMapper banqueProjetMapper;

    public BanqueProjetServiceImpl(BanqueProjetRepository banqueProjetRepository, BanqueProjetMapper banqueProjetMapper) {
        this.banqueProjetRepository = banqueProjetRepository;
        this.banqueProjetMapper = banqueProjetMapper;
    }

    @Override
    public BanqueProjetDTO save(BanqueProjetDTO banqueProjetDTO) {
        log.debug("Request to save BanqueProjet : {}", banqueProjetDTO);
        BanqueProjet banqueProjet = banqueProjetMapper.toEntity(banqueProjetDTO);
        banqueProjet = banqueProjetRepository.save(banqueProjet);
        return banqueProjetMapper.toDto(banqueProjet);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BanqueProjetDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BanqueProjets");
        return banqueProjetRepository.findAll(pageable)
            .map(banqueProjetMapper::toDto);
    }


    public Page<BanqueProjetDTO> findAllWithEagerRelationships(Pageable pageable) {
        return banqueProjetRepository.findAllWithEagerRelationships(pageable).map(banqueProjetMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<BanqueProjetDTO> findOne(Long id) {
        log.debug("Request to get BanqueProjet : {}", id);
        return banqueProjetRepository.findOneWithEagerRelationships(id)
            .map(banqueProjetMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete BanqueProjet : {}", id);
        banqueProjetRepository.deleteById(id);
    }
}
