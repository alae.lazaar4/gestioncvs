package com.optimgov.cvs.repository;

import com.optimgov.cvs.domain.ProjetDemande;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ProjetDemande entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProjetDemandeRepository extends JpaRepository<ProjetDemande, Long> {
}
