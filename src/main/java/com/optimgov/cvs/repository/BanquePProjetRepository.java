package com.optimgov.cvs.repository;

import com.optimgov.cvs.domain.BanquePProjet;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BanquePProjet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BanquePProjetRepository extends JpaRepository<BanquePProjet, Long> {
}
