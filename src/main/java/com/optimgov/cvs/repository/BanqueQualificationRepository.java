package com.optimgov.cvs.repository;

import com.optimgov.cvs.domain.BanqueQualification;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BanqueQualification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BanqueQualificationRepository extends JpaRepository<BanqueQualification, Long> {
}
