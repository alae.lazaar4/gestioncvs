package com.optimgov.cvs.repository;

import com.optimgov.cvs.domain.BanquePProfil;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BanquePProfil entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BanquePProfilRepository extends JpaRepository<BanquePProfil, Long> {
}
