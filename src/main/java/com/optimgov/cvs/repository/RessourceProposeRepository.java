package com.optimgov.cvs.repository;

import com.optimgov.cvs.domain.RessourcePropose;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the RessourcePropose entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RessourceProposeRepository extends JpaRepository<RessourcePropose, Long> {
}
