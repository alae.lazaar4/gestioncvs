package com.optimgov.cvs.repository;

import com.optimgov.cvs.domain.EnvironTech;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the EnvironTech entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EnvironTechRepository extends JpaRepository<EnvironTech, Long> {
}
