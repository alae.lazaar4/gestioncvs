package com.optimgov.cvs.repository;

import com.optimgov.cvs.domain.BanqueProjet;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the BanqueProjet entity.
 */
@Repository
public interface BanqueProjetRepository extends JpaRepository<BanqueProjet, Long> {

    @Query(value = "select distinct banqueProjet from BanqueProjet banqueProjet left join fetch banqueProjet.profils",
        countQuery = "select count(distinct banqueProjet) from BanqueProjet banqueProjet")
    Page<BanqueProjet> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct banqueProjet from BanqueProjet banqueProjet left join fetch banqueProjet.profils")
    List<BanqueProjet> findAllWithEagerRelationships();

    @Query("select banqueProjet from BanqueProjet banqueProjet left join fetch banqueProjet.profils where banqueProjet.id =:id")
    Optional<BanqueProjet> findOneWithEagerRelationships(@Param("id") Long id);
}
