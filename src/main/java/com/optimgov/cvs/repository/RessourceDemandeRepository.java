package com.optimgov.cvs.repository;

import com.optimgov.cvs.domain.RessourceDemande;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the RessourceDemande entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RessourceDemandeRepository extends JpaRepository<RessourceDemande, Long> {
}
