import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProfil, Profil } from 'app/shared/model/profil.model';
import { ProfilService } from './profil.service';
import { IBanqueQualification } from 'app/shared/model/banque-qualification.model';
import { BanqueQualificationService } from 'app/entities/banque-qualification/banque-qualification.service';

@Component({
  selector: 'jhi-profil-update',
  templateUrl: './profil-update.component.html',
})
export class ProfilUpdateComponent implements OnInit {
  isSaving = false;
  banquequalifications: IBanqueQualification[] = [];

  editForm = this.fb.group({
    id: [],
    activite: [],
    qualifications: [],
  });

  constructor(
    protected profilService: ProfilService,
    protected banqueQualificationService: BanqueQualificationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ profil }) => {
      this.updateForm(profil);

      this.banqueQualificationService
        .query()
        .subscribe((res: HttpResponse<IBanqueQualification[]>) => (this.banquequalifications = res.body || []));
    });
  }

  updateForm(profil: IProfil): void {
    this.editForm.patchValue({
      id: profil.id,
      activite: profil.activite,
      qualifications: profil.qualifications,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const profil = this.createFromForm();
    if (profil.id !== undefined) {
      this.subscribeToSaveResponse(this.profilService.update(profil));
    } else {
      this.subscribeToSaveResponse(this.profilService.create(profil));
    }
  }

  private createFromForm(): IProfil {
    return {
      ...new Profil(),
      id: this.editForm.get(['id'])!.value,
      activite: this.editForm.get(['activite'])!.value,
      qualifications: this.editForm.get(['qualifications'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProfil>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IBanqueQualification): any {
    return item.id;
  }

  getSelected(selectedVals: IBanqueQualification[], option: IBanqueQualification): IBanqueQualification {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
