import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBanquePProjet, BanquePProjet } from 'app/shared/model/banque-p-projet.model';
import { BanquePProjetService } from './banque-p-projet.service';

@Component({
  selector: 'jhi-banque-p-projet-update',
  templateUrl: './banque-p-projet-update.component.html',
})
export class BanquePProjetUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
  });

  constructor(protected banquePProjetService: BanquePProjetService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ banquePProjet }) => {
      this.updateForm(banquePProjet);
    });
  }

  updateForm(banquePProjet: IBanquePProjet): void {
    this.editForm.patchValue({
      id: banquePProjet.id,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const banquePProjet = this.createFromForm();
    if (banquePProjet.id !== undefined) {
      this.subscribeToSaveResponse(this.banquePProjetService.update(banquePProjet));
    } else {
      this.subscribeToSaveResponse(this.banquePProjetService.create(banquePProjet));
    }
  }

  private createFromForm(): IBanquePProjet {
    return {
      ...new BanquePProjet(),
      id: this.editForm.get(['id'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBanquePProjet>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
