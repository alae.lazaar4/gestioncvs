import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBanquePProjet, BanquePProjet } from 'app/shared/model/banque-p-projet.model';
import { BanquePProjetService } from './banque-p-projet.service';
import { BanquePProjetComponent } from './banque-p-projet.component';
import { BanquePProjetDetailComponent } from './banque-p-projet-detail.component';
import { BanquePProjetUpdateComponent } from './banque-p-projet-update.component';

@Injectable({ providedIn: 'root' })
export class BanquePProjetResolve implements Resolve<IBanquePProjet> {
  constructor(private service: BanquePProjetService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBanquePProjet> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((banquePProjet: HttpResponse<BanquePProjet>) => {
          if (banquePProjet.body) {
            return of(banquePProjet.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BanquePProjet());
  }
}

export const banquePProjetRoute: Routes = [
  {
    path: '',
    component: BanquePProjetComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'gestioncvsApp.banquePProjet.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BanquePProjetDetailComponent,
    resolve: {
      banquePProjet: BanquePProjetResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.banquePProjet.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BanquePProjetUpdateComponent,
    resolve: {
      banquePProjet: BanquePProjetResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.banquePProjet.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BanquePProjetUpdateComponent,
    resolve: {
      banquePProjet: BanquePProjetResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.banquePProjet.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
