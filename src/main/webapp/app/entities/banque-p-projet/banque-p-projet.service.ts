import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBanquePProjet } from 'app/shared/model/banque-p-projet.model';

type EntityResponseType = HttpResponse<IBanquePProjet>;
type EntityArrayResponseType = HttpResponse<IBanquePProjet[]>;

@Injectable({ providedIn: 'root' })
export class BanquePProjetService {
  public resourceUrl = SERVER_API_URL + 'api/banque-p-projets';

  constructor(protected http: HttpClient) {}

  create(banquePProjet: IBanquePProjet): Observable<EntityResponseType> {
    return this.http.post<IBanquePProjet>(this.resourceUrl, banquePProjet, { observe: 'response' });
  }

  update(banquePProjet: IBanquePProjet): Observable<EntityResponseType> {
    return this.http.put<IBanquePProjet>(this.resourceUrl, banquePProjet, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBanquePProjet>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBanquePProjet[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
