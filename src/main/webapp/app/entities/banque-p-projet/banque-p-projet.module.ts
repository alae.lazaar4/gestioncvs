import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestioncvsSharedModule } from 'app/shared/shared.module';
import { BanquePProjetComponent } from './banque-p-projet.component';
import { BanquePProjetDetailComponent } from './banque-p-projet-detail.component';
import { BanquePProjetUpdateComponent } from './banque-p-projet-update.component';
import { BanquePProjetDeleteDialogComponent } from './banque-p-projet-delete-dialog.component';
import { banquePProjetRoute } from './banque-p-projet.route';

@NgModule({
  imports: [GestioncvsSharedModule, RouterModule.forChild(banquePProjetRoute)],
  declarations: [BanquePProjetComponent, BanquePProjetDetailComponent, BanquePProjetUpdateComponent, BanquePProjetDeleteDialogComponent],
  entryComponents: [BanquePProjetDeleteDialogComponent],
})
export class GestioncvsBanquePProjetModule {}
