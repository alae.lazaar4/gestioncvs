import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBanquePProjet } from 'app/shared/model/banque-p-projet.model';
import { BanquePProjetService } from './banque-p-projet.service';

@Component({
  templateUrl: './banque-p-projet-delete-dialog.component.html',
})
export class BanquePProjetDeleteDialogComponent {
  banquePProjet?: IBanquePProjet;

  constructor(
    protected banquePProjetService: BanquePProjetService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.banquePProjetService.delete(id).subscribe(() => {
      this.eventManager.broadcast('banquePProjetListModification');
      this.activeModal.close();
    });
  }
}
