import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBanquePProjet } from 'app/shared/model/banque-p-projet.model';

@Component({
  selector: 'jhi-banque-p-projet-detail',
  templateUrl: './banque-p-projet-detail.component.html',
})
export class BanquePProjetDetailComponent implements OnInit {
  banquePProjet: IBanquePProjet | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ banquePProjet }) => (this.banquePProjet = banquePProjet));
  }

  previousState(): void {
    window.history.back();
  }
}
