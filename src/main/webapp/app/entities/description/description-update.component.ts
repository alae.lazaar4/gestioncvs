import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IDescription, Description } from 'app/shared/model/description.model';
import { DescriptionService } from './description.service';

@Component({
  selector: 'jhi-description-update',
  templateUrl: './description-update.component.html',
})
export class DescriptionUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    context: [],
    mission: [],
    environnementTechnique: [],
  });

  constructor(protected descriptionService: DescriptionService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ description }) => {
      this.updateForm(description);
    });
  }

  updateForm(description: IDescription): void {
    this.editForm.patchValue({
      id: description.id,
      context: description.context,
      mission: description.mission,
      environnementTechnique: description.environnementTechnique,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const description = this.createFromForm();
    if (description.id !== undefined) {
      this.subscribeToSaveResponse(this.descriptionService.update(description));
    } else {
      this.subscribeToSaveResponse(this.descriptionService.create(description));
    }
  }

  private createFromForm(): IDescription {
    return {
      ...new Description(),
      id: this.editForm.get(['id'])!.value,
      context: this.editForm.get(['context'])!.value,
      mission: this.editForm.get(['mission'])!.value,
      environnementTechnique: this.editForm.get(['environnementTechnique'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDescription>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
