import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestioncvsSharedModule } from 'app/shared/shared.module';
import { DescriptionComponent } from './description.component';
import { DescriptionDetailComponent } from './description-detail.component';
import { DescriptionUpdateComponent } from './description-update.component';
import { DescriptionDeleteDialogComponent } from './description-delete-dialog.component';
import { descriptionRoute } from './description.route';

@NgModule({
  imports: [GestioncvsSharedModule, RouterModule.forChild(descriptionRoute)],
  declarations: [DescriptionComponent, DescriptionDetailComponent, DescriptionUpdateComponent, DescriptionDeleteDialogComponent],
  entryComponents: [DescriptionDeleteDialogComponent],
})
export class GestioncvsDescriptionModule {}
