import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IDescription, Description } from 'app/shared/model/description.model';
import { DescriptionService } from './description.service';
import { DescriptionComponent } from './description.component';
import { DescriptionDetailComponent } from './description-detail.component';
import { DescriptionUpdateComponent } from './description-update.component';

@Injectable({ providedIn: 'root' })
export class DescriptionResolve implements Resolve<IDescription> {
  constructor(private service: DescriptionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDescription> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((description: HttpResponse<Description>) => {
          if (description.body) {
            return of(description.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Description());
  }
}

export const descriptionRoute: Routes = [
  {
    path: '',
    component: DescriptionComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'gestioncvsApp.description.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DescriptionDetailComponent,
    resolve: {
      description: DescriptionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.description.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DescriptionUpdateComponent,
    resolve: {
      description: DescriptionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.description.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DescriptionUpdateComponent,
    resolve: {
      description: DescriptionResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.description.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
