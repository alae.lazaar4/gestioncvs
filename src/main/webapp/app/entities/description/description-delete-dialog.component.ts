import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDescription } from 'app/shared/model/description.model';
import { DescriptionService } from './description.service';

@Component({
  templateUrl: './description-delete-dialog.component.html',
})
export class DescriptionDeleteDialogComponent {
  description?: IDescription;

  constructor(
    protected descriptionService: DescriptionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.descriptionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('descriptionListModification');
      this.activeModal.close();
    });
  }
}
