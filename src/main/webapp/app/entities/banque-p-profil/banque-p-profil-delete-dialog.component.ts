import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBanquePProfil } from 'app/shared/model/banque-p-profil.model';
import { BanquePProfilService } from './banque-p-profil.service';

@Component({
  templateUrl: './banque-p-profil-delete-dialog.component.html',
})
export class BanquePProfilDeleteDialogComponent {
  banquePProfil?: IBanquePProfil;

  constructor(
    protected banquePProfilService: BanquePProfilService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.banquePProfilService.delete(id).subscribe(() => {
      this.eventManager.broadcast('banquePProfilListModification');
      this.activeModal.close();
    });
  }
}
