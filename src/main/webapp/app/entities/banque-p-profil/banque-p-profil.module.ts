import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestioncvsSharedModule } from 'app/shared/shared.module';
import { BanquePProfilComponent } from './banque-p-profil.component';
import { BanquePProfilDetailComponent } from './banque-p-profil-detail.component';
import { BanquePProfilUpdateComponent } from './banque-p-profil-update.component';
import { BanquePProfilDeleteDialogComponent } from './banque-p-profil-delete-dialog.component';
import { banquePProfilRoute } from './banque-p-profil.route';

@NgModule({
  imports: [GestioncvsSharedModule, RouterModule.forChild(banquePProfilRoute)],
  declarations: [BanquePProfilComponent, BanquePProfilDetailComponent, BanquePProfilUpdateComponent, BanquePProfilDeleteDialogComponent],
  entryComponents: [BanquePProfilDeleteDialogComponent],
})
export class GestioncvsBanquePProfilModule {}
