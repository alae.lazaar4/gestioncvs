import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBanquePProfil } from 'app/shared/model/banque-p-profil.model';

@Component({
  selector: 'jhi-banque-p-profil-detail',
  templateUrl: './banque-p-profil-detail.component.html',
})
export class BanquePProfilDetailComponent implements OnInit {
  banquePProfil: IBanquePProfil | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ banquePProfil }) => (this.banquePProfil = banquePProfil));
  }

  previousState(): void {
    window.history.back();
  }
}
