import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBanquePProfil } from 'app/shared/model/banque-p-profil.model';

type EntityResponseType = HttpResponse<IBanquePProfil>;
type EntityArrayResponseType = HttpResponse<IBanquePProfil[]>;

@Injectable({ providedIn: 'root' })
export class BanquePProfilService {
  public resourceUrl = SERVER_API_URL + 'api/banque-p-profils';

  constructor(protected http: HttpClient) {}

  create(banquePProfil: IBanquePProfil): Observable<EntityResponseType> {
    return this.http.post<IBanquePProfil>(this.resourceUrl, banquePProfil, { observe: 'response' });
  }

  update(banquePProfil: IBanquePProfil): Observable<EntityResponseType> {
    return this.http.put<IBanquePProfil>(this.resourceUrl, banquePProfil, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBanquePProfil>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBanquePProfil[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
