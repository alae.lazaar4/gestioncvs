import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBanquePProfil, BanquePProfil } from 'app/shared/model/banque-p-profil.model';
import { BanquePProfilService } from './banque-p-profil.service';
import { BanquePProfilComponent } from './banque-p-profil.component';
import { BanquePProfilDetailComponent } from './banque-p-profil-detail.component';
import { BanquePProfilUpdateComponent } from './banque-p-profil-update.component';

@Injectable({ providedIn: 'root' })
export class BanquePProfilResolve implements Resolve<IBanquePProfil> {
  constructor(private service: BanquePProfilService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBanquePProfil> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((banquePProfil: HttpResponse<BanquePProfil>) => {
          if (banquePProfil.body) {
            return of(banquePProfil.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BanquePProfil());
  }
}

export const banquePProfilRoute: Routes = [
  {
    path: '',
    component: BanquePProfilComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'gestioncvsApp.banquePProfil.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BanquePProfilDetailComponent,
    resolve: {
      banquePProfil: BanquePProfilResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.banquePProfil.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BanquePProfilUpdateComponent,
    resolve: {
      banquePProfil: BanquePProfilResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.banquePProfil.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BanquePProfilUpdateComponent,
    resolve: {
      banquePProfil: BanquePProfilResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.banquePProfil.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
