import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBanquePProfil, BanquePProfil } from 'app/shared/model/banque-p-profil.model';
import { BanquePProfilService } from './banque-p-profil.service';

@Component({
  selector: 'jhi-banque-p-profil-update',
  templateUrl: './banque-p-profil-update.component.html',
})
export class BanquePProfilUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
  });

  constructor(protected banquePProfilService: BanquePProfilService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ banquePProfil }) => {
      this.updateForm(banquePProfil);
    });
  }

  updateForm(banquePProfil: IBanquePProfil): void {
    this.editForm.patchValue({
      id: banquePProfil.id,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const banquePProfil = this.createFromForm();
    if (banquePProfil.id !== undefined) {
      this.subscribeToSaveResponse(this.banquePProfilService.update(banquePProfil));
    } else {
      this.subscribeToSaveResponse(this.banquePProfilService.create(banquePProfil));
    }
  }

  private createFromForm(): IBanquePProfil {
    return {
      ...new BanquePProfil(),
      id: this.editForm.get(['id'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBanquePProfil>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
