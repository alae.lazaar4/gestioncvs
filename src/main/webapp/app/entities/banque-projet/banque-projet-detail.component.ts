import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBanqueProjet } from 'app/shared/model/banque-projet.model';

@Component({
  selector: 'jhi-banque-projet-detail',
  templateUrl: './banque-projet-detail.component.html',
})
export class BanqueProjetDetailComponent implements OnInit {
  banqueProjet: IBanqueProjet | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ banqueProjet }) => (this.banqueProjet = banqueProjet));
  }

  previousState(): void {
    window.history.back();
  }
}
