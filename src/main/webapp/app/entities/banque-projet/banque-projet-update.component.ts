import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IBanqueProjet, BanqueProjet } from 'app/shared/model/banque-projet.model';
import { BanqueProjetService } from './banque-projet.service';
import { IEnvironTech } from 'app/shared/model/environ-tech.model';
import { EnvironTechService } from 'app/entities/environ-tech/environ-tech.service';
import { IProfil } from 'app/shared/model/profil.model';
import { ProfilService } from 'app/entities/profil/profil.service';

type SelectableEntity = IEnvironTech | IProfil;

@Component({
  selector: 'jhi-banque-projet-update',
  templateUrl: './banque-projet-update.component.html',
})
export class BanqueProjetUpdateComponent implements OnInit {
  isSaving = false;
  environteches: IEnvironTech[] = [];
  profils: IProfil[] = [];

  editForm = this.fb.group({
    id: [],
    designation: [],
    organisme: [],
    budget: [],
    anneeProjet: [],
    duree: [],
    resultat: [],
    mission: [],
    environTechId: [],
    profils: [],
  });

  constructor(
    protected banqueProjetService: BanqueProjetService,
    protected environTechService: EnvironTechService,
    protected profilService: ProfilService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ banqueProjet }) => {
      if (!banqueProjet.id) {
        const today = moment().startOf('day');
        banqueProjet.anneeProjet = today;
      }

      this.updateForm(banqueProjet);

      this.environTechService.query().subscribe((res: HttpResponse<IEnvironTech[]>) => (this.environteches = res.body || []));

      this.profilService.query().subscribe((res: HttpResponse<IProfil[]>) => (this.profils = res.body || []));
    });
  }

  updateForm(banqueProjet: IBanqueProjet): void {
    this.editForm.patchValue({
      id: banqueProjet.id,
      designation: banqueProjet.designation,
      organisme: banqueProjet.organisme,
      budget: banqueProjet.budget,
      anneeProjet: banqueProjet.anneeProjet ? banqueProjet.anneeProjet.format(DATE_TIME_FORMAT) : null,
      duree: banqueProjet.duree,
      resultat: banqueProjet.resultat,
      mission: banqueProjet.mission,
      environTechId: banqueProjet.environTechId,
      profils: banqueProjet.profils,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const banqueProjet = this.createFromForm();
    if (banqueProjet.id !== undefined) {
      this.subscribeToSaveResponse(this.banqueProjetService.update(banqueProjet));
    } else {
      this.subscribeToSaveResponse(this.banqueProjetService.create(banqueProjet));
    }
  }

  private createFromForm(): IBanqueProjet {
    return {
      ...new BanqueProjet(),
      id: this.editForm.get(['id'])!.value,
      designation: this.editForm.get(['designation'])!.value,
      organisme: this.editForm.get(['organisme'])!.value,
      budget: this.editForm.get(['budget'])!.value,
      anneeProjet: this.editForm.get(['anneeProjet'])!.value
        ? moment(this.editForm.get(['anneeProjet'])!.value, DATE_TIME_FORMAT)
        : undefined,
      duree: this.editForm.get(['duree'])!.value,
      resultat: this.editForm.get(['resultat'])!.value,
      mission: this.editForm.get(['mission'])!.value,
      environTechId: this.editForm.get(['environTechId'])!.value,
      profils: this.editForm.get(['profils'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBanqueProjet>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: IProfil[], option: IProfil): IProfil {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
