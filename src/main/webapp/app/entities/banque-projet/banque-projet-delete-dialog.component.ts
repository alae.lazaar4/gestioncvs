import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBanqueProjet } from 'app/shared/model/banque-projet.model';
import { BanqueProjetService } from './banque-projet.service';

@Component({
  templateUrl: './banque-projet-delete-dialog.component.html',
})
export class BanqueProjetDeleteDialogComponent {
  banqueProjet?: IBanqueProjet;

  constructor(
    protected banqueProjetService: BanqueProjetService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.banqueProjetService.delete(id).subscribe(() => {
      this.eventManager.broadcast('banqueProjetListModification');
      this.activeModal.close();
    });
  }
}
