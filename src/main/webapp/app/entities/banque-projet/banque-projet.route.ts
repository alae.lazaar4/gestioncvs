import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBanqueProjet, BanqueProjet } from 'app/shared/model/banque-projet.model';
import { BanqueProjetService } from './banque-projet.service';
import { BanqueProjetComponent } from './banque-projet.component';
import { BanqueProjetDetailComponent } from './banque-projet-detail.component';
import { BanqueProjetUpdateComponent } from './banque-projet-update.component';

@Injectable({ providedIn: 'root' })
export class BanqueProjetResolve implements Resolve<IBanqueProjet> {
  constructor(private service: BanqueProjetService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBanqueProjet> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((banqueProjet: HttpResponse<BanqueProjet>) => {
          if (banqueProjet.body) {
            return of(banqueProjet.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BanqueProjet());
  }
}

export const banqueProjetRoute: Routes = [
  {
    path: '',
    component: BanqueProjetComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'gestioncvsApp.banqueProjet.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BanqueProjetDetailComponent,
    resolve: {
      banqueProjet: BanqueProjetResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.banqueProjet.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BanqueProjetUpdateComponent,
    resolve: {
      banqueProjet: BanqueProjetResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.banqueProjet.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BanqueProjetUpdateComponent,
    resolve: {
      banqueProjet: BanqueProjetResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.banqueProjet.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
