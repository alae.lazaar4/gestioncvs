import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBanqueProjet } from 'app/shared/model/banque-projet.model';

type EntityResponseType = HttpResponse<IBanqueProjet>;
type EntityArrayResponseType = HttpResponse<IBanqueProjet[]>;

@Injectable({ providedIn: 'root' })
export class BanqueProjetService {
  public resourceUrl = SERVER_API_URL + 'api/banque-projets';

  constructor(protected http: HttpClient) {}

  create(banqueProjet: IBanqueProjet): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(banqueProjet);
    return this.http
      .post<IBanqueProjet>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(banqueProjet: IBanqueProjet): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(banqueProjet);
    return this.http
      .put<IBanqueProjet>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBanqueProjet>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBanqueProjet[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(banqueProjet: IBanqueProjet): IBanqueProjet {
    const copy: IBanqueProjet = Object.assign({}, banqueProjet, {
      anneeProjet: banqueProjet.anneeProjet && banqueProjet.anneeProjet.isValid() ? banqueProjet.anneeProjet.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.anneeProjet = res.body.anneeProjet ? moment(res.body.anneeProjet) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((banqueProjet: IBanqueProjet) => {
        banqueProjet.anneeProjet = banqueProjet.anneeProjet ? moment(banqueProjet.anneeProjet) : undefined;
      });
    }
    return res;
  }
}
