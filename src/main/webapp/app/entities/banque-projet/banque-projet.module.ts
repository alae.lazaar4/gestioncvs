import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestioncvsSharedModule } from 'app/shared/shared.module';
import { BanqueProjetComponent } from './banque-projet.component';
import { BanqueProjetDetailComponent } from './banque-projet-detail.component';
import { BanqueProjetUpdateComponent } from './banque-projet-update.component';
import { BanqueProjetDeleteDialogComponent } from './banque-projet-delete-dialog.component';
import { banqueProjetRoute } from './banque-projet.route';

@NgModule({
  imports: [GestioncvsSharedModule, RouterModule.forChild(banqueProjetRoute)],
  declarations: [BanqueProjetComponent, BanqueProjetDetailComponent, BanqueProjetUpdateComponent, BanqueProjetDeleteDialogComponent],
  entryComponents: [BanqueProjetDeleteDialogComponent],
})
export class GestioncvsBanqueProjetModule {}
