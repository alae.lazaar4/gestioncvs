import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRessourceDemande } from 'app/shared/model/ressource-demande.model';

type EntityResponseType = HttpResponse<IRessourceDemande>;
type EntityArrayResponseType = HttpResponse<IRessourceDemande[]>;

@Injectable({ providedIn: 'root' })
export class RessourceDemandeService {
  public resourceUrl = SERVER_API_URL + 'api/ressource-demandes';

  constructor(protected http: HttpClient) {}

  create(ressourceDemande: IRessourceDemande): Observable<EntityResponseType> {
    return this.http.post<IRessourceDemande>(this.resourceUrl, ressourceDemande, { observe: 'response' });
  }

  update(ressourceDemande: IRessourceDemande): Observable<EntityResponseType> {
    return this.http.put<IRessourceDemande>(this.resourceUrl, ressourceDemande, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRessourceDemande>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRessourceDemande[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
