import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRessourceDemande } from 'app/shared/model/ressource-demande.model';

@Component({
  selector: 'jhi-ressource-demande-detail',
  templateUrl: './ressource-demande-detail.component.html',
})
export class RessourceDemandeDetailComponent implements OnInit {
  ressourceDemande: IRessourceDemande | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ressourceDemande }) => (this.ressourceDemande = ressourceDemande));
  }

  previousState(): void {
    window.history.back();
  }
}
