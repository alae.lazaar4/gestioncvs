import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestioncvsSharedModule } from 'app/shared/shared.module';
import { RessourceDemandeComponent } from './ressource-demande.component';
import { RessourceDemandeDetailComponent } from './ressource-demande-detail.component';
import { RessourceDemandeUpdateComponent } from './ressource-demande-update.component';
import { RessourceDemandeDeleteDialogComponent } from './ressource-demande-delete-dialog.component';
import { ressourceDemandeRoute } from './ressource-demande.route';

@NgModule({
  imports: [GestioncvsSharedModule, RouterModule.forChild(ressourceDemandeRoute)],
  declarations: [
    RessourceDemandeComponent,
    RessourceDemandeDetailComponent,
    RessourceDemandeUpdateComponent,
    RessourceDemandeDeleteDialogComponent,
  ],
  entryComponents: [RessourceDemandeDeleteDialogComponent],
})
export class GestioncvsRessourceDemandeModule {}
