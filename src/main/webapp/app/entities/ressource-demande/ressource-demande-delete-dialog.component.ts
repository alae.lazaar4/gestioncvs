import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRessourceDemande } from 'app/shared/model/ressource-demande.model';
import { RessourceDemandeService } from './ressource-demande.service';

@Component({
  templateUrl: './ressource-demande-delete-dialog.component.html',
})
export class RessourceDemandeDeleteDialogComponent {
  ressourceDemande?: IRessourceDemande;

  constructor(
    protected ressourceDemandeService: RessourceDemandeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.ressourceDemandeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('ressourceDemandeListModification');
      this.activeModal.close();
    });
  }
}
