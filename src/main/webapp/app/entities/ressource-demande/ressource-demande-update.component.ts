import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRessourceDemande, RessourceDemande } from 'app/shared/model/ressource-demande.model';
import { RessourceDemandeService } from './ressource-demande.service';
import { IAppelOffre } from 'app/shared/model/appel-offre.model';
import { AppelOffreService } from 'app/entities/appel-offre/appel-offre.service';

@Component({
  selector: 'jhi-ressource-demande-update',
  templateUrl: './ressource-demande-update.component.html',
})
export class RessourceDemandeUpdateComponent implements OnInit {
  isSaving = false;
  appeloffres: IAppelOffre[] = [];

  editForm = this.fb.group({
    id: [],
    emploiActuel: [],
    ancienneteEtude: [],
    ancienneteEmploi: [],
    fonction: [],
    nbRessources: [],
    appelOffreId: [],
  });

  constructor(
    protected ressourceDemandeService: RessourceDemandeService,
    protected appelOffreService: AppelOffreService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ressourceDemande }) => {
      this.updateForm(ressourceDemande);

      this.appelOffreService.query().subscribe((res: HttpResponse<IAppelOffre[]>) => (this.appeloffres = res.body || []));
    });
  }

  updateForm(ressourceDemande: IRessourceDemande): void {
    this.editForm.patchValue({
      id: ressourceDemande.id,
      emploiActuel: ressourceDemande.emploiActuel,
      ancienneteEtude: ressourceDemande.ancienneteEtude,
      ancienneteEmploi: ressourceDemande.ancienneteEmploi,
      fonction: ressourceDemande.fonction,
      nbRessources: ressourceDemande.nbRessources,
      appelOffreId: ressourceDemande.appelOffreId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const ressourceDemande = this.createFromForm();
    if (ressourceDemande.id !== undefined) {
      this.subscribeToSaveResponse(this.ressourceDemandeService.update(ressourceDemande));
    } else {
      this.subscribeToSaveResponse(this.ressourceDemandeService.create(ressourceDemande));
    }
  }

  private createFromForm(): IRessourceDemande {
    return {
      ...new RessourceDemande(),
      id: this.editForm.get(['id'])!.value,
      emploiActuel: this.editForm.get(['emploiActuel'])!.value,
      ancienneteEtude: this.editForm.get(['ancienneteEtude'])!.value,
      ancienneteEmploi: this.editForm.get(['ancienneteEmploi'])!.value,
      fonction: this.editForm.get(['fonction'])!.value,
      nbRessources: this.editForm.get(['nbRessources'])!.value,
      appelOffreId: this.editForm.get(['appelOffreId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRessourceDemande>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IAppelOffre): any {
    return item.id;
  }
}
