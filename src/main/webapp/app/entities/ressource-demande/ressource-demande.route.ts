import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRessourceDemande, RessourceDemande } from 'app/shared/model/ressource-demande.model';
import { RessourceDemandeService } from './ressource-demande.service';
import { RessourceDemandeComponent } from './ressource-demande.component';
import { RessourceDemandeDetailComponent } from './ressource-demande-detail.component';
import { RessourceDemandeUpdateComponent } from './ressource-demande-update.component';

@Injectable({ providedIn: 'root' })
export class RessourceDemandeResolve implements Resolve<IRessourceDemande> {
  constructor(private service: RessourceDemandeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRessourceDemande> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((ressourceDemande: HttpResponse<RessourceDemande>) => {
          if (ressourceDemande.body) {
            return of(ressourceDemande.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RessourceDemande());
  }
}

export const ressourceDemandeRoute: Routes = [
  {
    path: '',
    component: RessourceDemandeComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'gestioncvsApp.ressourceDemande.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RessourceDemandeDetailComponent,
    resolve: {
      ressourceDemande: RessourceDemandeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.ressourceDemande.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RessourceDemandeUpdateComponent,
    resolve: {
      ressourceDemande: RessourceDemandeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.ressourceDemande.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RessourceDemandeUpdateComponent,
    resolve: {
      ressourceDemande: RessourceDemandeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.ressourceDemande.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
