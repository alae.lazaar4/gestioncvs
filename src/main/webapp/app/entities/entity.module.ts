import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'appel-offre',
        loadChildren: () => import('./appel-offre/appel-offre.module').then(m => m.GestioncvsAppelOffreModule),
      },
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.GestioncvsClientModule),
      },
      {
        path: 'ressource-demande',
        loadChildren: () => import('./ressource-demande/ressource-demande.module').then(m => m.GestioncvsRessourceDemandeModule),
      },
      {
        path: 'ressource-propose',
        loadChildren: () => import('./ressource-propose/ressource-propose.module').then(m => m.GestioncvsRessourceProposeModule),
      },
      {
        path: 'certification',
        loadChildren: () => import('./certification/certification.module').then(m => m.GestioncvsCertificationModule),
      },
      {
        path: 'experience',
        loadChildren: () => import('./experience/experience.module').then(m => m.GestioncvsExperienceModule),
      },
      {
        path: 'formation',
        loadChildren: () => import('./formation/formation.module').then(m => m.GestioncvsFormationModule),
      },
      {
        path: 'banque-projet',
        loadChildren: () => import('./banque-projet/banque-projet.module').then(m => m.GestioncvsBanqueProjetModule),
      },
      {
        path: 'description',
        loadChildren: () => import('./description/description.module').then(m => m.GestioncvsDescriptionModule),
      },
      {
        path: 'banque-qualification',
        loadChildren: () => import('./banque-qualification/banque-qualification.module').then(m => m.GestioncvsBanqueQualificationModule),
      },
      {
        path: 'profil',
        loadChildren: () => import('./profil/profil.module').then(m => m.GestioncvsProfilModule),
      },
      {
        path: 'banque-p-projet',
        loadChildren: () => import('./banque-p-projet/banque-p-projet.module').then(m => m.GestioncvsBanquePProjetModule),
      },
      {
        path: 'banque-p-profil',
        loadChildren: () => import('./banque-p-profil/banque-p-profil.module').then(m => m.GestioncvsBanquePProfilModule),
      },
      {
        path: 'projet-demande',
        loadChildren: () => import('./projet-demande/projet-demande.module').then(m => m.GestioncvsProjetDemandeModule),
      },
      {
        path: 'equipe',
        loadChildren: () => import('./equipe/equipe.module').then(m => m.GestioncvsEquipeModule),
      },
      {
        path: 'environ-tech',
        loadChildren: () => import('./environ-tech/environ-tech.module').then(m => m.GestioncvsEnvironTechModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class GestioncvsEntityModule {}
