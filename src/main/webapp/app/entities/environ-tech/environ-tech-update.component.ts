import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IEnvironTech, EnvironTech } from 'app/shared/model/environ-tech.model';
import { EnvironTechService } from './environ-tech.service';

@Component({
  selector: 'jhi-environ-tech-update',
  templateUrl: './environ-tech-update.component.html',
})
export class EnvironTechUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    webTechnlogies: [],
    webClientTechnologies: [],
    baseDonnee: [],
    projetManagementBuilding: [],
    architechture: [],
  });

  constructor(protected environTechService: EnvironTechService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ environTech }) => {
      this.updateForm(environTech);
    });
  }

  updateForm(environTech: IEnvironTech): void {
    this.editForm.patchValue({
      id: environTech.id,
      webTechnlogies: environTech.webTechnlogies,
      webClientTechnologies: environTech.webClientTechnologies,
      baseDonnee: environTech.baseDonnee,
      projetManagementBuilding: environTech.projetManagementBuilding,
      architechture: environTech.architechture,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const environTech = this.createFromForm();
    if (environTech.id !== undefined) {
      this.subscribeToSaveResponse(this.environTechService.update(environTech));
    } else {
      this.subscribeToSaveResponse(this.environTechService.create(environTech));
    }
  }

  private createFromForm(): IEnvironTech {
    return {
      ...new EnvironTech(),
      id: this.editForm.get(['id'])!.value,
      webTechnlogies: this.editForm.get(['webTechnlogies'])!.value,
      webClientTechnologies: this.editForm.get(['webClientTechnologies'])!.value,
      baseDonnee: this.editForm.get(['baseDonnee'])!.value,
      projetManagementBuilding: this.editForm.get(['projetManagementBuilding'])!.value,
      architechture: this.editForm.get(['architechture'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEnvironTech>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
