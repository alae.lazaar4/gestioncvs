import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEnvironTech } from 'app/shared/model/environ-tech.model';

@Component({
  selector: 'jhi-environ-tech-detail',
  templateUrl: './environ-tech-detail.component.html',
})
export class EnvironTechDetailComponent implements OnInit {
  environTech: IEnvironTech | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ environTech }) => (this.environTech = environTech));
  }

  previousState(): void {
    window.history.back();
  }
}
