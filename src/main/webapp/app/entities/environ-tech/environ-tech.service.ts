import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IEnvironTech } from 'app/shared/model/environ-tech.model';

type EntityResponseType = HttpResponse<IEnvironTech>;
type EntityArrayResponseType = HttpResponse<IEnvironTech[]>;

@Injectable({ providedIn: 'root' })
export class EnvironTechService {
  public resourceUrl = SERVER_API_URL + 'api/environ-teches';

  constructor(protected http: HttpClient) {}

  create(environTech: IEnvironTech): Observable<EntityResponseType> {
    return this.http.post<IEnvironTech>(this.resourceUrl, environTech, { observe: 'response' });
  }

  update(environTech: IEnvironTech): Observable<EntityResponseType> {
    return this.http.put<IEnvironTech>(this.resourceUrl, environTech, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEnvironTech>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEnvironTech[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
