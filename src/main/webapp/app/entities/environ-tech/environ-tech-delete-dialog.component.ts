import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IEnvironTech } from 'app/shared/model/environ-tech.model';
import { EnvironTechService } from './environ-tech.service';

@Component({
  templateUrl: './environ-tech-delete-dialog.component.html',
})
export class EnvironTechDeleteDialogComponent {
  environTech?: IEnvironTech;

  constructor(
    protected environTechService: EnvironTechService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.environTechService.delete(id).subscribe(() => {
      this.eventManager.broadcast('environTechListModification');
      this.activeModal.close();
    });
  }
}
