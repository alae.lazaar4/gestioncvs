import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestioncvsSharedModule } from 'app/shared/shared.module';
import { EnvironTechComponent } from './environ-tech.component';
import { EnvironTechDetailComponent } from './environ-tech-detail.component';
import { EnvironTechUpdateComponent } from './environ-tech-update.component';
import { EnvironTechDeleteDialogComponent } from './environ-tech-delete-dialog.component';
import { environTechRoute } from './environ-tech.route';

@NgModule({
  imports: [GestioncvsSharedModule, RouterModule.forChild(environTechRoute)],
  declarations: [EnvironTechComponent, EnvironTechDetailComponent, EnvironTechUpdateComponent, EnvironTechDeleteDialogComponent],
  entryComponents: [EnvironTechDeleteDialogComponent],
})
export class GestioncvsEnvironTechModule {}
