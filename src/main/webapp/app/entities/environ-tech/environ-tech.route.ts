import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IEnvironTech, EnvironTech } from 'app/shared/model/environ-tech.model';
import { EnvironTechService } from './environ-tech.service';
import { EnvironTechComponent } from './environ-tech.component';
import { EnvironTechDetailComponent } from './environ-tech-detail.component';
import { EnvironTechUpdateComponent } from './environ-tech-update.component';

@Injectable({ providedIn: 'root' })
export class EnvironTechResolve implements Resolve<IEnvironTech> {
  constructor(private service: EnvironTechService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEnvironTech> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((environTech: HttpResponse<EnvironTech>) => {
          if (environTech.body) {
            return of(environTech.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new EnvironTech());
  }
}

export const environTechRoute: Routes = [
  {
    path: '',
    component: EnvironTechComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'gestioncvsApp.environTech.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EnvironTechDetailComponent,
    resolve: {
      environTech: EnvironTechResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.environTech.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EnvironTechUpdateComponent,
    resolve: {
      environTech: EnvironTechResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.environTech.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EnvironTechUpdateComponent,
    resolve: {
      environTech: EnvironTechResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.environTech.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
