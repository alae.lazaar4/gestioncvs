import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBanqueQualification } from 'app/shared/model/banque-qualification.model';
import { BanqueQualificationService } from './banque-qualification.service';

@Component({
  templateUrl: './banque-qualification-delete-dialog.component.html',
})
export class BanqueQualificationDeleteDialogComponent {
  banqueQualification?: IBanqueQualification;

  constructor(
    protected banqueQualificationService: BanqueQualificationService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.banqueQualificationService.delete(id).subscribe(() => {
      this.eventManager.broadcast('banqueQualificationListModification');
      this.activeModal.close();
    });
  }
}
