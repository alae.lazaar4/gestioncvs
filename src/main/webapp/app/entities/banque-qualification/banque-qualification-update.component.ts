import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBanqueQualification, BanqueQualification } from 'app/shared/model/banque-qualification.model';
import { BanqueQualificationService } from './banque-qualification.service';

@Component({
  selector: 'jhi-banque-qualification-update',
  templateUrl: './banque-qualification-update.component.html',
})
export class BanqueQualificationUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    libelle: [],
    type: [],
    description: [],
  });

  constructor(
    protected banqueQualificationService: BanqueQualificationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ banqueQualification }) => {
      this.updateForm(banqueQualification);
    });
  }

  updateForm(banqueQualification: IBanqueQualification): void {
    this.editForm.patchValue({
      id: banqueQualification.id,
      libelle: banqueQualification.libelle,
      type: banqueQualification.type,
      description: banqueQualification.description,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const banqueQualification = this.createFromForm();
    if (banqueQualification.id !== undefined) {
      this.subscribeToSaveResponse(this.banqueQualificationService.update(banqueQualification));
    } else {
      this.subscribeToSaveResponse(this.banqueQualificationService.create(banqueQualification));
    }
  }

  private createFromForm(): IBanqueQualification {
    return {
      ...new BanqueQualification(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
      type: this.editForm.get(['type'])!.value,
      description: this.editForm.get(['description'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBanqueQualification>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
