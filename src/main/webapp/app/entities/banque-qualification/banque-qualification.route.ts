import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBanqueQualification, BanqueQualification } from 'app/shared/model/banque-qualification.model';
import { BanqueQualificationService } from './banque-qualification.service';
import { BanqueQualificationComponent } from './banque-qualification.component';
import { BanqueQualificationDetailComponent } from './banque-qualification-detail.component';
import { BanqueQualificationUpdateComponent } from './banque-qualification-update.component';

@Injectable({ providedIn: 'root' })
export class BanqueQualificationResolve implements Resolve<IBanqueQualification> {
  constructor(private service: BanqueQualificationService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBanqueQualification> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((banqueQualification: HttpResponse<BanqueQualification>) => {
          if (banqueQualification.body) {
            return of(banqueQualification.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BanqueQualification());
  }
}

export const banqueQualificationRoute: Routes = [
  {
    path: '',
    component: BanqueQualificationComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'gestioncvsApp.banqueQualification.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BanqueQualificationDetailComponent,
    resolve: {
      banqueQualification: BanqueQualificationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.banqueQualification.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BanqueQualificationUpdateComponent,
    resolve: {
      banqueQualification: BanqueQualificationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.banqueQualification.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BanqueQualificationUpdateComponent,
    resolve: {
      banqueQualification: BanqueQualificationResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.banqueQualification.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
