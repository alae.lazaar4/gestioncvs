import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBanqueQualification } from 'app/shared/model/banque-qualification.model';

type EntityResponseType = HttpResponse<IBanqueQualification>;
type EntityArrayResponseType = HttpResponse<IBanqueQualification[]>;

@Injectable({ providedIn: 'root' })
export class BanqueQualificationService {
  public resourceUrl = SERVER_API_URL + 'api/banque-qualifications';

  constructor(protected http: HttpClient) {}

  create(banqueQualification: IBanqueQualification): Observable<EntityResponseType> {
    return this.http.post<IBanqueQualification>(this.resourceUrl, banqueQualification, { observe: 'response' });
  }

  update(banqueQualification: IBanqueQualification): Observable<EntityResponseType> {
    return this.http.put<IBanqueQualification>(this.resourceUrl, banqueQualification, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBanqueQualification>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBanqueQualification[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
