import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestioncvsSharedModule } from 'app/shared/shared.module';
import { BanqueQualificationComponent } from './banque-qualification.component';
import { BanqueQualificationDetailComponent } from './banque-qualification-detail.component';
import { BanqueQualificationUpdateComponent } from './banque-qualification-update.component';
import { BanqueQualificationDeleteDialogComponent } from './banque-qualification-delete-dialog.component';
import { banqueQualificationRoute } from './banque-qualification.route';

@NgModule({
  imports: [GestioncvsSharedModule, RouterModule.forChild(banqueQualificationRoute)],
  declarations: [
    BanqueQualificationComponent,
    BanqueQualificationDetailComponent,
    BanqueQualificationUpdateComponent,
    BanqueQualificationDeleteDialogComponent,
  ],
  entryComponents: [BanqueQualificationDeleteDialogComponent],
})
export class GestioncvsBanqueQualificationModule {}
