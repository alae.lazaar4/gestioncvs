import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBanqueQualification } from 'app/shared/model/banque-qualification.model';

@Component({
  selector: 'jhi-banque-qualification-detail',
  templateUrl: './banque-qualification-detail.component.html',
})
export class BanqueQualificationDetailComponent implements OnInit {
  banqueQualification: IBanqueQualification | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ banqueQualification }) => (this.banqueQualification = banqueQualification));
  }

  previousState(): void {
    window.history.back();
  }
}
