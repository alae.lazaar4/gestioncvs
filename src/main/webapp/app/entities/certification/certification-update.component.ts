import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICertification, Certification } from 'app/shared/model/certification.model';
import { CertificationService } from './certification.service';
import { IRessourcePropose } from 'app/shared/model/ressource-propose.model';
import { RessourceProposeService } from 'app/entities/ressource-propose/ressource-propose.service';

@Component({
  selector: 'jhi-certification-update',
  templateUrl: './certification-update.component.html',
})
export class CertificationUpdateComponent implements OnInit {
  isSaving = false;
  ressourceproposes: IRessourcePropose[] = [];

  editForm = this.fb.group({
    id: [],
    intitule: [],
    organisme: [],
    anneeObtention: [],
    domaine: [],
    ressourceProposeId: [],
  });

  constructor(
    protected certificationService: CertificationService,
    protected ressourceProposeService: RessourceProposeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ certification }) => {
      if (!certification.id) {
        const today = moment().startOf('day');
        certification.anneeObtention = today;
      }

      this.updateForm(certification);

      this.ressourceProposeService.query().subscribe((res: HttpResponse<IRessourcePropose[]>) => (this.ressourceproposes = res.body || []));
    });
  }

  updateForm(certification: ICertification): void {
    this.editForm.patchValue({
      id: certification.id,
      intitule: certification.intitule,
      organisme: certification.organisme,
      anneeObtention: certification.anneeObtention ? certification.anneeObtention.format(DATE_TIME_FORMAT) : null,
      domaine: certification.domaine,
      ressourceProposeId: certification.ressourceProposeId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const certification = this.createFromForm();
    if (certification.id !== undefined) {
      this.subscribeToSaveResponse(this.certificationService.update(certification));
    } else {
      this.subscribeToSaveResponse(this.certificationService.create(certification));
    }
  }

  private createFromForm(): ICertification {
    return {
      ...new Certification(),
      id: this.editForm.get(['id'])!.value,
      intitule: this.editForm.get(['intitule'])!.value,
      organisme: this.editForm.get(['organisme'])!.value,
      anneeObtention: this.editForm.get(['anneeObtention'])!.value
        ? moment(this.editForm.get(['anneeObtention'])!.value, DATE_TIME_FORMAT)
        : undefined,
      domaine: this.editForm.get(['domaine'])!.value,
      ressourceProposeId: this.editForm.get(['ressourceProposeId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICertification>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IRessourcePropose): any {
    return item.id;
  }
}
