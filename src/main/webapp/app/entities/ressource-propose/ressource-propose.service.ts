import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRessourcePropose } from 'app/shared/model/ressource-propose.model';

type EntityResponseType = HttpResponse<IRessourcePropose>;
type EntityArrayResponseType = HttpResponse<IRessourcePropose[]>;

@Injectable({ providedIn: 'root' })
export class RessourceProposeService {
  public resourceUrl = SERVER_API_URL + 'api/ressource-proposes';

  constructor(protected http: HttpClient) {}

  create(ressourcePropose: IRessourcePropose): Observable<EntityResponseType> {
    return this.http.post<IRessourcePropose>(this.resourceUrl, ressourcePropose, { observe: 'response' });
  }

  update(ressourcePropose: IRessourcePropose): Observable<EntityResponseType> {
    return this.http.put<IRessourcePropose>(this.resourceUrl, ressourcePropose, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRessourcePropose>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRessourcePropose[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
