import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRessourcePropose } from 'app/shared/model/ressource-propose.model';

@Component({
  selector: 'jhi-ressource-propose-detail',
  templateUrl: './ressource-propose-detail.component.html',
})
export class RessourceProposeDetailComponent implements OnInit {
  ressourcePropose: IRessourcePropose | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ressourcePropose }) => (this.ressourcePropose = ressourcePropose));
  }

  previousState(): void {
    window.history.back();
  }
}
