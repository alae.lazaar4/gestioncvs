import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRessourcePropose } from 'app/shared/model/ressource-propose.model';
import { RessourceProposeService } from './ressource-propose.service';

@Component({
  templateUrl: './ressource-propose-delete-dialog.component.html',
})
export class RessourceProposeDeleteDialogComponent {
  ressourcePropose?: IRessourcePropose;

  constructor(
    protected ressourceProposeService: RessourceProposeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.ressourceProposeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('ressourceProposeListModification');
      this.activeModal.close();
    });
  }
}
