import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestioncvsSharedModule } from 'app/shared/shared.module';
import { RessourceProposeComponent } from './ressource-propose.component';
import { RessourceProposeDetailComponent } from './ressource-propose-detail.component';
import { RessourceProposeUpdateComponent } from './ressource-propose-update.component';
import { RessourceProposeDeleteDialogComponent } from './ressource-propose-delete-dialog.component';
import { ressourceProposeRoute } from './ressource-propose.route';

@NgModule({
  imports: [GestioncvsSharedModule, RouterModule.forChild(ressourceProposeRoute)],
  declarations: [
    RessourceProposeComponent,
    RessourceProposeDetailComponent,
    RessourceProposeUpdateComponent,
    RessourceProposeDeleteDialogComponent,
  ],
  entryComponents: [RessourceProposeDeleteDialogComponent],
})
export class GestioncvsRessourceProposeModule {}
