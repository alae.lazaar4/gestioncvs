import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IRessourcePropose, RessourcePropose } from 'app/shared/model/ressource-propose.model';
import { RessourceProposeService } from './ressource-propose.service';
import { RessourceProposeComponent } from './ressource-propose.component';
import { RessourceProposeDetailComponent } from './ressource-propose-detail.component';
import { RessourceProposeUpdateComponent } from './ressource-propose-update.component';

@Injectable({ providedIn: 'root' })
export class RessourceProposeResolve implements Resolve<IRessourcePropose> {
  constructor(private service: RessourceProposeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRessourcePropose> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((ressourcePropose: HttpResponse<RessourcePropose>) => {
          if (ressourcePropose.body) {
            return of(ressourcePropose.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RessourcePropose());
  }
}

export const ressourceProposeRoute: Routes = [
  {
    path: '',
    component: RessourceProposeComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'gestioncvsApp.ressourcePropose.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RessourceProposeDetailComponent,
    resolve: {
      ressourcePropose: RessourceProposeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.ressourcePropose.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RessourceProposeUpdateComponent,
    resolve: {
      ressourcePropose: RessourceProposeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.ressourcePropose.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RessourceProposeUpdateComponent,
    resolve: {
      ressourcePropose: RessourceProposeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.ressourcePropose.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
