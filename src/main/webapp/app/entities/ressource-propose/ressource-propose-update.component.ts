import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IRessourcePropose, RessourcePropose } from 'app/shared/model/ressource-propose.model';
import { RessourceProposeService } from './ressource-propose.service';
import { IRessourceDemande } from 'app/shared/model/ressource-demande.model';
import { RessourceDemandeService } from 'app/entities/ressource-demande/ressource-demande.service';
import { IEquipe } from 'app/shared/model/equipe.model';
import { EquipeService } from 'app/entities/equipe/equipe.service';

type SelectableEntity = IRessourceDemande | IEquipe;

@Component({
  selector: 'jhi-ressource-propose-update',
  templateUrl: './ressource-propose-update.component.html',
})
export class RessourceProposeUpdateComponent implements OnInit {
  isSaving = false;
  ressourcedemandes: IRessourceDemande[] = [];
  equipes: IEquipe[] = [];

  editForm = this.fb.group({
    id: [],
    nom: [null, [Validators.required]],
    prenom: [],
    posteActuel: [],
    anciennete: [],
    ressourceDemandeId: [],
    nomEquipeId: [],
  });

  constructor(
    protected ressourceProposeService: RessourceProposeService,
    protected ressourceDemandeService: RessourceDemandeService,
    protected equipeService: EquipeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ressourcePropose }) => {
      this.updateForm(ressourcePropose);

      this.ressourceDemandeService.query().subscribe((res: HttpResponse<IRessourceDemande[]>) => (this.ressourcedemandes = res.body || []));

      this.equipeService.query().subscribe((res: HttpResponse<IEquipe[]>) => (this.equipes = res.body || []));
    });
  }

  updateForm(ressourcePropose: IRessourcePropose): void {
    this.editForm.patchValue({
      id: ressourcePropose.id,
      nom: ressourcePropose.nom,
      prenom: ressourcePropose.prenom,
      posteActuel: ressourcePropose.posteActuel,
      anciennete: ressourcePropose.anciennete,
      ressourceDemandeId: ressourcePropose.ressourceDemandeId,
      nomEquipeId: ressourcePropose.nomEquipeId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const ressourcePropose = this.createFromForm();
    if (ressourcePropose.id !== undefined) {
      this.subscribeToSaveResponse(this.ressourceProposeService.update(ressourcePropose));
    } else {
      this.subscribeToSaveResponse(this.ressourceProposeService.create(ressourcePropose));
    }
  }

  private createFromForm(): IRessourcePropose {
    return {
      ...new RessourcePropose(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      posteActuel: this.editForm.get(['posteActuel'])!.value,
      anciennete: this.editForm.get(['anciennete'])!.value,
      ressourceDemandeId: this.editForm.get(['ressourceDemandeId'])!.value,
      nomEquipeId: this.editForm.get(['nomEquipeId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRessourcePropose>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
