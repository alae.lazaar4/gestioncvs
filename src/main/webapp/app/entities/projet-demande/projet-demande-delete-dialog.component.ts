import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProjetDemande } from 'app/shared/model/projet-demande.model';
import { ProjetDemandeService } from './projet-demande.service';

@Component({
  templateUrl: './projet-demande-delete-dialog.component.html',
})
export class ProjetDemandeDeleteDialogComponent {
  projetDemande?: IProjetDemande;

  constructor(
    protected projetDemandeService: ProjetDemandeService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.projetDemandeService.delete(id).subscribe(() => {
      this.eventManager.broadcast('projetDemandeListModification');
      this.activeModal.close();
    });
  }
}
