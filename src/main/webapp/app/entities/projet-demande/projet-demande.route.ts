import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProjetDemande, ProjetDemande } from 'app/shared/model/projet-demande.model';
import { ProjetDemandeService } from './projet-demande.service';
import { ProjetDemandeComponent } from './projet-demande.component';
import { ProjetDemandeDetailComponent } from './projet-demande-detail.component';
import { ProjetDemandeUpdateComponent } from './projet-demande-update.component';

@Injectable({ providedIn: 'root' })
export class ProjetDemandeResolve implements Resolve<IProjetDemande> {
  constructor(private service: ProjetDemandeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProjetDemande> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((projetDemande: HttpResponse<ProjetDemande>) => {
          if (projetDemande.body) {
            return of(projetDemande.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProjetDemande());
  }
}

export const projetDemandeRoute: Routes = [
  {
    path: '',
    component: ProjetDemandeComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'gestioncvsApp.projetDemande.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProjetDemandeDetailComponent,
    resolve: {
      projetDemande: ProjetDemandeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.projetDemande.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProjetDemandeUpdateComponent,
    resolve: {
      projetDemande: ProjetDemandeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.projetDemande.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProjetDemandeUpdateComponent,
    resolve: {
      projetDemande: ProjetDemandeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'gestioncvsApp.projetDemande.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
