import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProjetDemande } from 'app/shared/model/projet-demande.model';

@Component({
  selector: 'jhi-projet-demande-detail',
  templateUrl: './projet-demande-detail.component.html',
})
export class ProjetDemandeDetailComponent implements OnInit {
  projetDemande: IProjetDemande | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ projetDemande }) => (this.projetDemande = projetDemande));
  }

  previousState(): void {
    window.history.back();
  }
}
