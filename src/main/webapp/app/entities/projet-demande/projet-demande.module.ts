import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestioncvsSharedModule } from 'app/shared/shared.module';
import { ProjetDemandeComponent } from './projet-demande.component';
import { ProjetDemandeDetailComponent } from './projet-demande-detail.component';
import { ProjetDemandeUpdateComponent } from './projet-demande-update.component';
import { ProjetDemandeDeleteDialogComponent } from './projet-demande-delete-dialog.component';
import { projetDemandeRoute } from './projet-demande.route';

@NgModule({
  imports: [GestioncvsSharedModule, RouterModule.forChild(projetDemandeRoute)],
  declarations: [ProjetDemandeComponent, ProjetDemandeDetailComponent, ProjetDemandeUpdateComponent, ProjetDemandeDeleteDialogComponent],
  entryComponents: [ProjetDemandeDeleteDialogComponent],
})
export class GestioncvsProjetDemandeModule {}
