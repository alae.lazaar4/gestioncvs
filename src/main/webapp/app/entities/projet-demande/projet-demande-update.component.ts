import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProjetDemande, ProjetDemande } from 'app/shared/model/projet-demande.model';
import { ProjetDemandeService } from './projet-demande.service';

@Component({
  selector: 'jhi-projet-demande-update',
  templateUrl: './projet-demande-update.component.html',
})
export class ProjetDemandeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    description: [],
    entreprise: [],
    duree: [],
  });

  constructor(protected projetDemandeService: ProjetDemandeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ projetDemande }) => {
      this.updateForm(projetDemande);
    });
  }

  updateForm(projetDemande: IProjetDemande): void {
    this.editForm.patchValue({
      id: projetDemande.id,
      description: projetDemande.description,
      entreprise: projetDemande.entreprise,
      duree: projetDemande.duree,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const projetDemande = this.createFromForm();
    if (projetDemande.id !== undefined) {
      this.subscribeToSaveResponse(this.projetDemandeService.update(projetDemande));
    } else {
      this.subscribeToSaveResponse(this.projetDemandeService.create(projetDemande));
    }
  }

  private createFromForm(): IProjetDemande {
    return {
      ...new ProjetDemande(),
      id: this.editForm.get(['id'])!.value,
      description: this.editForm.get(['description'])!.value,
      entreprise: this.editForm.get(['entreprise'])!.value,
      duree: this.editForm.get(['duree'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProjetDemande>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
