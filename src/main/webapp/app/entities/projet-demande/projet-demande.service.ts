import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IProjetDemande } from 'app/shared/model/projet-demande.model';

type EntityResponseType = HttpResponse<IProjetDemande>;
type EntityArrayResponseType = HttpResponse<IProjetDemande[]>;

@Injectable({ providedIn: 'root' })
export class ProjetDemandeService {
  public resourceUrl = SERVER_API_URL + 'api/projet-demandes';

  constructor(protected http: HttpClient) {}

  create(projetDemande: IProjetDemande): Observable<EntityResponseType> {
    return this.http.post<IProjetDemande>(this.resourceUrl, projetDemande, { observe: 'response' });
  }

  update(projetDemande: IProjetDemande): Observable<EntityResponseType> {
    return this.http.put<IProjetDemande>(this.resourceUrl, projetDemande, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProjetDemande>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProjetDemande[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
