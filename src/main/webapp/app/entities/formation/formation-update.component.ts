import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IFormation, Formation } from 'app/shared/model/formation.model';
import { FormationService } from './formation.service';
import { IRessourcePropose } from 'app/shared/model/ressource-propose.model';
import { RessourceProposeService } from 'app/entities/ressource-propose/ressource-propose.service';

@Component({
  selector: 'jhi-formation-update',
  templateUrl: './formation-update.component.html',
})
export class FormationUpdateComponent implements OnInit {
  isSaving = false;
  ressourceproposes: IRessourcePropose[] = [];

  editForm = this.fb.group({
    id: [],
    etablissemnt: [],
    dateEntree: [],
    dateSortie: [],
    ville: [],
    pays: [],
    titre: [],
    specialite: [],
    ressourceProposeId: [],
  });

  constructor(
    protected formationService: FormationService,
    protected ressourceProposeService: RessourceProposeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ formation }) => {
      this.updateForm(formation);

      this.ressourceProposeService.query().subscribe((res: HttpResponse<IRessourcePropose[]>) => (this.ressourceproposes = res.body || []));
    });
  }

  updateForm(formation: IFormation): void {
    this.editForm.patchValue({
      id: formation.id,
      etablissemnt: formation.etablissemnt,
      dateEntree: formation.dateEntree,
      dateSortie: formation.dateSortie,
      ville: formation.ville,
      pays: formation.pays,
      titre: formation.titre,
      specialite: formation.specialite,
      ressourceProposeId: formation.ressourceProposeId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const formation = this.createFromForm();
    if (formation.id !== undefined) {
      this.subscribeToSaveResponse(this.formationService.update(formation));
    } else {
      this.subscribeToSaveResponse(this.formationService.create(formation));
    }
  }

  private createFromForm(): IFormation {
    return {
      ...new Formation(),
      id: this.editForm.get(['id'])!.value,
      etablissemnt: this.editForm.get(['etablissemnt'])!.value,
      dateEntree: this.editForm.get(['dateEntree'])!.value,
      dateSortie: this.editForm.get(['dateSortie'])!.value,
      ville: this.editForm.get(['ville'])!.value,
      pays: this.editForm.get(['pays'])!.value,
      titre: this.editForm.get(['titre'])!.value,
      specialite: this.editForm.get(['specialite'])!.value,
      ressourceProposeId: this.editForm.get(['ressourceProposeId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFormation>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IRessourcePropose): any {
    return item.id;
  }
}
