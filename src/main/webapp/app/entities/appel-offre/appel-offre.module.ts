import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestioncvsSharedModule } from 'app/shared/shared.module';
import { AppelOffreComponent } from './appel-offre.component';
import { AppelOffreDetailComponent } from './appel-offre-detail.component';
import { AppelOffreUpdateComponent } from './appel-offre-update.component';
import { AppelOffreDeleteDialogComponent } from './appel-offre-delete-dialog.component';
import { appelOffreRoute } from './appel-offre.route';
import { NgWizardModule, NgWizardConfig, THEME } from 'ng-wizard';
const ngWizardConfig: NgWizardConfig = {
  theme: THEME.default
};
@NgModule({
  imports: [NgWizardModule.forRoot(ngWizardConfig),GestioncvsSharedModule, RouterModule.forChild(appelOffreRoute)],
  declarations: [AppelOffreComponent, AppelOffreDetailComponent, AppelOffreUpdateComponent, AppelOffreDeleteDialogComponent],
  entryComponents: [AppelOffreDeleteDialogComponent],
})
export class GestioncvsAppelOffreModule {}
