import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAppelOffre } from 'app/shared/model/appel-offre.model';

type EntityResponseType = HttpResponse<IAppelOffre>;
type EntityArrayResponseType = HttpResponse<IAppelOffre[]>;

@Injectable({ providedIn: 'root' })
export class AppelOffreService {
  public resourceUrl = SERVER_API_URL + 'api/appel-offres';

  constructor(protected http: HttpClient) {}

  create(appelOffre: IAppelOffre): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(appelOffre);
    return this.http
      .post<IAppelOffre>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(appelOffre: IAppelOffre): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(appelOffre);
    return this.http
      .put<IAppelOffre>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAppelOffre>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAppelOffre[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(appelOffre: IAppelOffre): IAppelOffre {
    const copy: IAppelOffre = Object.assign({}, appelOffre, {
      date: appelOffre.date && appelOffre.date.isValid() ? appelOffre.date.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? moment(res.body.date) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((appelOffre: IAppelOffre) => {
        appelOffre.date = appelOffre.date ? moment(appelOffre.date) : undefined;
      });
    }
    return res;
  }
}
