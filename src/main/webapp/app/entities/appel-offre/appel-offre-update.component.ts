import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {Observable, of} from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IAppelOffre, AppelOffre } from 'app/shared/model/appel-offre.model';
import { AppelOffreService } from './appel-offre.service';
import { IProjetDemande } from 'app/shared/model/projet-demande.model';
import { ProjetDemandeService } from 'app/entities/projet-demande/projet-demande.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client/client.service';
import { NgWizardConfig, NgWizardService, StepChangedArgs, StepValidationArgs, STEP_STATE, THEME } from 'ng-wizard';

type SelectableEntity = IProjetDemande | IClient;

@Component({
  selector: 'jhi-appel-offre-update',
  templateUrl: './appel-offre-update.component.html',
})
export class AppelOffreUpdateComponent implements OnInit {
  isSaving = false;
  projetsds: IProjetDemande[] = [];
  clients: IClient[] = [];

  appelForm = this.fb.group({
    id: [],
    date: [],
    budget: [],
    duree: [],
    context: [],
    projetsDId: [],
    clientId: [],
  });
  projetDForm=this.fb.group({
    description: [],
    entreprise: []
  });
  ressourceDForm=this.fb.group({
    description: [],
    entreprise: []
  });
  stepStates = {
    normal: STEP_STATE.normal,
    disabled: STEP_STATE.disabled,
    error: STEP_STATE.error,
    hidden: STEP_STATE.hidden
  };

  config: NgWizardConfig = {
    selected: 0,
    theme: THEME.arrows,
    toolbarSettings: {
      toolbarExtraButtons: [
        { text: 'Finish', class: 'btn btn-info', event: () => { alert("Finished!!!"); } }
      ],
    }
  };

  constructor(
    protected appelOffreService: AppelOffreService,
    protected projetDemandeService: ProjetDemandeService,
    protected clientService: ClientService,
    protected activatedRoute: ActivatedRoute,
    private ngWizardService: NgWizardService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ appelOffre }) => {
      if (!appelOffre.id) {
        const today = moment().startOf('day');
        appelOffre.date = today;
      }

      this.updateForm(appelOffre);

      this.projetDemandeService
        .query({ filter: 'appeloffre-is-null' })
        .pipe(
          map((res: HttpResponse<IProjetDemande[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IProjetDemande[]) => {
          if (!appelOffre.projetsDId) {
            this.projetsds = resBody;
          } else {
            this.projetDemandeService
              .find(appelOffre.projetsDId)
              .pipe(
                map((subRes: HttpResponse<IProjetDemande>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IProjetDemande[]) => (this.projetsds = concatRes));
          }
        });

      this.clientService.query().subscribe((res: HttpResponse<IClient[]>) => (this.clients = res.body || []));
    });
  }

  updateForm(appelOffre: IAppelOffre): void {
    this.appelForm.patchValue({
      id: appelOffre.id,
      date: appelOffre.date ? appelOffre.date.format(DATE_TIME_FORMAT) : null,
      budget: appelOffre.budget,
      duree: appelOffre.duree,
      context: appelOffre.context,
      projetsDId: appelOffre.projetsDId,
      clientId: appelOffre.clientId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const appelOffre = this.createFromForm();
    if (appelOffre.id !== undefined) {
      this.subscribeToSaveResponse(this.appelOffreService.update(appelOffre));
    } else {
      this.subscribeToSaveResponse(this.appelOffreService.create(appelOffre));
    }
  }

  private createFromForm(): IAppelOffre {
    return {
      ...new AppelOffre(),
      id: this.appelForm.get(['id'])!.value,
      date: this.appelForm.get(['date'])!.value ? moment(this.appelForm.get(['date'])!.value, DATE_TIME_FORMAT) : undefined,
      budget: this.appelForm.get(['budget'])!.value,
      duree: this.appelForm.get(['duree'])!.value,
      context: this.appelForm.get(['context'])!.value,
      projetsDId: this.appelForm.get(['projetsDId'])!.value,
      clientId: this.appelForm.get(['clientId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAppelOffre>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
  showPreviousStep(event?: Event) {
    this.ngWizardService.previous();
  }

  showNextStep(event?: Event) {
    this.ngWizardService.next();
  }

  resetWizard(event?: Event) {
    this.ngWizardService.reset();
  }

  setTheme(theme: THEME) {
    this.ngWizardService.theme(theme);
  }

  stepChanged(args: StepChangedArgs) {
    console.log(args.step);
  }

  isValidTypeBoolean: boolean = true;

  isValidFunctionReturnsBoolean(args: StepValidationArgs) {
    return true;
  }

  isValidFunctionReturnsObservable(args: StepValidationArgs) {
    return of(true);
  }

}
