import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IExperience, Experience } from 'app/shared/model/experience.model';
import { ExperienceService } from './experience.service';
import { IRessourcePropose } from 'app/shared/model/ressource-propose.model';
import { RessourceProposeService } from 'app/entities/ressource-propose/ressource-propose.service';

@Component({
  selector: 'jhi-experience-update',
  templateUrl: './experience-update.component.html',
})
export class ExperienceUpdateComponent implements OnInit {
  isSaving = false;
  ressourceproposes: IRessourcePropose[] = [];

  editForm = this.fb.group({
    id: [],
    firstName: [],
    lastName: [],
    email: [],
    phoneNumber: [],
    hireDate: [],
    salary: [],
    commissionPct: [],
    ressourceProposeId: [],
  });

  constructor(
    protected experienceService: ExperienceService,
    protected ressourceProposeService: RessourceProposeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ experience }) => {
      if (!experience.id) {
        const today = moment().startOf('day');
        experience.hireDate = today;
      }

      this.updateForm(experience);

      this.ressourceProposeService.query().subscribe((res: HttpResponse<IRessourcePropose[]>) => (this.ressourceproposes = res.body || []));
    });
  }

  updateForm(experience: IExperience): void {
    this.editForm.patchValue({
      id: experience.id,
      firstName: experience.firstName,
      lastName: experience.lastName,
      email: experience.email,
      phoneNumber: experience.phoneNumber,
      hireDate: experience.hireDate ? experience.hireDate.format(DATE_TIME_FORMAT) : null,
      salary: experience.salary,
      commissionPct: experience.commissionPct,
      ressourceProposeId: experience.ressourceProposeId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const experience = this.createFromForm();
    if (experience.id !== undefined) {
      this.subscribeToSaveResponse(this.experienceService.update(experience));
    } else {
      this.subscribeToSaveResponse(this.experienceService.create(experience));
    }
  }

  private createFromForm(): IExperience {
    return {
      ...new Experience(),
      id: this.editForm.get(['id'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      email: this.editForm.get(['email'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      hireDate: this.editForm.get(['hireDate'])!.value ? moment(this.editForm.get(['hireDate'])!.value, DATE_TIME_FORMAT) : undefined,
      salary: this.editForm.get(['salary'])!.value,
      commissionPct: this.editForm.get(['commissionPct'])!.value,
      ressourceProposeId: this.editForm.get(['ressourceProposeId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IExperience>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IRessourcePropose): any {
    return item.id;
  }
}
