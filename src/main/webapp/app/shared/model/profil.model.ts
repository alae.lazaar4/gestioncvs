import { IBanqueQualification } from 'app/shared/model/banque-qualification.model';
import { IBanqueProjet } from 'app/shared/model/banque-projet.model';

export interface IProfil {
  id?: number;
  activite?: string;
  qualifications?: IBanqueQualification[];
  projets?: IBanqueProjet[];
}

export class Profil implements IProfil {
  constructor(
    public id?: number,
    public activite?: string,
    public qualifications?: IBanqueQualification[],
    public projets?: IBanqueProjet[]
  ) {}
}
