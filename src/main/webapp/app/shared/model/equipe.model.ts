import { IRessourcePropose } from 'app/shared/model/ressource-propose.model';

export interface IEquipe {
  id?: number;
  chefEquipe?: string;
  ressourceProposes?: IRessourcePropose[];
}

export class Equipe implements IEquipe {
  constructor(public id?: number, public chefEquipe?: string, public ressourceProposes?: IRessourcePropose[]) {}
}
