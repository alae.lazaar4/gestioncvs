import { IBanqueProjet } from 'app/shared/model/banque-projet.model';

export interface IEnvironTech {
  id?: number;
  webTechnlogies?: string;
  webClientTechnologies?: string;
  baseDonnee?: string;
  projetManagementBuilding?: string;
  architechture?: string;
  nomProjets?: IBanqueProjet[];
}

export class EnvironTech implements IEnvironTech {
  constructor(
    public id?: number,
    public webTechnlogies?: string,
    public webClientTechnologies?: string,
    public baseDonnee?: string,
    public projetManagementBuilding?: string,
    public architechture?: string,
    public nomProjets?: IBanqueProjet[]
  ) {}
}
