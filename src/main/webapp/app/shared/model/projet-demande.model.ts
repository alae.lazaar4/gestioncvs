export interface IProjetDemande {
  id?: number;
  description?: string;
  entreprise?: string;
  duree?: string;
}

export class ProjetDemande implements IProjetDemande {
  constructor(public id?: number, public description?: string, public entreprise?: string, public duree?: string) {}
}
