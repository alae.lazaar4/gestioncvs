import { IProfil } from 'app/shared/model/profil.model';

export interface IBanqueQualification {
  id?: number;
  libelle?: string;
  type?: string;
  description?: string;
  profils?: IProfil[];
}

export class BanqueQualification implements IBanqueQualification {
  constructor(public id?: number, public libelle?: string, public type?: string, public description?: string, public profils?: IProfil[]) {}
}
