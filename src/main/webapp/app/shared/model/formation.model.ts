export interface IFormation {
  id?: number;
  etablissemnt?: string;
  dateEntree?: string;
  dateSortie?: string;
  ville?: string;
  pays?: string;
  titre?: string;
  specialite?: string;
  ressourceProposeId?: number;
}

export class Formation implements IFormation {
  constructor(
    public id?: number,
    public etablissemnt?: string,
    public dateEntree?: string,
    public dateSortie?: string,
    public ville?: string,
    public pays?: string,
    public titre?: string,
    public specialite?: string,
    public ressourceProposeId?: number
  ) {}
}
