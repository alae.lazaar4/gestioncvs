import { Moment } from 'moment';
import { IRessourceDemande } from 'app/shared/model/ressource-demande.model';

export interface IAppelOffre {
  id?: number;
  date?: Moment;
  budget?: string;
  duree?: number;
  context?: string;
  projetsDId?: number;
  ressourceDemandes?: IRessourceDemande[];
  clientId?: number;
}

export class AppelOffre implements IAppelOffre {
  constructor(
    public id?: number,
    public date?: Moment,
    public budget?: string,
    public duree?: number,
    public context?: string,
    public projetsDId?: number,
    public ressourceDemandes?: IRessourceDemande[],
    public clientId?: number
  ) {}
}
