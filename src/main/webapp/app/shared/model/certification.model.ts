import { Moment } from 'moment';

export interface ICertification {
  id?: number;
  intitule?: string;
  organisme?: string;
  anneeObtention?: Moment;
  domaine?: string;
  ressourceProposeId?: number;
}

export class Certification implements ICertification {
  constructor(
    public id?: number,
    public intitule?: string,
    public organisme?: string,
    public anneeObtention?: Moment,
    public domaine?: string,
    public ressourceProposeId?: number
  ) {}
}
