import { ICertification } from 'app/shared/model/certification.model';
import { IExperience } from 'app/shared/model/experience.model';
import { IFormation } from 'app/shared/model/formation.model';

export interface IRessourcePropose {
  id?: number;
  nom?: string;
  prenom?: string;
  posteActuel?: string;
  anciennete?: number;
  certifications?: ICertification[];
  experiences?: IExperience[];
  formations?: IFormation[];
  ressourceDemandeId?: number;
  nomEquipeId?: number;
}

export class RessourcePropose implements IRessourcePropose {
  constructor(
    public id?: number,
    public nom?: string,
    public prenom?: string,
    public posteActuel?: string,
    public anciennete?: number,
    public certifications?: ICertification[],
    public experiences?: IExperience[],
    public formations?: IFormation[],
    public ressourceDemandeId?: number,
    public nomEquipeId?: number
  ) {}
}
