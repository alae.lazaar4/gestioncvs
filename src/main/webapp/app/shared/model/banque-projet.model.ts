import { Moment } from 'moment';
import { IProfil } from 'app/shared/model/profil.model';

export interface IBanqueProjet {
  id?: number;
  designation?: string;
  organisme?: string;
  budget?: string;
  anneeProjet?: Moment;
  duree?: string;
  resultat?: string;
  mission?: string;
  environTechId?: number;
  profils?: IProfil[];
}

export class BanqueProjet implements IBanqueProjet {
  constructor(
    public id?: number,
    public designation?: string,
    public organisme?: string,
    public budget?: string,
    public anneeProjet?: Moment,
    public duree?: string,
    public resultat?: string,
    public mission?: string,
    public environTechId?: number,
    public profils?: IProfil[]
  ) {}
}
