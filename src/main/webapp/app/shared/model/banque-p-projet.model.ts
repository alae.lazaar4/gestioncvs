export interface IBanquePProjet {
  id?: number;
}

export class BanquePProjet implements IBanquePProjet {
  constructor(public id?: number) {}
}
