import { IRessourcePropose } from 'app/shared/model/ressource-propose.model';

export interface IRessourceDemande {
  id?: number;
  emploiActuel?: string;
  ancienneteEtude?: string;
  ancienneteEmploi?: string;
  fonction?: string;
  nbRessources?: number;
  ressourceProposes?: IRessourcePropose[];
  appelOffreId?: number;
}

export class RessourceDemande implements IRessourceDemande {
  constructor(
    public id?: number,
    public emploiActuel?: string,
    public ancienneteEtude?: string,
    public ancienneteEmploi?: string,
    public fonction?: string,
    public nbRessources?: number,
    public ressourceProposes?: IRessourcePropose[],
    public appelOffreId?: number
  ) {}
}
