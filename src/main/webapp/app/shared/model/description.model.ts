export interface IDescription {
  id?: number;
  context?: string;
  mission?: string;
  environnementTechnique?: string;
}

export class Description implements IDescription {
  constructor(public id?: number, public context?: string, public mission?: string, public environnementTechnique?: string) {}
}
