import { IAppelOffre } from 'app/shared/model/appel-offre.model';

export interface IClient {
  id?: number;
  organisme?: string;
  appelOffres?: IAppelOffre[];
}

export class Client implements IClient {
  constructor(public id?: number, public organisme?: string, public appelOffres?: IAppelOffre[]) {}
}
